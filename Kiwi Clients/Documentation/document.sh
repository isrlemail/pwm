chmod +x Documentation/bin/*.py

PARSER_INPUT="BrowserAddIn/WebServer/InPageServices"
PARSER_OUTPUT="output"
GENERATOR_OUTPUT="docs"
TEMPLATE="Documentation/template"

VERSION=1.0.0

YUI_VERSION=2

mkdir $PARSER_OUTPUT
mkdir $GENERATOR_OUTPUT

Documentation/bin/yuidoc.py $PARSER_INPUT -e ".js" -p $PARSER_OUTPUT -o $GENERATOR_OUTPUT -t $TEMPLATE -v $VERSION -Y $YUI_VERSION

rm -r $PARSER_OUTPUT
