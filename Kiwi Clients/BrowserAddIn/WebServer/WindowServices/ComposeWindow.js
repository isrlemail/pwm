/**
 * The compose window scripts.
 * @class ComposeWindow
 */
var ComposeWindow = new Class({

    //#region Fields

    /**
    * The guid of this frame.
    * @property guid
    * @type String
    */
    guid: '@GUID',

    /**
    * The id of the user.
    * @property userId
    * @type String
    */
    userId: null,

    /**
    * The Uri to use for the key server.
    * @property selectedKeyServer
    * @type String
    */
    selectedKeyServer: Uris.keyServerUris[0],

    /**
    * Last text that was saved.
    * @property lastText
    * @type String
    */
    lastText: '',

    //#endregion

    //#region Constructor

    /**
    * Constructs the compose window.
    * @method initialize.
    */
    initialize: function () {
        var self = this;

        // Sends a message to send the message.
        window.sendEncryptedMessage = function () {
            jQuery(document.body).mask('Encrypting message...');
            self.encryptMessage(function (kiwiPackage) {
                self.postMessage(WindowMessageTypes.SendMessage, kiwiPackage.toJsonObject());
                jQuery(document.body).unmask();
            }, false);
        }

        // Sends a message saving the draft.
        window.saveEncryptedDraft = function () {
            // Only save a draft if there has been a change.
            var newText = jQuery('#editor').cleanHtml();
            if (newText === self.lastText)
                return;
            self.lastText = newText;

            // Encrypte and save draft.
            self.encryptMessage(function (kiwiPackage) {
                self.postMessage(WindowMessageTypes.SaveDraft, kiwiPackage.toJsonObject());
            }, true);
        };

        // Method to cancel encryption.
        window.cancelEncryption = function () {
            self.postMessage(WindowMessageTypes.CancelEncryption, $('#editor').cleanHtml());
        };

        // Method to discard a draft.
        window.discardDraft = function () {
            self.postMessage(WindowMessageTypes.DiscardDraft, null);
        };

        // Fired when the document is done being loaded.
        jQuery(document).ready(function () {
            // Initialize the event listeners.
            jQuery('#encrypt').on('click', window.sendEncryptedMessage);
            jQuery('#cancel').on('click', window.cancelEncryption);
            jQuery('#discard').on('click', window.discardDraft);

            // Register the post message handler, and send a message to get initial contents.
            window.addEventListener('message', function (event) { self.handlePostMessage(event); }, false);
            self.postMessage(WindowMessageTypes.GetContents, null);
        });
    },

    //#endregion

    //#region Encryption

    /** 
    * Callback to be called when recipients retrieved.  Needed so encryption will get the correct results.
    * @property recipientCallback
    * @type Function
    */
    recipientCallback: null,

    /** 
    * Encrypts the message and returns a package.
    * @method encryptMessage
    * @param callback {Function} Function to call when encryption is finished, passed a string with the package.
    * @param isDraft {Boolean} Whether this is a draft.
    */
    encryptMessage: function (callback, isDraft) {
        var self = this;

        if (!isDraft)
            jQuery(document.body).mask('Retrieving encryption keys...');
        KeyManagerProxy.getCreatorKey(new KeyRequestArgs(this.selectedKeyServer, this.userId, null, new Date(), EmailPackager.PackageKeySize),
            function (success, value, args) {
                if (!success) {
                    if (!isDraft)
                        jQuery(document.body).mask("Failed to get an encryption key.  Try again or ensure you have permissions to use the selected key server.", 'OK');
                    return;
                }

                if (!isDraft)
                    jQuery(document.body).mask('Encrypting message...');

                self.recipientCallback = function (recipients) {
                    if (!isDraft && recipients.length == 0) {
                        jQuery(document.body).mask("There are no recipients. Please fix this and try again.", 'OK');
                        return;
                    }

                    try {
                        var packager = new EmailPackager();
                        packager.creatorId = args.creatorId;
                        packager.keyServerUri = args.keyServerUri;
                        packager.timestamp = args.timestamp;
                        packager.creatorKey = value;
                        
                        var sanitizedHTML = html_sanitize(jQuery('#editor').cleanHtml(), self.urlSanitizer, self.idSanitizer);

                        // Make links open in new window.
                        var email = document.createElement('div');
                        email.innerHTML = sanitizedHTML;
                        jQuery('a', email).attr('target', '_blank');

                        packager.message = new Message();
                        packager.message.contents = email.innerHTML;

                        if (isDraft) {
                            callback(packager.wrap());
                        } else {
                            for (var i = 0; i < recipients.length; i++)
                                packager.addViewer(recipients[i]);

                            var i = 0;
                            var showEncryptionMessage = function () {
                                if (i == recipients.length) {
                                    jQuery(document.body).mask('Finishing up encryption...');
                                    var encryptedPackage = packager.wrap();
                                    jQuery(document.body).mask('Message encrypted and sent.');
                                    callback(encryptedPackage);
                                }
                                else {
                                    jQuery(document.body).mask('Encrypting for ' + recipients[i] + '...');
                                    i += 1;
                                    window.setTimeout(showEncryptionMessage, 1500);
                                }
                            };
                            showEncryptionMessage();
                        }
                    }
                    catch (e) {
                        if (!isDraft)
                            jQuery(document.body).mask("Failed to encrypt the message. Please try again.", 'OK');
                    }
                };

                //self.encryptProgressDialog.setMessage("Getting recipients.");
                self.postMessage(WindowMessageTypes.GetRecipients, null);
            });
    },

    /**
    * Sanitizes URLs.
    */
    urlSanitizer: function(url) {
        if(/^(https?:\/\/|mailto:)/.test(url))
            return url;
        if (/^data:image\//.test(url))
            return url;
        else return null;
    },

    /**
    * Sanitizes ids.
    */
    idSanitizer: function(id) {
        return id;
    },

    /**
    * Takes a package and decrypts it.
    * @method decryptMessage
    * @param kiwiPackage {String} Decrypts the package string.
    * @param viewerId {String} Id of the viewer reading the message.
    * @param callback {Function} Function to call with decrypted package, passed the body of the email.
    */
    decryptMessage: function (kiwiPackage, viewerId, callback) {
        var self = this;
        try {
            var emailPackager = new EmailPackager(kiwiPackage);
        }
        catch (exception) {
            jQuery(document.body).mask("Invalid e-mail message.  Contact the sender and have them resend the message.", 'OK');
        }

        jQuery(document.body).mask('"Retreiving decryption key...');
        KeyManagerProxy.getViewerKey(new KeyRequestArgs(emailPackager.keyServerUri, emailPackager.creatorId, viewerId, emailPackager.timestamp, EmailPackager.PackageKeySize),
            function (success, value, args) {
                if (!success) {
                    jQuery(document.body).mask("Failed to get an decryption key.  Try to open the message again, or contact the sender and have them resend the message.", 'OK');
                    return;
                }

                try {
                    jQuery(document.body).mask('Decrypting message...');
                    emailPackager.unwrap(kiwiPackage, viewerId, value);
                    callback(emailPackager.message.contents);
                }
                catch (exception) {
                    jQuery(document.body).mask('Invalid e-mail message.  Contact the sender and have them resend the message.', 'OK');
                }
            });
    },

    //#endregion

    //#region Messaging

    /**
    * Sends a message to the parent frame.
    * @method postMessage
    * @param messageType {Number} The type of message being sent.
    * @param content {Object} The object to be sent.
    */
    postMessage: function (messageType, content) {
        var self = this;
        window.setTimeout(function () { window.top.postMessage(JSON.stringify(new WindowMessage(self.guid, messageType, content)), '*'); }, 0);
    },

    /**
    * Handles messages passed to the window.
    * @method handlePostMessage
    * @param event {Event} Event object
    */
    handlePostMessage: function (event) {
        var self = this;
        var message = JSON.parse(event.data);

        if (message.guid != this.guid)
            return;

        // Handle the various message types.
        var info = message.contents;
        if (message.messageType == WindowMessageTypes.GetRecipients) {
            if (this.recipientCallback)
                this.recipientCallback(info);
        }
        else if (message.messageType == WindowMessageTypes.GetContents) {
            this.userId = info.userId;
            if (!info.allowCancel)
                jQuery('#cancel').remove();

            // Pre-fetch creator key.
            KeyManagerProxy.getCreatorKey(new KeyRequestArgs(this.selectedKeyServer, this.userId, null, new Date(), EmailPackager.PackageKeySize), function () { });

            // If there is a kiwi package decrypt it and then show it.
            if (info.kiwiPackage) {
                jQuery(document.body).mask('Unwrapping original message...');
                this.decryptMessage(Package.fromJsonObject(info.kiwiPackage), this.userId, function (message) {
                    $('#editor').html(info.body.replace(info.templateItem, message));
                    jQuery(document.body).unmask();
                    $('#editor').focus();

                    // Set up draft saving.
                    self.lastText = jQuery('#editor').cleanHtml();
                    window.saveEncryptedDraft();
                    window.setInterval(window.saveEncryptedDraft, 5000);
                });
            }
            else {
                jQuery('#LockIcon').css('display', 'inline-block');
                $('#editor').html(info.body);
                $('#editor').focus();

                // Set up draft saving.
                window.saveEncryptedDraft();
                window.setInterval(window.saveEncryptedDraft, 5000);
            }

            
        }
        else if (message.messageType == WindowMessageTypes.FocusChanged) {
            $('#editor').focus();
        }
    }

    //#endregion

});

var composeWindow = new ComposeWindow();