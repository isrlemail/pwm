﻿/**
* JavaScript functions for the key manager window.
* @class KeyManagerWindow
*/
var KeyManagerWindow = new Class({

	//#region Fields

	/**
	* The guid of this frame.
	* @property guid
	* @type String
	*/
	guid: '@GUID',

	//#endregion

	//#region Constructor

	/**
	* Constructs the compose window.
	* @method initialize.
	*/
	initialize: function ()
	{
		var self = this;

		// Fired when the document is done being loaded.
		jQuery(document).ready(function ()
		{
			// Register the post message handler.
			window.addEventListener('message', function (event) { self.handlePostMessage(event); }, false);
		});

		// Register key manager events.
		if (AuthManager.passwordHandler)
			AuthManager.passwordHandler = function () { return self.requestPassword(); };
		if (AuthManager.sawTokenRequested)
			AuthManager.sawTokenRequested.addListener(function (transactionId) { self.checkForSawToken(transactionId); });
		if (AuthManager.otherAuthTokenRequested)
			AuthManager.otherAuthTokenRequested.addListener(function (authUri) { self.beginGetOtherAuthToken(authUri); });
		if (AuthManager.otherAuthTokenReceived)
			AuthManager.otherAuthTokenReceived.addListener(function () { self.endGetOtherAuthToken(); });
	},

	//#endregion

	//#region Messaging

	/**
	* Sends a message to the parent frame.
	* @method postMessage
	* @param messageType {Number} The type of message being sent.
	* @param content {Object} The object to be sent.
	*/
	postMessage: function (messageType, content)
	{
		var self = this;
		window.setTimeout(function () { window.top.postMessage(JSON.stringify(new WindowMessage(self.guid, messageType, content)), '*'); }, 0);
	},

	/**
	* Handles messages passed to the window.
	* @method handlePostMessage
	* @param event {Event} Event object
	*/
	handlePostMessage: function (event)
	{
		var message = JSON.parse(event.data);

		if (message.guid != this.guid)
			return;

		// Handle the various message types.
		var info = message.contents;
		switch (message.messageType)
		{
			case WindowMessageTypes.GetSawTokens:
				this.handleSawTokens(info);
				break;
		}
	},

	//#endregion

	//#region Password authentication

	/**
	* Requests a password from the user.
	* @method requestPassword
	* @param authServerUri {String} The auth server that prequested the password.
	*/
	requestPassword: function (authServerUri)
	{
		return window.prompt(authServerUri + ' has requested a password', '');
	},

	//#endregion

	//#region SAW

	/**
	* Requests to get saw tokens from the parent window.
	* @method checkForSawTokens
	*/
	checkForSawToken: function (transactionId)
	{
		this.postMessage(WindowMessageTypes.GetSawTokens, transactionId);
	},

	/**
	* Handles saw tokens sent from the parent window.
	* @method handleSawTokens
	* @param tokens {Array.<Object>} The tokens sent by the parent window.
	*/
	handleSawTokens: function (tokens)
	{
		for (var i = 0; i < tokens.length; i++)
		{
			AuthManager.registerSawToken(tokens[i].transactionId, tokens[i].value);
		}
	},

	//#endregion

	//#region Other auth

	/**
	* Request to get other auth tokens from the parent window
	* @method beginGetOtherAuthToken
	*/
	beginGetOtherAuthToken: function (authUri)
	{
		this.postMessage(WindowMessageTypes.BeginGetOtherTokens, authUri);
	},

	/**
	* Requests to close the authentication window
	* @method endGetOtherAuthToken
	*/
	endGetOtherAuthToken: function ()
	{
		this.postMessage(WindowMessageTypes.EndGetOtherTokens, null);
	}

	//#endregion

});

var keyManagerWindow = new KeyManagerWindow();

