/**
* The compose window scripts.
* @class ReadWindow
*/
var ReadWindow = new Class({

    //#region Fields

    /**
    * The guid of this frame.
    * @property guid
    * @type String
    */
    guid: '@GUID',

    //#endregion

    //#region Constructor

    /**
    * Constructs the compose window.
    * @method initialize.
    */
    initialize: function () {
        var self = this;

        // Fired when the document is done being loaded.
        jQuery(document).ready(function () {
            // Register the post message handler.
            window.addEventListener('message', function (event) { self.handlePostMessage(event); }, false);

            // Get the contents that will fill this message.
            jQuery(document.body).mask("Decrypting message...");
            self.postMessage(WindowMessageTypes.GetContents, null);
        });
    },

    //#endregion

    //#region Encryption

    /**
    * Takes a package and decrypts it.
    * @method decryptMessage
    * @param kiwiPackage {String} Decrypts the package string.
    * @param viewerId {String} Id of the viewer reading the message.
    * @param callback {Function} Function to call with decrypted package, passed the body of the email.
    */
    decryptMessage: function (kiwiPackage, viewerId, callback) {
        var self = this;
        try {
            var emailPackager = new EmailPackager(kiwiPackage);
        }
        catch (exception) {
            jQuery(document.body).mask("Invalid e-mail message.  Contact the sender and have them resend the message.", 'OK');
            return;
        }

        jQuery(document.body).mask("Retreiving decryption key...");
        KeyManagerProxy.getViewerKey(new KeyRequestArgs(emailPackager.keyServerUri, emailPackager.creatorId, viewerId, emailPackager.timestamp, EmailPackager.PackageKeySize),
            function (success, value, args) {
                if (!success) {
                    jQuery(document.body).mask("Failed to get an decryption key.  Try to open the message again, or contact the sender and have them resend the message.", 'OK');
                    return;
                }

                jQuery(document.body).mask("Decrypting message...");
                window.setTimeout(function () {
                    try {
                        emailPackager.unwrap(kiwiPackage, viewerId, value);

                        // Sanatize HTML.
                        var sanitizedHTML = html_sanitize(emailPackager.message.contents, self.urlSanitizer, self.idSanitizer);

                        // Make links open in new window.
                        var email = document.createElement('div');
                        email.innerHTML = sanitizedHTML;
                        jQuery('a', email).attr('target', '_blank');

                        callback(email.innerHTML);
                    }
                    catch (exception) {
                        jQuery(document.body).mask("Invalid e-mail message.  Contact the sender and have them resend the message.", 'OK');
                    }
                }, 1000);
            });
    },

    /**
    * Sanitizes URLs.
    */
    urlSanitizer: function(url) {
        if(/^(https?:\/\/|mailto:)/.test(url))
            return url;
        return url;
        if (/^data:image\//.test(url))
            return url;
        else return null;
    },

    /**
    * Sanitizes ids.
    */
    idSanitizer: function(id) {
        return id;
    },

    //#endregion

    //#region Messaging

    /**
    * Sends a message to the parent frame.
    * @method postMessage
    * @param messageType {Number} The type of message being sent.
    * @param content {Object} The object to be sent.
    */
    postMessage: function (messageType, content) {
        var self = this;
        window.setTimeout(function () { window.top.postMessage(JSON.stringify(new WindowMessage(self.guid, messageType, content)), '*'); }, 0);
    },

    /**
    * Handles messages passed to the window.
    * @method handlePostMessage
    * @param event {Event} Event object
    */
    handlePostMessage: function (event) {
        var self = this;
        var message = JSON.parse(event.data);

        if (message.guid != this.guid)
            return;

        // Handle the various message types.
        var info = message.contents;
        if (message.messageType == WindowMessageTypes.GetContents) {
            window.setTimeout(function () {
                try {
                    self.decryptMessage(Package.fromJsonObject(info.kiwiPackage), info.userId, function (message) {
                        jQuery("#messageBodyArea").html(message);
                        jQuery(document.body).unmask();
                        self.postMessage(WindowMessageTypes.SizingInfo,
                            { height: jQuery("#messageBodyArea").outerHeight(true) });
                    });
                }
                catch (exception) {
                    jQuery(document.body).mask("Invalid e-mail message.  Contact the sender and have them resend the message.", 'OK');
                    return;
                }
            }, 0);
        }
        else if (message.messageType == WindowMessageTypes.SizingInfo) {
            self.postMessage(WindowMessageTypes.SizingInfo,
                        { height: jQuery("#messageBodyArea").outerHeight(true) });
        }

    }

    //#endregion

});

// Initialize a class.
var readWindow = new ReadWindow();