﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Kiwi.WebServer
{

	/// <summary>
	/// Global runtime that handles parts of the ASP.NET request pipeline.
	/// </summary>
	public class Global : HttpApplication
	{
		/// <summary>
		/// Runs when the web application is first started. Sets up url routes and their handlers.
		/// </summary>
		protected void Application_Start(object sender, EventArgs e)
		{
			RegisterRoutes();
		}

		/// <summary>
		/// Registers routes.
		/// </summary>
		private void RegisterRoutes()
		{
			// If you want a different URL then change the string value
			RouteTable.Routes.Add(new Route("Bookmarklet.js", new ScriptGeneratorRouteHandler<BookmarkletScriptGenerator>()));
			RouteTable.Routes.Add(new Route("InPageServices.js", new ScriptGeneratorRouteHandler<InPageServicesScriptGenerator>()));
			RouteTable.Routes.Add(new Route("WindowServices.js", new ScriptGeneratorRouteHandler<WindowServicesScriptGenerator>()));
			RouteTable.Routes.Add(new Route("Proxy/", new ScriptGeneratorRouteHandler<Proxy>()));
		}

		#region Route Handlers

		/// <summary>
		/// Redirects request for the inpage service script to the proper generator.
		/// </summary>
		private class ScriptGeneratorRouteHandler<T> : IRouteHandler where T : IHttpHandler, new()
		{
			public IHttpHandler GetHttpHandler(RequestContext requestContext)
			{
				return new T();
			}
		}

		#endregion

	}

}