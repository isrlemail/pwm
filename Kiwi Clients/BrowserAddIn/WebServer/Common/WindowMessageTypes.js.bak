﻿/**
* This class represents the various message types.
* @class WindowMessageTypes
*/

var WindowMessageTypes = {

	/**
	* Request contents.
	* @property GetContents
	* @type Number
	*/
	GetContents: 1,

	/**
	* Cancel kiwi encryption
	* @property CancelEncryption
	* @type Number
	*/
	CancelEncryption: 2,

	/**
	* Saves a draft.
	* @property SaveDraft
	* @type Number
	*/
	SaveDraft: 3,

	/**
	* Sends a message.
	* @property SendMessage
	* @type Number
	*/
	SendMessage: 4,

    /**
	* Gets the recipients of a message.
	* @property GetRecipients
	* @type Number
	*/
	GetRecipients: 6,

	/**
	* Requests that a password prompt be displayed.
	* @property PasswordPromptRequest
	* @type Number
	*/
	PasswordPromptRequest: 7,

	/**
	* Returns the result of a password prompt.
	* @property PasswordPromptResult
	* @type Number
	*/
    PasswordPromptResult: 8

};