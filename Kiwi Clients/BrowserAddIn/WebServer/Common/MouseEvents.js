﻿var MouseEvents = {};

/**
* Simulates a mouse click.
* @method simulateMouseClick
*/
MouseEvents.simulateMouseClick = function (target) {
    this.performMouseAction(target, "mousedown");
    this.performMouseAction(target, "mouseup");
    this.performMouseAction(target, "click");
};

/**
* Perfoms a mouse aciton.
* @method performMouseAction
*/
MouseEvents.performMouseAction = function (target, eventName) {
    if (target == null)
        return;

    var event = target.ownerDocument.createEvent("MouseEvents");
    event.initMouseEvent(eventName, true, // can bubble
                true, // cancellable
                document.defaultView, 1, // clicks
                0, 0, // screen coordinates
                0, 0, // client coordinates
                false, false, false, false, // control/alt/shift/meta
                0, // button,
                null);
    target.dispatchEvent(event);
};