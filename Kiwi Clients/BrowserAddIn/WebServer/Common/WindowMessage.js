﻿/**
* Represents the messages being sent across the remoting boundary.
* @class WindowMessage
*/
var WindowMessage = new Class({

    /**
    * The guid of the window receiving the message.
    * @property guid
    * @type String
    */
    guid: null,

    /**
    * The type of the message.
    * @property messageType
    * @type Number
    */
    messageType: null,

    /**
    * The contents of the message.
    * @property contents
    * @type Object
    */
    contents: null,

    /**
    * Constructor
    * @method initialize
    * @param guid {String} The guid of the window receiving the message.
    * @param messageType {Number} The type of the message.
    * @param contents {Object} The contents of the message.
    */
    initialize: function (guid, messageType, contents) {
        this.guid = guid;
        this.messageType = messageType;
        this.contents = contents;
    }

});