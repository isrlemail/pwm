﻿/**
* The types of controllers the system supports.
* @class ControllerTypes
*/
var ControllerTypes = {
    
    /**
    * Means any controller matches this type.
    * @property any
    * @type String
    */
    any: "Any",

    /**Represents controllers for email.
    * @property email
    * @type String
    */
    email: "email"

};

/**
* This class represents an area of text that must be processed by Kiwi.
* @class Uris
*/
var Uris = {

    /**
     * Gets the domain of this script.
     * @property domainUri
     * @type String
     */
    domainUri: '@DOMAIN_URI',

    /**
     * Gets the URI of the proxy.
     * @property proxyUri
     * @type String
     */
    proxyUri: '@SERVER_URI/Proxy',
  
    /**
     * Gets the url for the read window.
     * @property readWindowUri
     * @type String
     */
    readWindowUri: '@SERVER_URI/ReadWindow.aspx',

    /**
     * Gets the url for the compose window.
     * @property composeWindowUri
     * @type String
     */
    composeWindowUri: '@SERVER_URI/ComposeWindow.aspx',

    /**
    * Gets the url for the Key Manager Window.
    * @property keyManagerWindowUri
    * @type String
    */
    keyManagerWindowUri: '@SERVER_URI/KeyManagerWindow.aspx',

    /**
    * Gets the url for the Pwm icon.
    * @property pwmIcon
    * @type String
    */
	pwmIcon: '@SERVER_URI/img/pwm.png',

    /**
    * Gets the close button image.
    * @property closeButton
    * @type String
    */
    closeButton: '@SERVER_URI/img/close.svg',

    /**
    * Gets the urls for the key servers.
    * @property keyServerUris
    * @type String
    */
    keyServerUris: [@TRUSTED_KEY_SERVERS],

};


/**
* This class represents the various message types.
* @class WindowMessageTypes
*/
var WindowMessageTypes = {

	/**
	* Request contents.
	* @property GetContents
	* @type Number
	*/
	GetContents: 1,

	/**
	* Cancel kiwi encryption
	* @property CancelEncryption
	* @type Number
	*/
	CancelEncryption: 2,

	/**
	* Saves a draft.
	* @property SaveDraft
	* @type Number
	*/
	SaveDraft: 3,

	/**
	* Sends a message.
	* @property SendMessage
	* @type Number
	*/
	SendMessage: 4,

    /**
	* Gets the recipients of a message.
	* @property GetRecipients
	* @type Number
	*/
	GetRecipients: 5,
    
    /**
	* Gets the saw tokens from email subjects.
	* @property GetSawTokens
	* @type Number
	*/
	GetSawTokens: 6,

	/**
	* Processes a message just received
	* @property MessageReceived
	* @type Number
	*/
	MessageReceived: 7,

	/**
	* Gets the token from other authentication forms
	* @property BeginGetOtherTokens
	* @type Number
	*/
	BeginGetOtherTokens: 8,

	/**
	* Ends the get other token process
	*/
	EndGetOtherTokens: 9,

    /**
    * Sends the sizing info for the overlay or vice-versa.
    */
	SizingInfo: 11,

    /**
    * Discard the draft.
    */
	DiscardDraft: 12,

    /**
    * The focus has changed to the overlay.
    */
	FocusChanged: 12,

};

/**
* The various authentication methods
* @class AuthenticationMethod
*/
var AuthenticationMethod = {

    /**
	* No authentication used.
	* @property None
	* @type Number
	*/
    None: 0,
    
    /**
	* Saw authentication used.
	* @property Saw
	* @type Number
	*/
    Saw: 1,
    
    /**
	* Password authentication used.
	* @property Password
	* @type Number
	*/
    Password: 2,
    
    /**
	* Other authentication used.
	* @property Facebook
	* @type Number
	*/
    Other: 3
};

/**
* The type of key request.
* @class KeyRequestType
*/
var KeyRequestType = {

    /**
	* Creator key is being requested.
	* @property CreatorKey
	* @type Number
	*/
    CreatorKey: 1,

    /**
	* Viewer key is being requested.
	* @property ViewerKey
	* @type Number
	*/
    ViewerKey: 2
};