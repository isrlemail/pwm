﻿/**
* A class that allows for javascript events.
* @class JSEvent
*/
var JSEvent = new Class({

    //#region Fields

    /**
    * The list of listeners listening to this object.
    * @property listeners
    * @type Array.<Function>
    */
    listeners: null,

    //#endregion

    //#region Constructor

    /**
    * Constructor.
    * @method initialize
    */
    initialize: function () {
        this.listeners = new Array();
    },

    //#endregion

    //#region Add/Remove/Fire Events

    /**
    * Adds a listener to the event
    * @method addListener
    * @param listener {Function} The function to call when this event fires.
    */
    addListener: function (listener) {
        if(typeof listener != "function")
            return;

        for (var i = 0; i < this.listeners.length; i++)
            if (this.listeners[i] == listener)
                return;
        this.listeners.push(listener);
    },

    /**
    * Removes a listener from the event
    * @method removeListener
    * @param listener {Function} The function to call when this event fires.
    */
    removeListener: function (listener) {
        for (var i = 0; i < this.listeners.length; i++)
            if (this.listeners[i] == listener) {
                this.listeners.splice(i, 1);
                break;
            }
    },

    /**
    * Fires the registered listeners.
    * @method fire
    */
    fire: function () {
        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i].apply(this, arguments);
        }
    }

    //#endregion

});
