﻿/**
 * Copyright (c) 2009 Sergiy Kovalchuk (serg472@gmail.com)
 * 
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *  
 * Following code is based on Element.mask() implementation from ExtJS framework (http://extjs.com/)
 *
 */
(function ($) {

    /**
    * Centers the item on the given element at its top.
    */
    $.fn.centerAtTop = function (element) {
        this.css("position", "absolute");
        this.css("top", "25px");
        this.css("left", Math.max(0, ((element.width() - $(this).outerWidth()) / 2) +
                                                    element.scrollLeft()) + "px");
        return this;
    }

    /**
    * Centers the item on the given element.
    */
    $.fn.center = function (element) {
        this.css("position", "absolute");
        this.css("top", Math.max(0, ((element.height() - $(this).outerHeight()) / 2) +
                                                    element.scrollTop()) + "px");
        this.css("left", Math.max(0, ((element.width() - $(this).outerWidth()) / 2) +
                                                    element.scrollLeft()) + "px");
        return this;
    }

    /**
 ﻿   * Displays loading mask over selected element(s). Accepts both single and multiple selectors.
 ﻿   *
 ﻿   * @param label Text message that will be displayed on top of the mask besides a spinner (optional). 
 ﻿   * ﻿  ﻿  ﻿  ﻿  If not provided only mask will be displayed without a label or a spinner.  ﻿ 
    * @param buttonText If set will add a button with the given text instead of the loading image.
 ﻿   * @param delay Delay in milliseconds before element is masked (optional). If unmask() is called 
 ﻿   *              before the delay times out, no mask is displayed. This can be used to prevent unnecessary 
 ﻿   *              mask display for quick processes.   ﻿  
 ﻿   */
    $.fn.mask = function (label, buttonText, delay) {
        $(this).each(function () {
            if (delay !== undefined && delay > 0) {
                var element = $(this);
                element.data("_mask_timeout", setTimeout(function () { $.maskElement(element, label) }, delay));
            } else {
                $.maskElement($(this), label, buttonText);
            }
        });
    };

    /**
 ﻿   * Removes mask from the element(s). Accepts both single and multiple selectors.
 ﻿   */
    $.fn.unmask = function () {
        $(this).each(function () {
            $.unmaskElement($(this));
        });
    };

    /**
 ﻿   * Checks if a single element is masked. Returns false if mask is delayed or not displayed. 
 ﻿   */
    $.fn.isMasked = function () {
        return this.hasClass("masked");
    };

    $.maskElement = function (element, label, buttonText) {

        //if this element has delayed mask scheduled then remove it and display the new one
        if (element.data("_mask_timeout") !== undefined) {
            clearTimeout(element.data("_mask_timeout"));
            element.removeData("_mask_timeout");
        }

        if (element.isMasked()) {
            $.unmaskElement(element);
        }

        if (element.css("position") == "static") {
            element.addClass("masked-relative");
        }

        element.addClass("masked");

        var maskDiv = $('<div class="loadmask"></div>');

        //auto height fix for IE
        if (navigator.userAgent.toLowerCase().indexOf("msie") > -1) {
            maskDiv.height(element.height() + parseInt(element.css("padding-top")) + parseInt(element.css("padding-bottom")));
            maskDiv.width(element.width() + parseInt(element.css("padding-left")) + parseInt(element.css("padding-right")));
        }

        //fix for z-index bug with selects in IE6
        if (navigator.userAgent.toLowerCase().indexOf("msie 6") > -1) {
            element.find("select").addClass("masked-hidden");
        }

        element.append(maskDiv);

        if (label !== undefined) {
            var maskMsgDiv = $([
                '<div class="loadmask-msg">',
                  '<div class="loadmask-msg-label">' + label + '</div>',
                  buttonText ?
                      '<div class="loadmask-msg-button">' + buttonText + '</div>':
                      '<img src="https://pwm.byu.edu/v1/img/loading.gif" />',
                '</div>'
            ].join(''));
            element.append(maskMsgDiv);

            // Clicking the button closes the dialog.
            $('.loadmask-msg-button', element).click(function () { $.unmaskElement(element); });

            // Keep the element centered.
            maskMsgDiv.centerAtTop(element);
            element.resize(function () {
                maskMsgDiv.centerAtTop(element);
            });

            maskMsgDiv.show();
        }

    };

    $.unmaskElement = function (element) {
        //if this element has delayed mask scheduled then remove it
        if (element.data("_mask_timeout") !== undefined) {
            clearTimeout(element.data("_mask_timeout"));
            element.removeData("_mask_timeout");
        }

        element.find(".loadmask-msg,.loadmask").remove();
        element.removeClass("masked");
        element.removeClass("masked-relative");
        element.find("select").removeClass("masked-hidden");
    };

})(jQuery);