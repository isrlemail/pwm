﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KeyManagerWindow.aspx.cs" Inherits="Kiwi.WebServer.KeyManagerWindow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="<%= Kiwi.WebServer.Utils.JQueryLibraryUri %>"></script>
    <script type="text/javascript" src="<%= Kiwi.WebServer.Utils.MooToolsLibraryUri %>"></script>
    <script type="text/javascript" src='<%= Page.ResolveUrl(string.Format("~/WindowServices.js?guid={0}&debug={1}&controllerType={2}", Request["guid"] ?? "", Request["debug"] ?? "false", Request["controllerType"] ?? "")) %>'></script>
</head>
<body>
</body>
</html>
