﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComposeWindow.aspx.cs" Inherits="Kiwi.WebServer.ComposeWindow" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Secure Compose Window</title>

    <!-- Add our core libraries. -->
    <script type="text/javascript" src="<%= Kiwi.WebServer.Utils.JQueryLibraryUri %>"></script>
    <script type="text/javascript" src="<%= Kiwi.WebServer.Utils.MooToolsLibraryUri %>"></script>


    <!-- Add boostrap libraries. -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    <!-- Add our page specific stuff. -->
    <link rel="stylesheet" type="text/css" href='<%= Page.ResolveUrl("~/css/ComposeWindow.css") %>' />
    <link rel="stylesheet" type="text/css" href='<%= Page.ResolveUrl("~/css/mask.css") %>' />
    <script type="text/javascript" src='<%= Page.ResolveUrl(string.Format("~/WindowServices.js?guid={0}&debug={1}", Request["guid"] ?? "", Request["debug"] ?? "false")) %>'></script>
</head>
<body>
    <div>
        <font size="2" face="arial, helvetica, sans-serif">
            <div id="editor"></div>
        </font>
    </div>
    <div id="toolbar" data-role="editor-toolbar" data-target="#editor">
        <div id="toolbarPopup" class="btn-toolbar" style="display: none;">
            <div id="fonts" class="btn-group dropup">
                <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Font" style="width: 122px; text-align: left;">
                    <span id="selectedFont">Sans Serif</span>
                    <b class="caret" style="float: right; margin-top: 7px;"></b>
                </a>
                <ul class="dropdown-menu scrollable-menu"></ul>
            </div>

            <div id="fontSizes" class="btn-group dropup">
                <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Font Size" style="width: 75px; text-align: left;">
                    <span id="selectedFontSize">Normal</span>
                    <b class="caret" style="float: right; margin-top: 7px;"></b>
                </a>
                <ul class="dropdown-menu">
                    <li><a data-edit="fontSize 1" data-fontsize="1"><font size="1">Small</font></a></li>
                    <li><a data-edit="fontSize 2" data-fontsize="2"><font size="2">Normal</font></a></li>
                    <li><a data-edit="fontSize 4" data-fontsize="4"><font size="4">Large</font></a></li>
                    <li><a data-edit="fontSize 6" data-fontsize="6"><font size="6">Huge</font></a></li>
                </ul>
            </div>
            <div class="btn-group dropup">
                <a class="btn btn-default btn-sm" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                <a class="btn btn-default btn-sm" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                <a class="btn btn-default btn-sm" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
            </div>
            <div id="alignments" class="btn-group dropup">
                <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Alignment"><i id="alignmentIcon" class="fa fa-align-left"></i>&nbsp;&nbsp;<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a></li>
                    <li><a data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a></li>
                    <li><a data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a></li>
                    <li><a data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a></li>
                </ul>
            </div>
            <div class="btn-group">
                <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                <a class="btn btn-default btn-sm" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-indent"></i></a>
                <a class="btn btn-default btn-sm" data-edit="indent" title="Indent (Tab)"><i class="fa fa-outdent"></i></a>
            </div>

            <div class="btn-group">
                <a class="btn btn-default btn-sm" data-edit="removeformat" title="Remove formatting"><i class="fa fa-eraser"></i></a>
            </div>
        </div>

        <div class="btn-toolbar" style="clear: both; padding-top: 5px;">
            <div class="btn-group">
                <a id="encrypt" class="btn btn-default btn-sm" title="Send encrypted email">Send encrypted</a>
            </div>

            <div class="btn-group">
                <a id="textToolsButton" class="btn btn-default btn-sm" title="Enable/disable text formatting tools"><i class="fa fa-text-width"></i></a>
            </div>

            <div class="btn-group dropup">
                <a class="btn btn-default btn-sm" title="Insert Hyperlink" id="linkButton"><i class="fa fa-link"></i></a>
                <input id="linkInput" type="text" data-edit="createLink" style="display: none;" />
                <a class="btn btn-default btn-sm" title="Remove Hyperlink" id="unlinkButton"><i class="fa fa-unlink"></i></a>
                <a class="btn btn-default btn-sm" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
            </div>

            <div class="btn-group pull-right">
                <a id="discard" class="btn btn-default btn-sm" title="Discard"><i class="fa fa-trash"></i></a>
            </div>

            <div class="btn-group pull-right">
                <a id="cancel" class="btn btn-default btn-sm" title="Pwm is encrypting this email. Clicking this button will turn off encryption.">Turn off encryption</a>
            </div>
        </div>

        <input id="upload" type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="display: none" />
        <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
    </div>

    <div id="linkMask" style="display: none;"></div>
    <div id="linkEntry" class="popover" style="display: none;">
        <div class="popover-title-div">
            <div class="popover-title">Edit Link</div>
            <img id="linkCancelCorner" src='<%= Page.ResolveUrl("~/img/close.svg") %>'>
        </div>

        <div class="popover-content">
            <div id="linkEntryFields">
                <div>
                    <label for="linkText">Text to display</label>
                    <span><input name="linkText" id="linkText" type="text" /></span>
                </div>
                <div>
                    <label for="linkURL">URL</label>
                    <span><input name="linkURL" id="linkURL" type="text" /></span>
                </div>
            </div>

            <a id="linkOK" class="btn btn-default btn-sm">Ok</a>
            <a id="linkCancel" class="btn btn-default btn-sm">Cancel</a>
        </div>
    </div>

    <script>
        (function ($) {
            // Setup the toolbar bindings.
            var fonts = [
                ['Sans Serif', 'arial, helvetica, sans-serif'],
                ['Serif', '\'times new roman\', serif'],
                ['Fixed Width', 'monospace, monospace'],
                ['Wide', '\'arial black\', sans-serif'],
                ['Narrow', '\'arial narrow\', sans-serif'],
                ['Comic Sans MS', '\'comic sans ms\', sans-serif'],
                ['Garamond', 'garamond, serif'],
                ['Georgia', 'georgia, serif'],
                ['Tahoma', 'tahoma, sans-serif'],
                ['Trebuchet MS', '\'trebuchet ms\', sans-serif'],
                ['Verdana', 'verdana, sans-serif']
            ];
            var fontTarget = $('[title=Font]').next('.dropdown-menu');
            $.each(fonts, function (idx, fontItem) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontItem[1] + '" data-font="' + fontItem[1] + '"><font face="' + fontItem[1] + '">' + fontItem[0] + '</font></a></li>'));
            });

            // Add tooltips
            $('[title]').tooltip({ container: 'body' }).on('click', function () { $(this).tooltip('hide'); });

            // Setup link creation, removal, and editing.
            var linkMask = $('#linkMask'), linkEntry = $('#linkEntry'),
                linkText = $('#linkText'), linkURL = $('#linkURL'), linkOK = $('#linkOK');

            var showLinkEntry = function (textToDisplay, URL) {
                linkMask.show();
                linkEntry.show();
                linkEntry.center($(document.body));

                updateTextToUrl = false;
                linkText.val(textToDisplay || '');
                linkURL.val(URL || '').trigger('input');
            };
            var hideLinkEntry = function () {
                linkMask.hide();
                linkEntry.hide();
            }

            var updateTextToUrl;
            linkText.on('input propertychange paste', function () {
                if (linkText.val() !== linkURL.val())
                    updateTextToUrl = false;
            });
            linkURL.on('input propertychange paste', function () {
                if (linkText.val() === '' || updateTextToUrl) {
                    updateTextToUrl = true;
                    linkText.val(linkURL.val());
                }
                if (linkURL.val() === '')
                    linkOK.attr('disabled', true);
                else
                    linkOK.attr('disabled', false);
            });
            linkText.add(linkURL).on('keypress', function (e) {
                if (e.keyCode == 13) {
                    linkOK.click();
                    return false;
                }
            });
            jQuery(document).on('keyup', function (e) {
                if (e.keyCode == 27) {
                    hideLinkEntry();
                    return false;
                }
            });

            // Add link button functionality.
            var getSelectedLinkTag = function () {
                var selection = window.getSelection();
                if (selection.anchorNode == null)
                    return null;
                var node = selection.anchorNode.nodeType == 3 ? selection.anchorNode.parentNode : selection.anchorNode;
                if (node.tagName.toUpperCase() === 'A' || $(node).parents('a').length > 0) {
                    return node.tagName.toUpperCase() === 'A' ? node : $(node).closest('a')[0];
                } else return null;
            };

            var unlinkFunction = function () {
                var node = getSelectedLinkTag();
                if (node != null) {
                    var range = document.createRange();
                    range.selectNode(node);
                    var selection = window.getSelection();

                    selection.removeAllRanges();
                    selection.addRange(range);

                    jQuery('#editor').focus();
                    document.execCommand('unlink');

                    selection.removeAllRanges();
                } else return;
            };

            var linkCurrentText, linkCurrentNode, linkSelection;
            var linkFunction = function (textToDisplay, URL) {
                var selection = window.getSelection();

                // Update the text.
                if (textToDisplay !== linkCurrentText) {
                    linkSelection.deleteContents();
                    linkSelection.insertNode(document.createTextNode(textToDisplay));
                }

                // Sanitize the URL.
                if (!/^(https?:\/\/|mailto:)/.test(URL))
                    URL = 'http://' + URL;

                // If we are editing a URL we behave differently.
                if (linkCurrentNode != null) {
                    linkCurrentNode.href = URL;
                } else {
                    $('#editor').focus();
                    selection.removeAllRanges();
                    selection.addRange(linkSelection);
                    document.execCommand('createLink', true, URL);
                }

                selection.removeAllRanges();
            };

            $('#linkButton').click(function () {
                var selection = window.getSelection();
                if (!selection.rangeCount)
                    return;

                linkCurrentNode = getSelectedLinkTag();
                var linkCurrentURL = '';
                if (linkCurrentNode != null) {
                    linkCurrentURL = linkCurrentNode.href || '';
                    var range = document.createRange();
                    range.selectNodeContents(linkCurrentNode);
                    selection.removeAllRanges();
                    selection.addRange(range);
                }

                linkCurrentText = selection.toString();
                linkSelection = selection.getRangeAt(0);
                showLinkEntry(linkCurrentText, linkCurrentURL);
            });
            $('#unlinkButton').click(function () {
                if (getSelectedLinkTag() != null) {
                    unlinkFunction();
                }
            });
            linkOK.on('click', function () { hideLinkEntry(); linkFunction(linkText.val(), linkURL.val()); });
            $('#linkCancel,#linkCancelCorner').on('click', function () { hideLinkEntry(); });

            // Add an option for speach input.
            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();
                $('#voiceBtn').css('position', 'fixed').offset({ top: editorOffset.top, left: editorOffset.left + $('#editor').innerWidth() - 35 });
            } else {
                $('#voiceBtn').hide();
            }

            // Add upload button handling.
            $($('#upload').data('target')).on('click', function () {
                document.getElementById('upload').click();
            });

            // Setup the editor.
            var toolbar = $('#toolbar'), editor = $('#editor');
            editor.wysiwyg({
                fileUploadError: function (reason, detail) {
                    var msg = '';
                    if (reason === 'unsupported-file-type')
                        msg = 'The selected file was not a supported image.';
                    else {
                        msg = 'Error uploading file: ' + reason + ', ' + detail;
                        console.log('error uploading file', reason, detail);
                    }
                    $(document.body).mask(msg, 'OK');
                }
            });

            // Set the editor to always take up the entire viewing area.
            var updateEditorView = function () {
                // Update the editor.
                var height = toolbar.offset().top - editor.offset().top;
                editor.css('min-height', height + 'px');
                editor.css('max-height', height + 'px');

                // Make sure the carrot or selection is still visible.
                var selection = window.getSelection();
                if (selection.anchorNode != null) {
                    var node = selection.anchorNode.nodeType == 3 ? selection.anchorNode.parentNode : selection.anchorNode;
                    node.scrollIntoViewIfNeeded();
                }

                // Adjust other elements as needed.
                $('.scrollable-menu').css('max-height', editor.height() + 'px');
                linkEntry.center($(document.body));
            };
            $(window).resize(updateEditorView);

            // Handle the text tools popup.
            var toolbarPopup = jQuery('#toolbarPopup');
            var textToolsButton = jQuery('#textToolsButton');
            var textToolsEnabled = window.localStorage['TextToolsEnabled'] || 'true';
            if (textToolsEnabled === 'true') {
                textToolsButton.addClass('active');
                toolbarPopup.show();
            }
            textToolsButton.on('click', function () {
                if (textToolsEnabled === 'true') {
                    textToolsEnabled = 'false';
                    textToolsButton.removeClass('active');
                    toolbarPopup.hide();
                } else {
                    textToolsEnabled = 'true';
                    textToolsButton.addClass('active');
                    toolbarPopup.show();
                }
                window.localStorage['TextToolsEnabled'] = textToolsEnabled;
                updateEditorView();
            });


            // Set focus to the editor window and update its view.
            editor.focus();
            updateEditorView();
        })(jQuery);
    </script>
</body>
</html>
