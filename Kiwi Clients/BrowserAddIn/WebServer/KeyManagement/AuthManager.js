﻿/**
* @class AuthManager
* This should be a static class.
*       A user has a preferred method of authenticating (password/SAW/OAth/etc.) which is respected when attemping to get an authentication token.
*       The algorithm to get an authentication token for key server KS is as follows:
*       - query KS for list of trusted Auth Servers AS
*       - pick an Auth Server at random and query it for its methods and URIs, store these in session storage
*       - if the preferred method of authentication is supported by this server
*       - use it
*       - else
*       - pick another Auth Server and repeat the process
**/
var AuthManager = (function () {

    var authManager = new Class({

        //#region Fields

        /**
        * The preferred method of authentication for this key manager.
        * @property preferedAuthenticationMethod
        * @type AuthenticationMethod
        */
        preferedAuthenticationMethod: AuthenticationMethod.Saw,

        //#endregion

        //#region Password auth

        /**
        * The callback to use to get a password request.
        * @property passwordRequestCallback
        * @type {Function}
        */
        passwordHandler: null,

        //#endregion

        //#region SAW Auth

        /**
        * Called when a saw token is requested.
        * @property sawTokenRequest
        * @type {Event}
        */
        sawTokenRequested: null,

        /**
        * Called when a saw token is received.
        * @property sawTokenReceived
        * @type {Event}
        */
        sawTokenReceived: null,

        /**
        * The wait time before we give up on getting a saw token back.
        * @property sawWaitTime
        * @type Number
        */
        sawWaitTime: 60000,

        /**
        * The event handler for when saw tokens are received.
        * @property sawEventHandler
        * @type {Event}
        */
        registerSawToken: function (transactionId, value) {
            this.sawTokenReceived.fire(transactionId, value);
        },

        //#endregion

        //#region Constructor

        /**
        * Constructor
        * @method initialize
        */
        initialize: function () {
            this.sawTokenRequested = new JSEvent();
            this.sawTokenReceived = new JSEvent();
        },

        //#endregion

        //#region Auth token retrieval

        /**
        * Gets an auth token.
        * @method getAuthToken
        * @param userId {String} The user id to get the auth token for.
        * @param keyServerUri {String} The Uri of the key server to get an auth token for.
        * @param callback {Function} Callback to call when this method has finished.
        */
        getAuthToken: function (userId, keyServerUri, callback) {
            var authToken = this.getCachedAuthToken(userId, keyServerUri);

            if (authToken != null) {
                callback(true, authToken);
                return;
            }

            // Request the auth servers that can be used.
            var self = this;
            jQuery.ajax({
                accepts: 'application/json',
                url: String.format('{0}?url={1}', Uris.proxyUri, encodeURIComponent(keyServerUri + 'AuthServers')),
                success: function (authServerUris) {
                    self.processAuthServers(userId, authServerUris, function (success, authToken) {
                        // Store the retrieved auth token.
                        if (success)
                            window.localStorage[self.getAuthTokenStorageKey(userId, keyServerUri)] = JSON.stringify(authToken.toJsonObject());

                        callback(success, authToken);
                    });
                },
                error: function () {
                    // Key server is not resonding.
                    callback(false, null);
                }
            });
        },

        /**
        * Processes the retrieved auth servers to get an auth token.
        * Attempts to use preferred methods first.
        * @param userId {String} The user id to get the auth token for.
        * @param authServerUris {String} The Uri of the key server to get an auth token for.
        * @param callback {Function} Callback to call when this method has finished.
        */
        processAuthServers: function (userId, authServerUris, callback) {
            var self = this;
            var remainingMethods = new Array();

            /* 
            * Loop to run over methods and Uris retrieved from auth server.
            * @method methodsAndUrisLoop
            * @param methods {Array.<Tupple>} Array to loop over.
            * @param i {Number} Position in array to check.
            * @param checkPreferred {Boolean} Whether to check if the method is preferred or not before running.
            * @param finished {Function} Function to call when finished.
            */
            var methodsAndUrisLoop = function (methods, i, checkPreferred, finished) {
                if (i == methods.length) {
                    finished();
                    return;
                }
                if (!checkPreferred || methods[i].m_Item1 == self.preferedAuthenticationMethod) {
                    var token = self.tryGetAuthToken(userId, methods[i].m_Item2, methods[i].m_Item1,
                        function (success, value) {
                            if (success) {
                                callback(success, value);
                                return;
                            }

                            methodsAndUrisLoop(methods, i + 1, checkPreferred, finished);
                        });
                }
                else {
                    if (checkPreferred) {
                        remainingMethods.push(methods[i]);
                    }
                    methodsAndUrisLoop(methods, i + 1, checkPreferred, finished);
                }
            };

            /* 
            * Loop over the auth server.
            * @method authServerLoop
            * @param i {Number} Position in array to check.
            */
            var authServerLoop = function (i) {
                // Fired only when no more methods exist.
                if (i == authServerUris.length) {
                    methodsAndUrisLoop(remainingMethods, 0, false, function () { callback(false, null); });
                    return;
                }

                jQuery.ajax({
                    accepts: 'application/json',
                    url: String.format('{0}?url={1}', Uris.proxyUri, encodeURIComponent(authServerUris[i] + 'Info/MethodsAndUris')),
                    success: function (methodsAndUris) {
                        methodsAndUrisLoop(methodsAndUris, 0, true, function () { authServerLoop(i + 1); });
                    },
                    error: function () {
                        authServerLoop(i + 1);
                    }
                });
            };
            authServerLoop(0);
        },

        /**
        * Tries to get a token from the given uri, with the given method.
        * @method tryGetAuthToken
        * @param userId {String} The user id to get the auth token for.
        * @param authUri {String} The Uri the service that gives auth token resides at.
        * @param method {AuthenticationMethod} The method the Uri is using to provide auth tokens.
        * @param callback {Function} Function to be called when this method is finished.
        */
        tryGetAuthToken: function (userId, authUri, method, callback) {
            var self = this;

            this.getAjaxAuthToken(authUri, userId, function (success, token) {
                if (!success) {
                    callback(false, null);
                    return;
                }

                try {
                    switch (method) {
                        case AuthenticationMethod.Saw:
                            // Setup a handler to accept the second half of the token.
                            var handler = function (transactionId, value) {
                                if (token.transactionId == transactionId) {
                                    token.combine(value, true);
                                    self.sawTokenReceived.removeListener(handler);
                                    callback(true, token);
                                }
                            };

                            // Get the token
                            self.sawTokenReceived.addListener(handler);
                            self.sawTokenRequested.fire(token.transactionId);

                            // If the token hasn't been complete in thirty seconds report failure.
                            window.setTimeout(function () { if (!token.isTokenComplete) callback(false, null); }, self.sawWaitTime);
                            break;

                        case AuthenticationMethod.Password:
                            if (self.passwordHandler) {
                                var password = self.passwordHandler(authUri);
                                if (password != null) {
                                    token.decryptWithPasswordKey(token.getPasswordKey(password));
                                    callback(true, token);
                                }
                                else {
                                    callback(false, null);
                                }
                            }
                            else {
                                callback(false, null);
                            }
                            break;

                        case AuthenticationMethod.None:
                            callback(true, token);
                            break;

                        default:
                            callback(false, null);
                            break;
                    }
                }
                catch (e) {
                    console.log("Caught exception in tryGetAuthToken: " + e);
                    callback(false, null);
                }
            });
        },

        /**
        * Gets an auth token from a server.
        * @method getAjaxAuthToken
        * @param authUri {String} The Uri of the the auth server.
        * @param userId {String} Id of the user.
        * @return {AuthToken} The auth token retrieved.  Most likely incomplete.
        */
        getAjaxAuthToken: function (authUri, userId, callback) {
            jQuery.ajax({
                type: 'POST',
                accepts: 'application/json',
                contentType: 'application/json',
                url: String.format('{0}?url={1}', Uris.proxyUri, encodeURIComponent(authUri)),
                data: JSON.stringify(userId),
                success: function (authToken) {
                    try {
                        callback(true, AuthToken.fromJsonObject(authToken));
                    }
                    catch (e) {
                        console.log("Caught exception in getAjaxAuthToken: " + e);
                        callback(false, null);
                    }
                },
                error: function () {
                    console.log("Unknown error in getAjaxAuthToken");
                    callback(false, null);
                }
            });
        },

        /**
        * Gets an auth token that has be retrieved in the past.
        * @method getCachedAuthToken
        * @param userId {String} The user id to get the auth token for.
        * @param keyServerUri {String} The Uri of the key server to get an auth token for.
        * @return {AuthToken} The retrieved auth token, or null if their is not one.
        */
        getCachedAuthToken: function (userId, keyServerUri) {
            // Get the auth token from storage.
            var authTokenKey = this.getAuthTokenStorageKey(userId, keyServerUri);
            var authToken = window.localStorage[authTokenKey];
            if (authToken == null)
                return null;

            var authToken = AuthToken.fromJsonObject(JSON.parse(authToken));
            if (!authToken.period.isCurrent()) {
                window.localStorage.removeItem(authTokenKey);
                return null;
            }
            return authToken;

        } .protect(),

        /**
        * Gets the local storage key for an auth token.
        * @method getAuthTokenStorageKey
        * @param userId {String} The user id to get the auth token for.
        * @param keyServerUri {String} The Uri of the key server to get an auth token for.
        * @return {String} The key to look in local storage for to get the auth token.
        */
        getAuthTokenStorageKey: function (userId, serverUri) {
            return String.format('AUTH_TOKEN|{0}|{1}', userId.toLowerCase(), serverUri);
        }

        //#endregion

    });

    return new authManager();
})();
