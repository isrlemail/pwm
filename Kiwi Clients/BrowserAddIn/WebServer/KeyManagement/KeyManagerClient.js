﻿/**
* A fascade used to access the key manager that lives in a different frame.
* @class KeyManagerClient
* @constructor
*/
var KeyManagerClient = (function () {
    var keyManagerClient = new Class({

        /**
        * Regular expression for parsing the session storage requests.
        * @property requestRegex
        * @type XRegExp
        */
        requestRegex: new XRegExp("^KeyRequest\\|(?<type>\\d)\\|(?<id>\\d+)", 'i'),

        /**
        * Adds the window event listener that will get key responses from
        * the key manager overlay.
        * @method initialize
        */
        initialize: function () {
            var self = this;

            window.addEventListener("storage", function (e) {
                // We only care about new entries in session storage.
                // TODO: I do not like using !!e.oldValue, but I have to since IE sets e.oldValue to '' when it should be null.
                if (e.storageArea !== window.sessionStorage || !e.newValue || !!e.oldValue)
                    return;

                // See if the result is something we are waiting for.
                var results = self.requestRegex.exec(e.key);
                if (results) {
                    var args = JSON.parse(e.newValue);

                    // Switch on the key request type.
                    switch (parseInt(results.type)) {
                        case KeyRequestType.CreatorKey:
                            KeyManager.getCreatorKey(args.creatorId, args.keyServerUri, args.keySize, Date.parseJson(args.timestamp), function (success, value) {
                                window.setTimeout(function () { window.sessionStorage[String.format("KeyResponse|{0}", results.id)] = JSON.stringify({ success: success, value: value }); }, 0);
                            });
                            break;

                        case KeyRequestType.ViewerKey:
                            KeyManager.getViewerKey(args.creatorId, args.viewerId, args.keyServerUri, args.keySize, Date.parseJson(args.timestamp), function (success, value) {
                                window.setTimeout(function () { window.sessionStorage[String.format("KeyResponse|{0}", results.id)] = JSON.stringify({ success: success, value: value }); }, 0);
                            });
                            break;
                    }

                    window.setTimeout(function () { window.sessionStorage.removeItem(e.key); }, 0);
                }
            }, false);
        }

    });

    return new keyManagerClient();
})();