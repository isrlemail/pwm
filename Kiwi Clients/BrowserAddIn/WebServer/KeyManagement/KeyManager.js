﻿/**
* @class KeyManager
* This should be a static class.
*       A user has a preferred method of authenticating (password/SAW/OAth/etc.) which is respected when attemping to get an authentication token.
*       The algorithm to get an authentication token for key server KS is as follows:
*       - query KS for list of trusted Auth Servers AS
*       - pick an Auth Server at random and query it for its methods and URIs, store these in session storage
*       - if the preferred method of authentication is supported by this server
*       - use it
*       - else
*       - pick another Auth Server and repeat the process
**/
var KeyManager = (function () {

    var keyManager = new Class({

        //#region Key Retreival

        /**
        * Gets the creator key.
        * @method getCreatorKey
        * @param creatorId {String} The Id of the creator the key is for.
        * @param serverUri {String} The key server to use.
        * @param keySize {Number} The size of the key to retrieve.
        * @param timestamp {Date} The timestamp to get the key for.
        * @param callback {Function} Function to be called with the results of this method.
        **/
        getCreatorKey: function (creatorId, keyServerUri, keySize, timestamp, callback) {
            this.getKey(KeyRequestType.CreatorKey, creatorId, null, keyServerUri, keySize, timestamp,
                this.getCreatorKeyStorageKey(keyServerUri, creatorId, keySize), callback);
        },

        /**
        * Gets the viewer key.
        * @method getViewerKey
        * @param creatorId {String} The Id of the creator the key is for.
        * @param viewerId {String} The Id of the viewer the key is for.
        * @param serverUri {String} The key server to use.
        * @param keySize {Number} The size of the key to retrieve.
        * @param timestamp {Date} The timestamp to get the key for.
        * @param callback {Function} Function to be called with the results of this method.
        **/
        getViewerKey: function (creatorId, viewerId, keyServerUri, keySize, timestamp, callback) {
            this.getKey(KeyRequestType.ViewerKey, creatorId, viewerId, keyServerUri, keySize, timestamp,
                this.getViewerKeyStorageKey(keyServerUri, creatorId, viewerId, keySize), callback);
        },

        /**
        * Gets a key from the key server.
        * @method getCreatorKey
        * @param keyType {KeyRequestType} The type of key being requested.
        * @param creatorId {String} The Id of the creator the key is for.
        * @param viewerId {String} The Id of the viewer the key is for.
        * @param serverUri {String} The key server to use.
        * @param keySize {Number} The size of the key to retrieve.
        * @param timestamp {Date} The timestamp to get the key for.
        * @param storageKey {String} Key to retrieve key before.
        * @param callback {Function} Function to be called with the results of this method.
        **/
        getKey: function (keyType, creatorId, viewerId, keyServerUri, keySize, timestamp, storageKey, callback) {
            var self = this;

            var encryptionKey = this.getCachedKey(storageKey, timestamp);
            if (encryptionKey != null) {
                callback(true, encryptionKey);
                return;
            }

            // Get an auth token to make the request.
            AuthManager.getAuthToken(keyType == KeyRequestType.CreatorKey ? creatorId : viewerId, keyServerUri, function (success, authToken) {
                if (!success) {
                    callback(false, null);
                    return;
                }

                // Get a key from the server.
                jQuery.ajax({
                    accepts: 'application/json',
                    contentType: 'application/json',
                    url: String.format('{0}?url={1}', Uris.proxyUri, encodeURIComponent(keyServerUri + 'Key')),
                    type: 'POST',
                    data: JSON.stringify(new KeyRequest(authToken, creatorId, viewerId, timestamp, keySize, keyType).toJsonObject()),
                    success: function (periodAndKey) {
                        encryptionKey = Base64.decodeStringToByteArray(periodAndKey.m_Item1);
                        var period = PeriodBase.fromJsonObject(periodAndKey.m_Item2);
                        self.cacheKey(storageKey, period, encryptionKey);
                        callback(true, encryptionKey);
                    },
                    error: function () {
                        callback(false, null);
                    }
                });
            });
        },

        /**
        * Gets a cached key.
        * @method getCachedKey
        * @param storageKey {String} Key to retrieve key before.
        * @param timestamp {DateTime} Timestamp the key needs to be valid for.
        * @return {Array.<Byte>} The key that was in storage.
        */
        getCachedKey: function (storageKey, timestamp) {
            var existingKeys = window.localStorage[storageKey];
            if (existingKeys == null)
                return null;

            // Attempt to find a key that is valid for the given timestamp.
            existingKeys = JSON.parse(existingKeys);
            for (var period in existingKeys) {
                var actualPeriod = PeriodBase.fromJsonObject(JSON.parse(period));

                if (actualPeriod.validForDate(timestamp))
                    return Base64.decodeStringToByteArray(existingKeys[period]);
            }

            return null;
        },

        /**
        * Caches a key at the given location.
        * @method cacheKey
        * @param storageKey {String} Key to store key with.
        * @param period {PeriodBase} Period the key is valid for.
        * @param encryptionKey {Array.<Byte>} The actual encryption key.
        */
        cacheKey: function (storageKey, period, encryptionKey) {
            // Get the current set of keys from local storage.
            var existingKeys = window.localStorage[storageKey];
            if (existingKeys == null)
                existingKeys = {};
            else
                existingKeys = JSON.parse(existingKeys);

            // Add the key to the set.
            existingKeys[JSON.stringify(period.toJsonObject())] = Base64.encodeByteArray(encryptionKey);

            // Write into local storage the keys.
            window.localStorage[storageKey] = JSON.stringify(existingKeys);
        },

        /**
        * Get the key used to store creator keys.
        * @method getViewerKeyStorageKey
        * @param keyServerUri Uri of the key server.
        * @param creatorId {String} Id of the creator.
        * @param keySize {Number} Size of the key.
        * @return {String} Key matching keys are stored in local storage under.
        */
        getCreatorKeyStorageKey: function (keyServerUri, creatorId, keySize) {
            return String.format('CK|{0}|{1}|{2}', keyServerUri, creatorId, keySize);
        },

        /**
        * Get the key used to store viewer keys.
        * @method getViewerKeyStorageKey
        * @param keyServerUri Uri of the key server.
        * @param creatorId {String} Id of the creator.
        * @param viewerId {String} Id of the viewer.
        * @param keySize {Number} Size of the key.
        * @return {String} Key matching keys are stored in local storage under.
        */
        getViewerKeyStorageKey: function (keyServerUri, creatorId, viewerId, keySize) {
            return String.format('VK|{0}|{1}|{2}|{3}', keyServerUri, creatorId, viewerId, keySize);
        },

        //#endregion

    });

    return new keyManager();
})();
