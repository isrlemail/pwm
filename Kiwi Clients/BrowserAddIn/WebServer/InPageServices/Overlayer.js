﻿/**
* JavaScript code that runs in the context of the page.
* @module InPageServices
*/

/**
* Responsible for keeping window objects over the elements they are supposed to overlay.
* @class Overlayer
*/
var Overlayer = (function ()
{
	var overlayer = new Class({

		//#region Properties

		/**
		* Collection of windows managers for which this class is overlaying content.
		* @property windowManagers
		* @type [WindowManager]
		*/
	    windowManagers: null,

	    /**
        * The size of borders on the iframes.
        */
	    borderSize: 0,

		//#endregion

		//#region Constructor

		/**
		* Constructor.
		* @method initialize
		*/
		initialize: function ()
		{
			var self = this;
			this.windowManagers = new Array();

			/*var resize = function ()
			{
				for (var i = 0; i < self.windowManagers.length; i++)
				{
					self.updateOverlay(self.windowManagers[i]);
				}

				// Continue running this code every 1/10 of a second.
				window.setTimeout(resize, 10);
			};*/
			//resize();
		},

		//#endregion

		//#region Private overlay methods

		/**
		* Updates the sizing and position of overlays. This needs to be the window manager.
		* @method updateOverlay
		* @param {WindowManager} WindowManager Manager to update.
		*/
		updateOverlay: function ()
		{
		    var windowManager = (this instanceof WindowManager) ? this : Overlayer.getWindowManager(this);
		    if (windowManager == null)
		        return;

		    if (windowManager.refreshScheduled)
		        return;
		    windowManager.refreshScheduled = true;

		    window.setTimeout(function () {
		        var frame = jQuery(windowManager.frame);
		        var element = jQuery(windowManager.overlayedElement);

		        // Match the frame's size to the element's size.
		        var position = element.position();
		        var maxHeight = element.offsetParent().height() - position.top + windowManager.getHeightOffset();
		        var maxWidth = element.offsetParent().width() - position.left + windowManager.getWidthOffset();
		        frame.height(Math.min(element.outerHeight(true), maxHeight) - Overlayer.borderSize);
		        frame.width(Math.min(element.outerWidth(true), maxWidth) - Overlayer.borderSize);

		        // Set the frame position to match the element
		        frame.css({ "top": position.top, "left": position.left });
		        windowManager.refreshScheduled = false;
		    }, 10);
		},

		/**
		* Adds a window manager to the set of managers being overlaid, and
		* overlays the desired element for the window manager.
		* @method addOverlay
		* @param {WindowManager} WindowManager Manager to overlay.
		*/
		addOverlay: function (windowManager)
		{
			var frame = jQuery(windowManager.frame);
			var element = jQuery(windowManager.overlayedElement);

			// Make the element invisible so that scrolling looks correct.
			element.data("originalOpacity", element.css("opacity"));
			element.css({ opacity: 0 });

			element.before(frame);

			jQuery(windowManager.overlayedElement).observe('childList attributes subtree', this.updateOverlay);
			this.updateOverlay.apply(windowManager);
		} .protect(),

		/**
		* Removes a window manager to the set of managers being overlaid, and
		* removes the overlay if the parent element still exists on the page.
		* @method removeOverlay
		* @param {WindowManager} windowManager Manager to be removed.
		*/
		removeOverlay: function (windowManager)
		{
		    jQuery(windowManager.overlayedElement).disconnect('childList attributes subtree', this.updateOverlay);
			var element = jQuery(windowManager.overlayedElement);
			jQuery(element).css({ opacity: element.data("originalOpacity") });
			jQuery(windowManager.frame).remove();
			windowManager.cleanup();
		} .protect(),

		//#endregion

		//#region Public overlay methods

		/**
		* Adds a window manager to the set of managers being overlaid, and
		* overlays the desired element for the window manager.
		* @method addIndividualOverlay
		* @param {WindowManager} windowManager Manager to overlay.
		*/
		addIndividualOverlay: function (windowManager)
		{
			this.addOverlay(windowManager);
			this.windowManagers.push(windowManager);
		},

		/**
		* Removes a window manager to the set of managers being overlaid, and
		* removes the overlay if the parent element still exists on the page.
		* @method removeIndividualOverlay
		* @param {WindowManager} windowManager Manager to be removed.
		*/
		removeIndividualOverlay: function (windowManager)
		{
			var index = this.windowManagers.indexOf(windowManager);
			this.removeOverlay(windowManager);
			this.windowManagers.splice(index, 1);
		},

		/**
		* Sets the overlays to be taken care of, replacing the old set.
		* @method setOverlays
		* @param {[WindowManager]} windows The new set of windows to be overlaying.
		*/
		setOverlays: function (windows)
		{

			// Go through the set of windows, if the window is already
			// overlaid remove it from the current list of overlays, so
			// that it won't get deleted, otherwise the overlay is new and
			// needs to be added.
			var i;
			for (i = 0; i < windows.length; i++)
			{
				var position = this.windowManagers.indexOf(windows[i]);
				if (position != -1)
				{
					delete this.windowManagers[position];
				}
				else
				{
					this.addOverlay(windows[i]);
				}
			}

			// Delete the old overlays that are no longer existent.
			for (i = 0; i < this.windowManagers.length; i++)
			{
				if (this.windowManagers[i])
				{
					this.removeOverlay(this.windowManagers[i]);
				}
			}

			// Update the set of window managers.
			this.windowManagers = windows;
		},

		/**
		* Gets the window manager for the given element.
		* @method getWindowManager
		* @param {Element} element Element to be checked.
		* @return {WindowManager} The windowManager that overlays the element.
		*/
		getWindowManager: function (element)
		{
			for (var i = 0; i < this.windowManagers.length; i++)
			{
				if (this.windowManagers[i].guid === jQuery(element).data("guid"))
				{
					return this.windowManagers[i];
				}
			}
			return null;
		}

		//#endregion

	});
	return new overlayer();
})();
