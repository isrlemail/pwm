/**
 *	A base class for all secure windows.  Can only be used by the overlayer once, as it
 *  is the overlay and not this class that actually adds the element to the document.
 *  @class WindowManager
 */
var WindowManager = new Class({

    //#region Properties

    /**
     * Keeps track of the element being overlaid by the window manager.
     * @property overlayedElement
     * @type Element
     */
    overlayedElement: null,

    /**
     * The main parent window element.  This is used to let the overlayer detect when
     * the overlay window is extending beyond the parent window and clip it.
     * @property clippingBoundariesElement
     * @type Element
     */
    clippingElement: null,

    /**
    * Keeps track of the frame displaying the secure window.
    * @property frame
    * @type IFrame
    */
    frame: null,

    /**
    * A guid value for this instance.
    * @property guid
    * @type String
    */
    guid: null,

    /**
    * The url of the window of secure content.
    * @property url
    * @type String
    */
    url: null,

    /**
    * Whether or not the code is in debug mode.
    * @property debug
    * @type bool
    */
    debug: @DEBUG,

    /**
     * An object that holds all of the message handlers.
     * @property messageHandlers
     * @type Object
     */
    messageHandlers: {},

    //#endregion

    //#region Constructor

    /**
    * Constructor
    * @method initialize
    * @param {Element} overlayedElement The element the secure window overlays.
    * @param {string} url URL to the secure content window.
	* @param {string} controllerType Type of communication system this key manager window is being instantiated in.
    */
    initialize: function (overlayedElement, url, controllerType) {
        this.guid = Math.random().toString().substring(2);
        this.url = String.format("{0}?guid={1}&debug={2}&controllerType={3}", url, this.guid, this.debug, (controllerType != null) ? controllerType : ControllerTypes.any);
        this.overlayedElement = overlayedElement;
        this.frame = document.createElement("iframe");
        this.frame.setAttribute("src", this.url);

        // Assigns a guid to the element for tracking purposes.
        jQuery(this.overlayedElement).data("guid", this.guid);
    },

    //#endregion

    //#region Utility methods.

    /**
    * Get a height offset by which to increase the height of the overlay.
    * @method getHeightOffset
    * @return {Number} Number of pixels to increase the height of the overlay.
    */
    getHeightOffset: function() {
        return 0;
    },

    /**
    * Get a width offset by which to increase the width of the overlay.
    * @method getWidthOffset
    * @return {Number} Number of pixels to increase the width of the overlay.
    */
    getWidthOffset: function() {
        return 0;
    },

    //#endregion

    //#region Window messaging

    /**
     * Registers a function to receive all messages of a certain type.
     * @method registerMessageHandler
     * @param {string} messageType The type of message the callback will be receiving.
     * @param {function} handler The function to be called when a message of messageType is received.
     * The function is of the form function(windowMessage : WindowMessage).
    */
    registerMessageHandler: function (messageType, handler) {
        this.messageHandlers[messageType] = handler;
    } .protect(),

    /** 
     * Called when a message has been received.
     * @method handleMessage
     * @param {WindowMessage} The window message that was received.
     */
    handleMessage: function(windowMessage) {
        if(windowMessage.guid != this.guid) {
            return;
        }

        if(this.messageHandlers[windowMessage.messageType]) {
            this.messageHandlers[windowMessage.messageType].call(this, windowMessage);
        }
    },

    /**
     * Posts a message to the frame this window class manages.
     * @method postMessage
     * @param {string} messageType The type of message the callback will be receiving.
     * @param {Object} content The message to be sent.
    */
    postMessage: function (messageType, content) {
        this.frame.contentWindow.postMessage(
            JSON.stringify(new WindowMessage(this.guid, messageType, content)), Uris.domainUri);
    },

    /**
     * Cleans up the manager.
     * @method cleanup
     */
    cleanup: function() {
    }

    //#endregion
});
