/**
* This class represents an area of text that must be processed by Kiwi.
* @class ReadArea
* @constructor
*/

var ReadArea = new Class({

    /**
    * The main read area
    * @property element
    * @type Element
    */
    element: null,

    /**
    * @method initialize
    * @param {Element} element an element comprising an area of text
    */
    initialize: function (element) {
        this.element = jQuery(element);
    },

    /**
    * @method getFrom
    * @return {string} the email of the sender of the text comprised by this area
    */
    getFrom: function () {
        return this.element.closest('span[email]').eq(0).attr('email');
    },

    /**
    * @method getBody
    * @return {string} the text comprised by this area
    */
    getBody: function () {
        //return this.element.html();
        return jQuery('div[name="pwmPackage"]', this.element).html();
    },

    /**
    * @method getElement
    * @return {Element} The overlayed element.
    */
    getElement: function () {
        return jQuery('div[name="encryptedEmail"]', this.element);
    },

    /**
    * Check if this read area has been clipped.
    */
    isClipped: function () {
        return jQuery('.vem', this.element).length > 0;
    },

    /**
    * Allow us to intelligently handle clipped areas.
    */
    prepareClippedArea: function () {
        var pwmPackage = jQuery('[name=pwmPackage]', this.element);
        if (pwmPackage.is(':visible')) {
            pwmPackage.parent().prepend(jQuery.parseHTML('<div style="font-weight: bold;">The encrypted message is very large and Gmail has clipped it. Please click on the link below to view this message.</div>'));
            jQuery('.vem', this.element).on('click', function () { 
                alert('This encrypted email will now open in a new window. You will have to click on the Pwm bookmarklet in this new window to decrypt the message.');
            });
            pwmPackage.hide();
        }
    }

});
