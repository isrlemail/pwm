/**
* This class represents an inbox item of a Kiwi message.

* @class SubjectItem
* @constructor
*/

var SubjectItem = new Class({

    /**
    * The overlayed element
    * @property element
    * @type Element
    */
    element: null,

    /**
    * @method initialize
    * @param {Element} element an element that contains this SubjectItem, spatially
    */
    initialize: function (element) {
        this.element = element;
    },

    /**
    * @method getSubject
    * @return {string} the subject of the email represented by this SubjectItem
    */
    getSubject: function () {
        return this.element.innerText;
    },

    /**
    * @method setSubject
    * @param {string} subject the subject to display by the email represented by this SubjectItem
    */
    setSubject: function (subject) {
        this.element.innerText = subject;
    },

    /**
    * @method getElement
    * @return {Element} The overlayed element.
    */
    getElement: function () {
        return this.element;
    }
});

