/**
* An encrypted email system.
* @module Kiwi
*/

/**
* This class provides access to SubjectItems, ReadAreas, and a ComposeArea on Gmail pages.
* @class PageScanner
* @constructor
*/
var PageScanner = new Class({

    /**
    * The tutuorial for this page scanner.
    */
    tutorial: null,
    /**
    * @method initialize
    * @param {function} callback A function called after the PageScanner is ready
    */
    initialize: function () {
        this.tutorial = new Tutorial();
    },

    /**
    * Get location to store icon.
    */
    getIconLocation: function() {
        return jQuery('div.gb_Ia');
    },

    /**
    * @method getSubjectArea
    * @return {object} A SubjectArea or null if there isn't one on the page
    */
    getSubjectAreas: function () {
        var subjectArea = jQuery('.hP,body > div.bodycontainer div.maincontent > table:nth-child(1) b');
        if (subjectArea.length > 0)
            return [new SubjectArea(subjectArea[0])];
        else return [];
    },

    /**
    * @method getReadAreas
    * @return {array[object]} An array of ReadAreas on the page
    */
    getReadAreas: function () {
        return jQuery.map(jQuery('.ads.adn .a3s:has(div[name="pwmPackage"]),body>div.bodycontainer div:has([name=pwmPackage]):last'), function (item) { return new ReadArea(item); });
        //return jQuery.map(jQuery('div[name="pwmPackage"]'), function (item) { return new ReadArea(item); });
    },

    /**
    * This method recognizes two types of ComposeAreas: those that appear as the result of "Compose ..." and those that appear at the end of a threaded message.
    * Fortunately, the elements of interest can be accessed identically relative to a carefully selected root element. If Gmail DOM changes break this at some
    * point in the future, we should consider writing a ComposeViewComposeArea and a ThreadViewComposeArea class that implement our ComposeArea interface instead
    * of trying to glean common structure from the pages.
    * @method getComposeAreas
    * @return {array[object]} an array of ComposeAreas on the page
    */
    getComposeAreas: function () {
        // Need to check for new style compose windows
        return jQuery.map(jQuery('.I5'), function (e) { return new ComposeArea(e); });
    },

    /**
    * @method getSubjectItems
    * @return {array[object]} an array of SubjectItems on the page
    */
    getSubjectItems: function () {
        return jQuery.map($('table.F.cf.zt div.y6 > span'), function (e) { return new SubjectItem(e); });
    }
});

/**
* A regular expression for finding e-mail addresses.
* @property emailRegex
* @type RegExp
*/
PageScanner.emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;

/**
* Gets the email address for the current user.
* @method getEmailAddress
* @return {String} The user's email id.
*/
PageScanner.getEmailAddress = function () {
    // GLOBALS is an array of interesting information that Google provides.
    try {
        return GLOBALS[10];
    } catch (e) {
        return $('body > div.bodycontainer > table:nth-child(1) b').text().match(PageScanner.emailRegex)[0];
    }
};

/**
* Gets the subjects for unread messages.
* @method getNewEmailSubjects
* @param transactionIds {Array.<String>} List of transaction Ids to check for.
* @param callback {Function} Method to call with retrieved tokens.
*/
PageScanner.getNewEmailSubjects = function (transactionIds, callback) {
    //console.log("getNewEmailSubjects with transaction ids: " + transactionIds);
    jQuery.ajax({
        type: 'GET',
        // /\/mail\/u\/(\d+)\//
        url: location.pathname + 'feed/atom',
        dataType: 'xml',
        success: function (xml) {
            var subjects = new Array();

            jQuery('entry', xml).each(function () {
                var subject = jQuery(this).find('title').eq(0).text();
                // Only handle emails with transaction Ids we care about.
                for (var i = 0; i < transactionIds.length; i++) {
                    if (subject.contains(transactionIds[i])) {
                        subjects.push(subject);
                        PageScanner.deleteEmail(this);
                    }
                }
            });

            callback(subjects);
        }
    });
};

/**
* Deletes the email represented by the XML entry.
* @method getNewEmailSubjects
*/
PageScanner.deleteEmail = function (entry) {
    var messageIdRegex = new XRegExp('message_id=(?<MessageId>[a-f0-9]+)');
    var authTokenRegex = new XRegExp('GMAIL_AT=(?<AT>[^;]+)');

    var messageId = messageIdRegex.exec(jQuery(entry).find('link').eq(0).attr('href')).MessageId;
    var authToken = authTokenRegex.exec(document.cookie).AT;

    // Send the delete request.
    jQuery.ajax({
        type: 'GET',
        url: String.format(location.pathname + '?act=tr&at={0}&t={1}', authToken, messageId),
    });
};
