/**
* This class represents the area on the page associated with composing a message.
* It has a few responsibilities. First, like other page areas, it needs to provide
* the DOM element to be overlaid. Second, it provides an interface to get/set the
* primary recipient (to), copied recipients (cc), the subject and the body text of
* the underlying elements. Fourth, it provides an interface to send a message or save
* a draft. Fifth, it makes and keeps hidden the Gmail interface elements from which we
* wish to intercept or disable functionality (such as send, save, etc.).
* The ComposeArea has an element field and a getElement accessor. The element field
* references a DOM element that is an ancestor of the to, cc, and subject fields and
* body element. The getElement accessor returns the element that is overlaid.
*
* @class ComposeArea
* @constructor
*/
var ComposeArea = new Class({

    //#region Properties

    /**
    * The overlayed element.
    * @property element
    * @type Element
    */
    element: null,

    /**
    * A regular expression for finding e-mail addresses.
    * @property emailRegex
    * @type RegExp
    */
    emailRegex: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i,

    /**
    * Offset for hight needed to compensate for larger view.
    */
    heightOffset: 0,

    //#endregion

    //#region Constructor

    /**
    * @method initialize
    * @param {Element} element an element comprising the compose area to be overlayed
    */
    initialize: function (element) {
        this.element = element;

        // Fired when this compose area is moved into the maximized window.
        var self = this;
        this.maximizedViewChanged = function () {
            if (jQuery(self.element).parents('.aSt').length > 0) {
                self.getUnsupportedElements().hide();
                self.disableEditorSizing();
                self.heightOffset = -47;
            } else {
                self.getUnsupportedElements().show();
                self.enableEditorSizing();
                self.heightOffset = 0;
            }
        };
    },

    //#endregion

    //#region Email form access methods

    /**
    * @method getElement
    * @return {Element} The overlayed element.
    */
    getElement: function () {
        return jQuery('table.iN', this.element);
    },

    /**
    * @method getClippingElement
    * @return {Element} The parent element.
    */
    getClippingElement: function () {
        return this.element;
    },

    /**
    * @method getRecipients
    * @return Array.<string> The recipients.
    */
    getRecipients: function () {
        var emailRegex = this.emailRegex;
        var recipients = jQuery('input[name="to"],input[name="cc"],input[name="bcc"]', this.element).filter(function () {
            return this.value.test(emailRegex);
        });
        return jQuery.map(recipients, function (item) { return item.value.match(emailRegex)[0]; });
    },

    /**
    * @method getSubject
    * @return {string} The value of the 'subject' field.
    */
    getSubject: function () {
        return jQuery("input[name='subjectbox']", this.element)[0].value;
    },

    /**
    * @method setSubject
    * @param {string} to The value of the 'subject' field.
    */
    setSubject: function (subject) {
        jQuery("input[name='subjectbox']", this.element)[0].value = subject.trim();
    },

    // Get's the preamble.
    getPreamble: function () {
        return jQuery('.composePreamble > textarea', this.element).val();
    },

    // Set's the preamble.
    setPreamble: function (preamble) {
        if (preamble && preamble.trim)
            jQuery('.composePreamble > textarea', this.element).val(preamble.trim());
    },

    /**
    * @method getBodyElement
    * @return {Element} the body element for this compose area
    */
    getBodyElement: function () {
        return jQuery('div[contentEditable]', this.element);
    },

    /**
    * @method getBody
    * @return {string} The inner text of the body field.
    */
    getBody: function () {
        return this.getBodyElement().html();
        //innerHTML.replace(/<wbr>/g, '').replace(/<\/wbr>/g, '').replace(/<br\s*[\/]?>/gi, "");
    },

    /**
    * Sets the body of the message.
    * @method setBody
    * @param {string} The inner text of the body field.
    */
    setBody: function (body) {
        this.getBodyElement().html(body);
    },

    /**
     * Checks whether this compose area is a reply to an encrypted message.
     * @method isEncryptedReply
     * @return {Boolean} Whether this is a reply to an encrypted message.
     */
    isEncryptedReply: function () {
        var parentElement = jQuery(this.element).closest('.h7.Jux0I.j23lnd');
        if (!parentElement)
            return false;
        return jQuery('div[name="encryptedEmail"]', parentElement).length != 0;
    },

    /**
     * Setup the area to support enabling encryption.
     */
    setup: function (clickHandler) {
        var lockBar = new Element("div", { "class": "pwmBar" });

        var lockEncryption = new Element("div", { "class": "pwmLockDiv", style: 'display: none;' });
        lockEncryption.innerText = 'Encrypted';

        // Span with info about wether the element is currently locked or not.
        var lockBarInfo = new Element("span", {
            "class": "pwmInfo",
            "data-encrypted-message": "Pwm is encrypting this message",
            "data-unencrypted-message": "Pwm is not encrypting this message"
        });
        lockBarInfo.innerText = lockBarInfo.attributes["data-unencrypted-message"].value;

        var lockButton = new Element("div", { "class": "pwmLockButton" });
        lockButton.innerText = 'Turn on Encryption';
        lockButton.addEventListener('click', clickHandler, false);

        jQuery(lockBar).append(lockEncryption, lockBarInfo, lockButton);
        jQuery(".fX.aXjCH", this.element).before(lockBar);

        // Change the send button.
        jQuery('div.aoO[role="button"]', this.element).html('Send unencrypted');
        jQuery('div.aA4', this.element).css('left', '140px');
    },

    isSetup: function () {
        return jQuery('.pwmBar', this.element).length > 0;
    },

    /**
     * Enable encryption for this area.
     */
    enable: function () {
        jQuery('.pwmLockButton:visible', this.element).click();
    },

    //#endregion

    //#region Button Manipulators

    /**
    * Gets the send button.
    * @method getSendButton
    * @return {Element} The send button (or gmail's approximation of a button)
    */
    getSendButton: function () {
        //With new compose window no longer need to go up to parents
        return jQuery("div:contains('Send')[role='button']", this.element)[0];
    },

    /**
    * Clicks the send button in this form.
    * @method clickSendButton
    */
    clickSendButton: function () {
        this.ensureRichTextMode();
        MouseEvents.simulateMouseClick(this.getSendButton());
    },

    /**
    * Clicks the save draft button in this form.
    * @method clickSendButton
    */
    clickSaveDraftButton: function () {
        this.ensureRichTextMode();

        // Emulate changing the document to enable the save draft button, and then click it.
        var currentScrollHeight = jQuery('.Tm.aeJ').scrollTop();

        var event = document.createEvent('KeyboardEvent');
        event.initKeyboardEvent('keydown', true, true, null, 'Enter', 0, null, null, null);

        this.getBodyElement()[0].dispatchEvent(event);
        //jQuery('.Tm.aeJ').scrollTop(currentScrollHeight);

        //window.setTimeout(function () { console.log('here'); jQuery('.Tm.aeJ').scrollTop(currentScrollHeight); }, 1000);
    },

    /**
    * Simulates a mouse click to expand the reply text.
    */
    expandReply: function () {
        var expander = jQuery('.ajT', this.element);
        if (expander.length > 0)
            MouseEvents.simulateMouseClick(expander[0]);
    },

    /**
    * Discards the draft.
    */
    discardDraft: function () {
        var expander = jQuery('.og.T-I-J3', this.element);
        if (expander.length > 0)
            MouseEvents.simulateMouseClick(expander[0]);
    },

    //#endregion

    //#region Element Manipulation

    /**
    * Ensure that the message is in rich text mode.
    */
    ensureRichTextMode: function () {
    // Ensure that we are in rich-text mode.
    var parentMenu = jQuery(this.element).closest(".gB,[role=dialog]").first();
    var plainTextSelector = parentMenu.find('[role=menuitemcheckbox]:contains("Plain text mode")');

    // If the menu has never been shown, we need to generate it.
    if (plainTextSelector.length == 0) {
        MouseEvents.simulateMouseClick(jQuery('.Xv[role=button]', parentMenu)[0]);
        MouseEvents.simulateMouseClick(jQuery('.Xv[role=button]', parentMenu)[0]);
        plainTextSelector = parentMenu.find('[role=menuitemcheckbox]:contains("Plain text mode")');
    }

    this.originalCheckedState = plainTextSelector.hasClass('J-Ks-KO');
    if (this.originalCheckedState)
        MouseEvents.simulateMouseClick(plainTextSelector[0]);
    },

    /**
     * Prepare the area to use encrypted email.
     */
    prepareArea: function () {
        var self = this;

        // Placeholder update methods.
        var subjectBox = jQuery('input[name="subjectbox"]', this.element);
        this.subjectBlur = function () {
            subjectBox.prop('placeholder', 'Subject - The subject will not be encrypted.');
        }
        var recipientBox = jQuery('textarea[name="to"]', this.element);
        this.recipientBlur = function () {
            recipientBox.prop('placeholder', 'This message will be encrypted for these recipients.');
            window.setTimeout(self.recipientBlur, 0);
        }

        // Store original placeholder values
        this.originalSubjectPlaceholder = subjectBox.prop('placeholder');
        this.originalToPlaceHolder = recipientBox.prop('placeholder');

        // Update the current values and add listeners.
        subjectBox.on('blur', this.subjectBlur).on('focus', this.subjectBlur);
        recipientBox.on('blur', this.recipientBlur).on('focus', this.recipientBlur);
        this.subjectBlur();
        this.recipientBlur();

        // Add a preamble if not an encrypted reply.
        if (!this.isEncryptedReply() && jQuery('div.composePreamble', this.element).length == 0) {
            jQuery(".aoD.az6:last", this.element).after([
                '<div class="aoD az6 composePreamble">',
		            '<textarea class="aoT art" ' +
                        'placeholder="Greeting (Optional) - Add an unencrypted personal greeting at the start of your email. This lets recipients know that your message is genuine and not spam."' +
                        'spellcheck="true" tabindex="1" />',
	            '</div>'
            ].join(''));
        }
        jQuery('div.composePreamble', this.element).show();

        // Change message box text.
        var pwmEncrypted = jQuery(".pwmLockDiv", this.element);
        pwmEncrypted.show();
        var pwmInfo = jQuery(".pwmInfo", this.element);
        pwmInfo.text(pwmInfo.data("encrypted-message"));
        var pwmLockButton = jQuery(".pwmLockButton", this.element);
        pwmLockButton.hide();

        // Watch for us entering a maximized state.
        this.enableEditorSizing();
        this.hideUnsupportedElements();
        jQuery(document.body).observe('childlist', '.I5', this.maximizedViewChanged);
        this.maximizedViewChanged();
    },

    /**
     * Revert the area to non-encrypted mod.
     */
    revertArea: function () {
        // Reset place holder values.
        jQuery('input[name="subjectbox"]', this.element).prop('placeholder', this.originalSubjectPlaceholder);
        jQuery('textarea[name="to"]', this.element).prop('placeholder', this.originalToPlaceHolder);
        jQuery('.oL.aDm', this.element).text(this.originalRecipientPlaceholder);

        // Remove handlers
        jQuery('input[name="subjectbox"]', this.element).off('blur', this.subjectBlur).off('focus', this.subjectBlur);
        jQuery('textarea[name="to"],.oL.Adm', this.element).off('blur', this.recipientBlur).off('focus', this.recipientBlur);

        // Hide preamble div.
        jQuery('div.composePreamble', this.element).hide();

        // Change message box text.
        var pwmEncrypted = jQuery(".pwmLockDiv", this.element);
        pwmEncrypted.hide();
        var pwmInfo = jQuery(".pwmInfo", this.element);
        pwmInfo.text(pwmInfo.data("unencrypted-message"));
        var pwmLockButton = jQuery(".pwmLockButton", this.element);
        pwmLockButton.show();

        // Stop watching for us entering a maximized state.
        this.disableEditorSizing();
        this.showUnsupportedElements();
        jQuery(document.body).disconnect('childlist', '.I5', this.maximizedViewChanged);
    },

    /**
    * Enable sizing of the editor.
    */
    enableEditorSizing: function () {
        if (this.editorSizingEnabled)
            return;
        this.editorSizingEnabled = true;

        var tableElement = this.getElement();
        this.originalDisplay = tableElement.css('display') || '';
        this.originalHeight = tableElement.css('height') || '';
        var height = jQuery(window).height() < 750 ? '375px' : '475px';
        tableElement.css({
            'display': 'block',
            'height': height,
        });
    },

    /**
    * Disable sizing of the editor.
    */
    disableEditorSizing: function () {
        var tableElement = this.getElement();
        tableElement.css({
            'display': this.originalDisplay,
            'height': this.originalHeight,
        });
    },

    /**
    * Get's elements that should not be shown while the overlay is being shown.
    * @method getUnsupportedElements
    */
    getUnsupportedElements: function () {
        return jQuery('table.iN tr:nth-child(2)', this.element);
    },

    /**
    * Removes the fixed positioning class.
    */
    removeFixedPositioningClass: function () {
        var item = jQuery(this);
        if (item.hasClass('aDi')) {
            item.disconnect({ attributes: true, attributeFilter: ['class'] }, arguments.callee);
            item.data('originalClass', this.className);
            item.removeClass('aDi');
            item.observe({ attributes: true, attributeFilter: ['class'] }, arguments.callee);
        }
    },

    /**
     * Fired when the maximized view changed function fires.
     */
    maximizedViewChanged: null,

    /**
    * Hide's elements that should not be shown while the overlay is being shown.
    * @method hideUnsupportedElements
    */
    hideUnsupportedElements: function () {
        var self = this;
        this.getUnsupportedElements().hide();

        var elements = jQuery('.aDj', this.element);
        elements.observe({ attributes: true, attributeFilter: ['class'] }, this.removeFixedPositioningClass);
        elements.each(function () {
            jQuery(this).data('originalClass', this.className);
            self.removeFixedPositioningClass.call(this);
        });

        jQuery(".pwmLockButton", this.element).css("visibility", "hidden");
    },

    /**
    * Shows elements that were hidden when the overlay was not being shown.
    * @method showUnsupportedElements
    */
    showUnsupportedElements: function () {
        this.getUnsupportedElements().show();

        var elements = jQuery('.aDj', this.element);
        elements.disconnect({ attributes: true, attributeFilter: ['class'] }, this.removeFixedPositioningClass); var self = this;
        elements.each(function () {
            this.className = jQuery(this).data('originalClass');
        });

        jQuery(".pwmLockButton", this.element).css("visibility", "visible");
    },

    //#endregion

});

