﻿/**
* The email controller loads the pagescanner, ', and overlay
* instances. It also registers event listeners so that it will be notified when
* the page changes. Whenever the page changes, the controller removes old overlays
* and instantiates new ones for the current page.
* @class Controller
* @constructor
*/
var Tutorial = new Class({

    startTemplate: [
        "<div class='popover tour'>",
            "<div class='arrow'></div>",
            "<div class='popover-title-div'>" +
                "<h3 class='popover-title'></h3>",
                "<img src='" + Uris.closeButton + "' data-role='end' title='Exit Tutorial' />" +
            "</div>" +
            "<div class='popover-content'></div>",
            "<div class='popover-navigation'>",
                "<div class='btn-group'>",
                    "<button class='btn btn-sm btn-default disabled' data-role='prev'>« Prev</button>",
                    "<button class='btn btn-sm btn-default' data-role='next'>Next »</button>",
                "</div>",
                "<button class='btn btn-sm btn-default' data-role='end'>Skip tutorial</button>",
            "</div>",
        "</div>"
    ].join("\n"),

    defaultTemplate: [
        "<div class='popover tour'>",
            "<div class='arrow'></div>",
            "<div class='popover-title-div'>" +
                "<h3 class='popover-title'></h3>",
                "<img src='" + Uris.closeButton + "' data-role='end' title='Exit Tutorial' />" +
            "</div>" +
            "<div class='popover-content'></div>",
            "<div class='popover-navigation'>",
                "<div class='btn-group'>",
                    "<button class='btn btn-sm btn-default' data-role='prev'>« Prev</button>",
                    "<button class='btn btn-sm btn-default' data-role='next'>Next »</button>",
                "</div>",
            "</div>",
        "</div>"
    ].join("\n"),

    disabledPrev: [
        "<div class='popover tour'>",
            "<div class='arrow'></div>",
            "<div class='popover-title-div'>" +
                "<h3 class='popover-title'></h3>",
                "<img src='" + Uris.closeButton + "' data-role='end' title='Exit Tutorial' />" +
            "</div>" +
            "<div class='popover-content'></div>",
            "<div class='popover-navigation'>",
                "<div class='btn-group'>",
                    "<button class='btn btn-sm btn-default disabled' data-role='prev'>« Prev</button>",
                    "<button class='btn btn-sm btn-default' data-role='next'>Next »</button>",
                "</div>",
            "</div>",
        "</div>"
    ].join("\n"),

    finishedTemplate: [
        "<div class='popover tour'>",
            "<div class='arrow'></div>",
            "<div class='popover-title-div'>" +
                "<h3 class='popover-title'></h3>",
                "<img src='" + Uris.closeButton + "' data-role='end' title='Exit Tutorial' />" +
            "</div>" +
            "<div class='popover-content'></div>",
            "<div class='popover-navigation'>",
                "<div class='btn-group'>",
                    "<button class='btn btn-sm btn-default' data-role='prev'>« Prev</button>",
                    "<button class='btn btn-sm btn-default disabled' data-role='next'>Next »</button>",
                "</div>",
                "<button class='btn btn-sm btn-default' data-role='end'>Finished</button>",
            "</div>",
        "</div>"
    ].join("\n"),

    actionTemplate: [
        "<div class='popover tour'>",
            "<div class='arrow'></div>",
            "<div class='popover-title-div'>" +
                "<h3 class='popover-title'></h3>",
                "<img src='" + Uris.closeButton + "' data-role='end' title='Exit Tutorial' />" +
            "</div>" +
            "<div class='popover-content'></div>",
        "</div>"
    ].join("\n"),

    /**
    * Whether to enable listeners for the other tutorials on exit.
    */
    enableListenersOnFinish: false,

    /**
    * Tracks whether the listeners have already been setup or not.
    */
    alreadySetup: false,

    /**
    * Keeps track of the current tour.
    */
    currentTour: null,

    /**
    * Tutorial initializer
    * @method initialize
    */
    initialize: function () {
        var self = this;

        var defaultTourOptions = {
            debug: true,

            // Allow generic instructions that do not highlight a specific item.
            orphan: true,

            // Only allow interaction with the intended icon.
            backdrop: true,

            // Turn off storage, so the tutorial always starts from the begining.
            storage: false,

            // Turn off keyboard navagation of the tutorial.
            keyboard: false,

            // HTML template for the tutorial.
            template: this.defaultTemplate
        };

        // Create the base tour.
        this.baseTutorial = new Tour(jQuery.extend({}, defaultTourOptions, {
            name: 'baseTutorial',
            steps: [{
                title: 'Welcome to Pwm!',

                content: '<p>Pwm will help protect your email.</p>',
                template: this.startTemplate,
                onShown: function () { },
                onHidden: function () { }
            }, {
                element: '#pwmIcon:eq(0)',
                placement: 'bottom',
                title: 'Pwm is running',

                content: '<p>This icon is always visible when Pwm is running.</p>' + 
                    '<p>In the future, click here if you need help.</p>',
                template: this.finishedTemplate
            }],
            onStart: function (tour) {
                localStorage.setItem('priorPwmUserBase', true);
            },
            onEnd: function (tour) {
                self.currentTour = null;
                if (self.enableListenersOnFinish)
                    self.setupListeners();
                self.enableListenersOnFinish = false;
            },
            onShown: function (tour) {
                self.updateTutorialMask();
            },
            onHidden: function (tour) {
                self.removeUpdatedTutorialMask();
            }
        }));

        // Create the Compose tour.
        this.composeTutorial = new Tour(jQuery.extend({}, defaultTourOptions, {
            name: 'composeTutorial',
            steps: [{
                element: 'div[role="button"]:contains("COMPOSE"):eq(0)',

                title: 'Click compose',
                content: '<p>Click the compose button to begin.</p>',
                template: this.actionTemplate,

                // Move to the next step when the compose button is clicked.
                onShown: function (tour) {
                    tour.checkForComposeInterfaceInterval = window.setInterval(function () {
                        if (jQuery('.pwmBar:visible').length > 0) {
                            window.clearInterval(tour.checkForComposeInterfaceInterval);
                            tour.checkForComposeInterfaceInterval = null;
                            tour.next();
                        }
                    }, 10);
                },
                onHidden: function (tour) {
                    window.clearInterval(tour.checkForComposeInterfaceInterval);
                }
            }, {
                element: '[role="dialog"] .pwmBar:visible:last',
                placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

                title: 'Click "Turn on encryption"',
                content: '<p>Clicking "Turn on encryption" will encrypt this email.</p>' +
                    '<p>Click this before typing your email to prevent Gmail from reading it while you compose it.</p>',
                template: this.actionTemplate,

                // Move to the next step when the compose button is clicked.
                onShown: function (tour) {
                    self.updateTutorialMask();

                    tour.checkForComposeWindowInterval = window.setInterval(function () {
                        if (jQuery('.pwmComposeOverlay:visible').length > 0) {
                            window.clearInterval(tour.checkForComposeWindowInterval);
                            tour.checkForComposeWindowInterval = null;
                            window.setTimeout(function () { tour.next(); }, 50);
                        }
                    }, 10);
                },
                onHidden: function (tour) {
                    self.removeUpdatedTutorialMask();

                    window.clearInterval(tour.checkForComposeWindowInterval);
                }
            }, {
                element: '[role="dialog"] .fX.aXjCH:visible:last,[role="dialog"] .hl:visible:last',
                placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

                title: 'Add recipients',
                content: '<p>This email can only be read by the recipients you specify.</p>' +
                    '<p>Even Gmail cannot read your encrypted email.</p>',
                template: this.disabledPrev,

                // Move to the next step when the Next button is clicked.
                onShown: function (tour) {
                    self.updateTutorialMask();
                    jQuery(tour._options.steps[tour._current].element).closest('form').find('.fX.aXjCH,.hl').addClass('tour-step-backdrop');
                },
                onHide: function (tour) {
                    jQuery(tour._options.steps[tour._current].element).closest('form').find('.tour-step-backdrop').removeClass('tour-step-backdrop');
                }
            }, {
                element: '[role="dialog"] .aoD.az6:not(.composePreamble):visible:last',
                placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

                title: 'Enter a subject',
                content: 'The subject is not encrypted, so avoid putting anything sensitive here.</p>',
            }, {
                element: '[role="dialog"] .aoD.az6.composePreamble:visible:last',
                placement: function () { return self.pickValueBasedOnMaximized('bottom', 'left'); },

                title: 'Add an optional greeting',
                content: '<p>Greetings are not encrypted and help recipients who don\'t use Pwm to know this email isn\'t spam.</p>'
            }, {
                element: '[role="dialog"] .pwmComposeOverlay:visible:last',
                placement: function () { return self.pickValueBasedOnMaximized('top', 'left'); },

                title: 'Write your message',
                content: '<p>Anything written in this box is encrypted.</p>',
                template: this.finishedTemplate
            }, {
                title: 'Instructions',
                content: '<p>Encrypted email automatically includes instructions on how to decrypt it.</p>',
                template: this.finishedTemplate
            }],
            onStart: function (tour) {
                localStorage.setItem('priorPwmUserCompose', true);
            },
            onEnd: function (tour) {
                self.currentTour = null;
            },
            onShown: function (tour) {
                self.updateTutorialMask();
            },
            onHidden: function () {
                self.removeUpdatedTutorialMask();
            }
        }));

        this.readTutorial = new Tour(jQuery.extend({}, defaultTourOptions, {
            name: 'readTutorial',
            steps: [{
                title: 'Open encrypted email',
                content: '<p>Open an encrypted email to begin.</p>',

                template: this.actionTemplate,
                backdrop: false,

                onShown: function (tour) {
                    tour.checkForReadWindowInterval = window.setInterval(function () {
                        if (jQuery('.pwmReadOverlay:visible').length > 0) {
                            window.clearInterval(tour.checkForReadWindowInterval);
                            tour.checkForReadWindowInterval = null;
                            tour.next();
                        }
                    }, 10);
                },
                onHidden: function (tour) {
                    window.clearInterval(tour.checkForReadWindowInterval);
                }
            }, {
                element: '.pwmLockDiv:visible:first',
                placement: 'bottom',

                title: 'Encrypted label',
                content: '<p>This label means an email is encrypted.</p>',
                template: this.startTemplate,
            }, {
                element: '.ix td:nth-child(1) h3:nth-child(1):visible:first',
                placement: 'bottom',

                title: 'Verified sender',
                content: '<p>Pwm verifies that an email is really from the address it says it is.</p>',
            }, {
                element: '.adz .ajw:visible:first',
                placement: 'bottom',

                title: 'Only readable by recipients',
                content: '<p>An encrypted email can only be read by its recipients.</p>' +
                    '<p>Even Gmail cannot read your encrypted email.</p>',

                onHidden: function (tour) {
                    tour.prevStep = tour._current;
                }
            }, {
                element: 'div[name=preamble]:visible:first',
                placement: 'top',

                title: 'Unencrypted greeting',
                content: '<p>Authors can include an unencrypted greeting to give context.</p>',

                onShown: function (tour) {
                    // If there is no preamble skip this step.
                    if (jQuery('div[name=preamble]:visible').length == 0) {
                        if (tour.prevStep == 3)
                            tour.goTo(5);
                        else
                            tour.goTo(3);
                    }
                }
            }, {
                element: '.pwmReadOverlay:visible:first',
                placement: 'top',

                title: 'Encrypted email',
                content: '<p>Text within this box was decrypted by Pwm.</p>' +
                    '<p>Pwm ensures that the text hasn\'t been tampered with.</p>',

                onHidden: function (tour) {
                    tour.prevStep = tour._current;
                }
            }, {
                title: 'Encrypted replies',
                content: '<p>Replies to encrypted email are automatically encrypted.</p>' +
                    '<p>To protect the original email contents, encryption cannot be turned off for replies.</p>',
                template: this.finishedTemplate
            }],
            onStart: function (tour) {
                localStorage.setItem('priorPwmUserRead', true);
                if (jQuery('.pwmReadOverlay:visible').length > 0)
                    tour.goTo(1);
            },
            onEnd: function (tour) {
                self.currentTour = null;
            }
        }));

        this.inboxTutorial = new Tour(jQuery.extend({}, defaultTourOptions, {
            name: 'inboxTutorial',
            steps: [{
                element: '.gbqfqw:visible:eq(0)',
                placement: 'bottom',

                title: 'Finding encrypted email',
                content: '<p>To find encrypted email, search for <span style="white-space: pre; font-family: monospace;">subject: encrypted</span> and any words in the subject or greeting.</p>' +
                    '<p>Because these email are encrypted, you can\'t find them by searching for their contents.</p>'
            }, {
                element: '.pwmLockDiv:visible:first',
                placement: 'bottom',

                title: 'Encrypted label',
                content: '<div class="pwmLockDiv" style="display: none">Encrypted</div>' +
                    '<p>The above label means an email is encrypted.</p>' +
                    '<p>Pwm must be running to read encrypted email.</p>',
                template: this.finishedTemplate,
                onShown: function (tour) {
                    self.updateTutorialMask();

                    if (jQuery(tour._options.steps[tour._current].element).length == 0)
                        jQuery('.popover.tour .pwmLockDiv').show();
                },
            }],
            onStart: function (tour) {
                localStorage.setItem('priorPwmUserInbox', true);
                window.location.hash = '#search/subject%3A+encrypted';
            },
            onEnd: function (tour) {
                self.currentTour = null;
            },
            onShown: function () {
                self.updateTutorialMask();
            },
            onHidden: function () {
                self.removeUpdatedTutorialMask();
            }
        }));

        this.baseTutorial.init();
        this.composeTutorial.init();
        this.readTutorial.init();
        this.inboxTutorial.init();
    },

    initialRun: function () {
        if (!localStorage.priorPwmUserBase) {
            this.enableListenersOnFinish = true;
            this.playBaseTutorial();
        } else {
            this.setupListeners();
        }
    },

    /**
    * Play the base tutorial.
    */
    playBaseTutorial: function () {
        if (this.currentTour != null)
            return;
        this.currentTour = this.baseTutorial;

        this.baseTutorial.restart();
    },

    /**
    * Walks the user through the process of using Pwm to compose a message.
    * @method playComposeTutorial
    */
    playComposeTutorial: function (skipToStepTwo) {
        if (this.currentTour != null)
            return;
        this.currentTour = this.composeTutorial;

        var self = this;
        this.minimizeComposeWindows();

        // Play the tutorial.
        if (skipToStepTwo) {
            var checkForComposeInterfaceInterval = window.setInterval(function () {
                if (jQuery('.pwmBar:visible').length > 0) {
                    window.clearInterval(checkForComposeInterfaceInterval);
                    checkForComposeInterfaceInterval = null;
                    
                    self.composeTutorial.restart();
                    self.composeTutorial.goTo(1);
                }
            }, 10);
        } else
            this.composeTutorial.restart();

    },

    playReadTutorial: function () {
        if (this.currentTour != null)
            return;
        this.currentTour = this.readTutorial;

        this.minimizeComposeWindows();
        this.readTutorial.restart();
    },

    playInboxTutorial: function () {
        if (this.currentTour != null)
            return;
        this.currentTour = this.inboxTutorial;

        var self = this;
        this.minimizeComposeWindows();

        self.inboxTutorial.restart();
    },

    /**
    * Minimize the compose windows.
    */
    minimizeComposeWindows: function () {
        jQuery('div.nH.Hd').filter(function () { return jQuery(this).children('div.nH:nth-child(3):visible').length; }).find('.l.m').each(function () { MouseEvents.simulateMouseClick(this) });
    },

    /**
    * Update the tutorial mask position to work better in gmail.
    */
    updateTutorialMask: function () {
        var stepElement = jQuery(this.currentTour._options.steps[this.currentTour._current].element);
        stepElement.after(jQuery('.tour-step-background'));
        stepElement.after(jQuery('.tour-backdrop'));


        // Cover up the gmail icon.
        var gmailIcon = jQuery('div.akh');
        if (!gmailIcon.is(stepElement) && gmailIcon.has(stepElement).length == 0)
            gmailIcon.css('z-index', 0);

        // Cover up the banner.
        var banner = jQuery('div[role=banner]');
        var bannerHeight = banner.height();
        var backdrop = jQuery('.tour-backdrop');
        backdrop.css('top', bannerHeight);

        // Cover everything but the current element.
        var extraDivHTML = '<div class="pwmExtraMask tour-backdrop"></div>';
        if (banner.is(stepElement)) {
            return;
        } else if (banner.has(stepElement).length > 0) {
            var offset = stepElement.offset();

            // Left item.
            var extraDiv = jQuery(jQuery.parseHTML(extraDivHTML));
            extraDiv.height(bannerHeight);
            extraDiv.width(offset.left);
            jQuery(document.body).append(extraDiv);

            // Right item.
            extraDiv = jQuery(jQuery.parseHTML(extraDivHTML));
            extraDiv.height(bannerHeight);
            extraDiv.css('left', offset.left + stepElement.width());
            jQuery(document.body).append(extraDiv);

            // Top item.
            extraDiv = jQuery(jQuery.parseHTML(extraDivHTML));
            extraDiv.height(offset.top - 1);
            extraDiv.width(stepElement.width());
            extraDiv.css('top', 0);
            extraDiv.css('left', offset.left);
            jQuery(document.body).append(extraDiv);

            // bottom item.
            extraDiv = jQuery(jQuery.parseHTML(extraDivHTML));
            extraDiv.height(bannerHeight - offset.top - stepElement.height() + 1);
            extraDiv.width(stepElement.width());
            extraDiv.css('top', offset.top + stepElement.height());
            extraDiv.css('left', offset.left);
            jQuery(document.body).append(extraDiv);
        } else {
            var extraDiv = jQuery(jQuery.parseHTML(extraDivHTML));
            extraDiv.height(bannerHeight);
            jQuery(document.body).append(extraDiv);
        }
    },

    /**
    * Remove the div elements created during updateTutorialMask.
    */
    removeUpdatedTutorialMask: function () {
        jQuery('div.akh').css('z-index', '');
        jQuery('.pwmExtraMask').remove();
    },

    /**
    * Pick the value based on if the current element is maximized or not. Return value1 if it is, value 2 otherwise.
    */
    pickValueBasedOnMaximized: function (value1, value2) {
        var stepElement = this.currentTour._options.steps[this.currentTour._current].element;
        return jQuery(stepElement).parents('.aSt').length == 0 ? value2 : value1;
    },

    /**
    * Setup listeners for the tutorials.
    */
    setupListeners: function () {
        var self = this;

        if (this.alreadySetup)
            return;
        this.alreadySetup = true;

        // Listen for first compose action after using Pwm.
        if (!localStorage.priorPwmUserCompose) {
            var composeButton = jQuery('div[role="button"]:contains("COMPOSE")');
            var onComposeClick = function () {
                composeButton.off('click', onComposeClick);

                if (!localStorage.priorPwmUserCompose) {
                    localStorage.setItem('priorPwmUserCompose', true);
                    if (confirm("This is the first time you have composed an email with Pwm. Would you like to see a tutorial on how to encrypt this message?"))
                        self.playComposeTutorial(true);
                }
            };
            composeButton.on('click', onComposeClick);
        }

        // Listen for first read dialog after installing pwm.
        if (!localStorage.priorPwmUserRead) {
            var readInterval = setInterval(function () {
                if (jQuery('.pwmReadOverlay:visible').length == 0)
                    return;

                window.clearInterval(readInterval);

                if (!localStorage.priorPwmUserRead)
                    self.playReadTutorial();
            }, 100);
        }
    }
});