/**
* This class represents a SubjectArea of an email being read.
* @class SubjectArea
*/

var SubjectArea = new Class({
	/**
	* the overlayed element
	* @property element
	*/
	element: null,

	/**
	* @method initialize
	* @param {Element} element an element that contains this SubjectArea, spatially
	*/
	initialize: function( element ) {
		this.element = element;
	},

	/**
	* @method getSubject
	* @return {string} the subject of the email represented by this SubjectArea
	*/
	getSubject: function() {
		return jQuery(this.element).text();
    },

    /**
    * @method setSubject
    * @param {string} subject the subject to display by the email represented by this SubjectItem
    */
    setSubject: function (subject) {
        jQuery(this.element).text(subject);
    },

    /**
    * @method getElement
    * @return {Element} The overlayed element.
    */
    getElement: function () {
        return this.element;
    }

});
