/**
 * Handles the wraping of the body and subject of kiwi packages.
 * 
 * @class EmailPackageWrapper
 * @extends PackageWrapper
 */
var PackageWrapper = (function () {

    //#region Private constants
    /**
    * An optional preamble template for user supplied preamble to the bootstrap message.
    * @property preamble
    * @type String
    */
    var preambleHTML = [
    "<div name='preamble'>",
        "@PREAMBLE",
    "</div>",
    ].join('\n');

    var pwmDescriptionHTML = [
        '<img src="' + Uris.pwmIcon + '" style="float: left;" />',
        '<div style="font-size: 24px; margin-left: 139px;">',
            'This message has been encrypted using Pwm&nbsp;(Private&nbsp;WebMail)',
        '</div>',

        '<div style="min-height:35px; clear:both" />',
        
	    '<div style="font-size: 18px; display: inline-block; text-align: center;">',
            '<div style="border:1px solid #18bc9c; border-radius:5px; padding:20px;">',
                'If you already have Pwm installed, this message will be automatically decrypted.',
            '</div>',
            '<div style="padding-top: 16px; padding-bottom: 16px;">Otherwise</div>',
            '<div><a href="https://pwm.byu.edu/home/?version=@VERSION#download" style="text-decoration:none" target="_blank">',
                '<div style="background-color:#2c3e50; border:1px solid #233140; border-radius:5px; padding:20px; color:white;">',
                    'Click here to get Pwm and decrypt this email.',
                '</div>',
            '</a></div>',
        '</div>',

	    '<div style="min-height: 60px;"></div>',
            
        '<div style="font-size: 14pt; line-height: 0px;">What does this mean?</div><br />',
	    'The sender of this email encrypted it using Pwm. Only recipients this email was encrypted for will be able to read the encrypted contents.<br /><br /><br />',

	    '<div style="font-size: 14pt; line-height: 0px;">What should I do?</div><br />',
	    'Visit <a href="https://pwm.byu.edu/home/?version=@VERSION#download">https://pwm.byu.edu/</a> to download Pwm and decrypt this message.<br /><br /><br />',

	    '<div style="font-size: 14pt; line-height: 0px;">Where can I get more information?</div><br />',
	    'You can get more information at our website <a href="https://pwm.byu.edu/home/?version=@VERSION">https://pwm.byu.edu/</a><br />'
    ].join('\n');

    var packageWrapperHTML = [
        "<div>",
            "<div>",
                "---Begin PWM Encrypted Message---",
            "</div>",
            "<br />",

            "<div name='package' style='font-family:monospace; font-size: 11px;'>@PACKAGE</div>",
            "</div>",
            "<br />",

            "<div style='display: block;'>",
                "---End PWM Encrypted Message---",
            "</div>",
        "</div>"
    ].join('\n');

    /**
    * The header for a Pwm subject.
    * @property subjectHeader
    * @type String
    */
    var subjectHeader = "[Encrypted]";

    /**
    * The regex used to match subjects.
    * @property subjectRegex
    * @type XRegExp
    */
    var subjectRegex = new XRegExp(String.format("^\\s*(<?MessageType>((?<Reply>re:)|(?<Forward>fwd:))?\\s*)?{0}(?<Subject>.*)", XRegExp.escape(subjectHeader)), "i");

    //#endregion

    var packageWrapper = new Class({

        //#region Fields

        /**
        * The subject of the message.
        * @property subject
        * @type String
        */
        subject: null,

        /**
        * The wrapped subject of the message.
        * @property wrappedSubject
        * @type String
        */
        wrappedSubject: null,

        /**
        * The package that is actually being sent in the message.
        * @property package
        * @type Package
        */
        kiwiPackage: null,

        /**
        * The body of the message that is wrapping the subject.
        * @property wrappedBody
        * @type String
        */
        wrappedBody: null,

        //#endregion

        //#region Body Methods

        /**
        * Wraps the package with an optional preamble message.
        * @method wrapPackage
        * @param {string} preambleMessage An optional pre-amble set by the user for this message.
        */
        wrapPackage: function (preambleMessage) {
            this.wrappedBody = '<div name="pwmPackage">';
            if (preambleMessage != null && preambleMessage.trim() != "") {
                preambleMessage = preambleMessage.replace(/(?:\r\n|\r|\n)/g, '<br />');
                this.wrappedBody += preambleHTML.replace('@PREAMBLE', preambleMessage);
                this.wrappedBody += "<br /><hr /><br />";
            }

            this.wrappedBody += '<div name="encryptedEmail">';
            this.wrappedBody += pwmDescriptionHTML;
            this.wrappedBody += "<br /><hr /><br />";
            var encodedPackage = Base64.encodeString(JSON.stringify(this.kiwiPackage));
            this.wrappedBody += packageWrapperHTML.replace('@PACKAGE', encodedPackage);
            this.wrappedBody += '</div></div>';
        },

        /**
        * Unwraps the package from a message.
        * @method unwrapPackage
        */
        unwrapPackage: function () {
            var root = document.createElement('div');
            root.innerHTML = this.wrappedBody;
            this.kiwiPackage = JSON.parse(Base64.decodeString(jQuery('div[name="package"]', root).text()));
            this.preamble = packageWrapper.getPreamble(this.wrappedBody);
        }

        //#endregion

    });


    //#region Simple static package operations 

    /**
    * Checks whether a given subject is a wrapped subject.
    * @method isWrappedSubject
    * @param {String} subject The subject of the message.
    * @return {boolean} Whether the subject is wrapped or not.
    */
    packageWrapper.isWrappedSubject = function (subject) {
        return subjectRegex.test(subject);
    };

    /**
    * Checks whether the given body has a wrapped pacakge.
    * @method hasWrappedPackage
    * @param {String} body The body of the message.
    * @return {boolean} Whether there is a wrapped package or not.
    */
    packageWrapper.hasWrappedPackage = function (body) {
        var root = document.createElement('div');
        root.innerHTML = body;
        return jQuery('div[name="encryptedEmail"]', root).length > 0;
    };

    /**
    * Gets the preamble for the message.
    * @method getPreamble
    * @param {String} body The body of the message.
    * @return {String} The preamble, if there was one.
    */
    packageWrapper.getPreamble = function (body) {
        var root = document.createElement('div');
        root.innerHTML = body;
        var preamble = jQuery('div[name="preamble"]', root);
        if (preamble.length > 0)
            return preamble[0].innerHTML;
        else return null;
    };

    /**
    * Unwraps the given subject.
    * @method unwrapSubject
    * @param {String} wrappedSubject The wrapped subject.
    * @return {String} The unwrapped package.
    */
    packageWrapper.unwrapSubject = function (wrappedSubject) {
        if (!packageWrapper.isWrappedSubject(wrappedSubject))
            return wrappedSubject;
        var results = subjectRegex.exec(wrappedSubject);
        return ((results.MessageType == null ? "" : results.MessageType) + results.Subject).trim();
    };

    /**
    * Unwraps the given body.
    * @method unwrapPackage
    * @param {String} wrappedBody The body with a wrapped subject.
    * @return {String} The unwrapped package.
    */
    packageWrapper.unwrapPackage = function (wrappedBody) {
        var wrapper = new packageWrapper();
        wrapper.wrappedBody = wrappedBody;
        try {
            wrapper.unwrapPackage();
            return wrapper.kiwiPackage;
        }
        catch (e) {
            return null;
        }
    };

    /**
    * Gets a template of a body with a package, where the package and its surrounding
    * text is replaced by the given template name.
    * @method getTemplate
    * @param {String} body The body with a wrapped package.
    * @param {String} template The template to insert in place of the package.
    * @return {String} The body with the kiwi message replaced by the template.
    */
    packageWrapper.getTemplate = function (body, template) {
        var messageBody = document.createElement('div');
        messageBody.innerHTML = body;
        jQuery('div[name="pwmPackage"]', messageBody).replaceWith(template);
        return messageBody.innerHTML;
    };

    //#endregion

    //#region Static initilaize functions

    /**
    * Creates a package wrapper for a wrapper intended to write messages.
    * @method createWriteWrapper
    * @param {String} subject The subject of the message.
    * @param {String} preambleMessage An optional pre-amble to be included with the bootstrap message.
    * @param {String} package The package to be wrapped.
    * @return {PackageWrapper} The created wrapper.
    */
    packageWrapper.createWriteWrapper = function (subject, preambleMessage, kiwiPackage) {
        var wrapper = new packageWrapper();

        // Set the wrapped subject.
        wrapper.subject = subject;
        var results = subjectRegex.exec(subject);
        if (results != null)
            wrapper.wrappedSubject = subject;
        else
            wrapper.wrappedSubject = String.format("{0} {1}", subjectHeader, subject);

        // Set the package and then wrap the body.
        wrapper.kiwiPackage = kiwiPackage;
        wrapper.wrapPackage(preambleMessage);

        return wrapper;
    };

    /**
    * Creates a package wrapper for a wrapper intended to unwrap messages.
    * @method createWriteWrapper
    * @param {String} wrappedSubject The wrapped subject of the message.
    * @param {String} wrappedBody A wrapped body containing a package.
    * @return {PackageWrapper} The created wrapper, or null if the message could not be decoded.
    */
    packageWrapper.createReadWrapper = function (wrappedSubject, wrappedBody) {
        // Check for some basic assumptions.
        if (wrappedSubject == null || wrappedBody == null || !packageWrapper.hasWrappedPackage(wrappedBody))
            return null;

        // Handle the subject.
        var wrapper = new packageWrapper();
        wrapper.wrappedSubject = wrappedSubject;
        wrapper.subject = packageWrapper.unwrapSubject(wrappedSubject);

        wrapper.wrappedBody = wrappedBody;
        try {
            wrapper.unwrapPackage();
        }
        catch (e) {
            return null;
        }

        return wrapper;
    };

    //#endregion

    return packageWrapper;
})();
