/**
* Window manager for compose windows.
* @class ComposeWindowManager
*/
var ComposeWindowManager = new Class({

    Extends: WindowManager,

    //#region Properties

    /**
    * Keeps track of the compose area that this secure window is securing.
    * content for.
    * @property composeArea
    * @type ComposeArea
    */
    composeArea: null,

    //#endregion

    //#region Constructor

    /**
    * Constructor
    * @method initialize
    * @param {ComposeArea} composeArea The compose area that this secure window is securing.
    * @param {Function} removeOverlay Callback that will remove the overlay.  Used in conjunction
    * with a message from the form saying encryption is no longer desired.
    */
    initialize: function (composeArea, removeOverlayCallback) {
        this.parent(composeArea.getElement(), Uris.composeWindowUri);
        this.frame.className = "pwmComposeOverlay";
        this.frame.setAttribute("scrolling", "no");
        this.clippingElement = composeArea.getClippingElement();
        this.composeArea = composeArea;
        var self = this;

        // TODO - REMOVE: Stop-gap measure to fix severe problem with gmail.
        jQuery('.aC2', composeArea.getElement).remove();

        // Fired when a message is being sent.
        this.registerMessageHandler(WindowMessageTypes.SendMessage, function (message) {
            this.updateComposeArea(message.contents);
            removeOverlayCallback(this);
            this.composeArea.clickSendButton();
        });

        // Fired when a draft is being saved.
        this.registerMessageHandler(WindowMessageTypes.SaveDraft, function (message) {
            this.updateComposeArea(message.contents);
            this.composeArea.clickSaveDraftButton();
        });

        // Fired when an overlay is being removed.
        this.registerMessageHandler(WindowMessageTypes.CancelEncryption, function (message) {
            if (PackageWrapper.isWrappedSubject(this.composeArea.getSubject()))
                this.composeArea.setSubject(PackageWrapper.unwrapSubject(this.composeArea.getSubject()));
            this.composeArea.setBody(message.contents);
            removeOverlayCallback(this);
        });

        // Fired when an overlay wants to get who the message is being sent to.
        this.registerMessageHandler(WindowMessageTypes.GetRecipients, function (message) {
            this.postMessage(WindowMessageTypes.GetRecipients, composeArea.getRecipients());
        });

        // Fired when the window has finished being setup and needs the compose area information.
        this.registerMessageHandler(WindowMessageTypes.GetContents, function (message) {
            var info = {
                userId: PageScanner.getEmailAddress(),
                allowCancel: !this.composeArea.isEncryptedReply()
            };

            // If the message being composed has a wrapped message, replace the entire wrapped section with the message body.
            this.composeArea.expandReply();
            var packageWrapper = PackageWrapper.createReadWrapper(this.composeArea.getSubject(), this.composeArea.getBody());
            if (packageWrapper != null) {
                this.composeArea.setPreamble(packageWrapper.preamble);
                info.subject = packageWrapper.subject;
                info.body = PackageWrapper.getTemplate(this.composeArea.getBody(), "[Pwm Package]"); // Replace the package document with the actual package.
                info.templateItem = "[Pwm Package]";
                info.kiwiPackage = packageWrapper.kiwiPackage;
            }
            else {
                info.subject = this.composeArea.getSubject();
                info.body = this.composeArea.getBody();
                info.templateItem = null;
                info.kiwiPackage = null;
            }

            this.postMessage(WindowMessageTypes.GetContents, info);
        });

        // Fired when the window has finished being setup and needs the compose area information.
        this.registerMessageHandler(WindowMessageTypes.DiscardDraft, function () {
            this.composeArea.discardDraft();
        });

        this.focusHandler = function () {
            window.setTimeout(function () {
                self.frame.contentWindow.focus();
                self.postMessage(WindowMessageTypes.FocusChanged, null);
            }, 0);
        };

        // Setup a handler to redirect tabing into the compose area.
        this.composeArea.getBodyElement().on('focus', this.focusHandler);
    },

    /**
    * Get a hight offset by which to increase the height of the overlay.
    * @method getHeightOffset
    * @return {Number} Number of pixels to increase the height of the overlay.
    */
    getHeightOffset: function () {
        return this.composeArea.heightOffset || 0;
    },

    /**
    * Updates the compose area with information from the package.
    * @method updateComposeArea
    * @param {kiwiPackage} Package The package to update the compose area from.
    */
    updateComposeArea: function (kiwiPackage) {
        var packageWrapper = PackageWrapper.createWriteWrapper(this.composeArea.getSubject(), this.composeArea.getPreamble(), kiwiPackage);
        this.composeArea.setSubject(packageWrapper.wrappedSubject);
        this.composeArea.setBody(packageWrapper.wrappedBody);
    },

    /**
    * Cleans up the manager.
    * @method cleanup
    */
    cleanup: function () {
        this.composeArea.getBodyElement().off('focus', this.focusHandler);
    }

    //#endregion

});

