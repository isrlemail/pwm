/**
* Window manager for read windows.
* @class ReadWindowManager
*/
var ReadWindowManager = new Class({

    Extends: WindowManager,

    //#region Properties

    /**
    * Keeps track of the read area that this secure window is securing.
    * content for.
    * @property readArea
    * @property ReadArea
    */
    readArea: null,

    //#endregion

    //#region Constructor

    /**
    * Constructor
    * @method initialize
    * @param {ReadArea} readArea The read area that this secure window is securing.
    */
    initialize: function (readArea) {
        this.parent(readArea.getElement(), Uris.readWindowUri);
        this.frame.className = "pwmReadOverlay";
        this.frame.setAttribute("scrolling", "no");
        this.readArea = readArea;
        var self = this;

        this.registerMessageHandler(WindowMessageTypes.GetContents, function (windowMessage) {
            this.postMessage(WindowMessageTypes.GetContents,
                {
                    kiwiPackage: PackageWrapper.unwrapPackage(this.readArea.getBody()),
                    userId: PageScanner.getEmailAddress()
                });
        });

        // Fired when a sizing request comes from the overlay.
        this.registerMessageHandler(WindowMessageTypes.SizingInfo, function (message) {
            jQuery(this.overlayedElement).height(message.contents.height + 4).css({ overflow: "hidden" });
        });

        // If the window is resized, we may want to resize the overlay element as well.
        this.windowResizeFunction = function () {
            self.postMessage(WindowMessageTypes.SizingInfo, null);
        };
        jQuery(window).on('resize', this.windowResizeFunction);
    },

    /**
     * Cleans up the manager.
     * @method cleanup
     */
    cleanup: function () {
        jQuery(window).off('resize', this.windowResizeFunction);
    }

    //#endregion

});
