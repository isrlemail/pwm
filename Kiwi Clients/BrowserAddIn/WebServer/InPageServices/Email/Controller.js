/**
* The email controller loads the pagescanner, ', and overlay
* instances. It also registers event listeners so that it will be notified when
* the page changes. Whenever the page changes, the controller removes old overlays
* and instantiates new ones for the current page.
* @class Controller
* @constructor
*/
var Controller = new Class({

    /**
    * The page scanner used by this controller to find relevant page content.
    * @property pageScanner
    * @type PageScanner
    */
    pageScanner: null,

    /**
    * A handle to the key manager.
    * @property keyManagerWindowManager
    * @type KeyManagerWindowManager
    */
    keyManagerWindowManager: null,

    /**
    * Email Controller initializer
    * @method initialize
    */
    initialize: function () {
        var self = this;

        // Load the page scanner.
        this.pageScanner = new PageScanner();

        // Add the key manager window.
        this.keyManagerWindowManager = new KeyManagerWindowManager(document.body, ControllerTypes.email);
        document.body.appendChild(this.keyManagerWindowManager.frame);

        // This handles cross-domain frame messages.
        window.addEventListener('message', function (event) {
            // Check the origin.
            if (event.origin != Uris.domainUri) {
                return;
            }

            // Get the object out of the message.  If its guid is defined, then we will
            // handle this message with a registered handler if available.
            // In later non-prototype versions this guid will need to be more thoroughly
            // checked.
            var windowMessage = JSON.parse(event.data);

            if (windowMessage.guid !== undefined && windowMessage.messageType !== undefined && windowMessage.contents !== undefined) {
                self.keyManagerWindowManager.handleMessage(windowMessage);
                Overlayer.windowManagers.each(function (item) {
                    item.handleMessage(windowMessage);
                });
            }
        }, false);

        // Add a Pwm icon to the browser bar.
        var pwmIconDiv = jQuery.parseHTML([
            '<div id="pwmIconDiv" class="gb_Lc gb_n gb_0c gb_Sc">',
                '<img id="pwmIcon" src="' + Uris.pwmIcon + '" title="Pwm is currently running. Click this icon to replay a tutorial." className="pwmIcon" />',
                '<ul id="iconDropdownMenu" style="display: none;">',
                    '<li id="baseTutorialLink">Introduction</li>',
                    '<li id="readTutorialLink">Read encrypted email</li>',
                    '<li id="composeTutorialLink">Send encrypted email</li>',
                    '<li id="findTutorialLink">Find encrypted email</li>',
                '</ul>',
            '</div>'
        ].join('\n'));
        var icon = jQuery('#pwmIcon', pwmIconDiv);
        var iconDropdownMenu = jQuery('#iconDropdownMenu', pwmIconDiv);
        this.pageScanner.getIconLocation().prepend(pwmIconDiv);
        
        jQuery(document).on('click', function (e) {
            if (icon.is(e.target) && iconDropdownMenu.is(':not(:visible)'))
                iconDropdownMenu.show();
            else
                iconDropdownMenu.hide();
        });

        var tutorial = self.pageScanner.tutorial;
        jQuery('#baseTutorialLink').bind('click', function () { tutorial.playBaseTutorial(); });
        jQuery('#findTutorialLink').bind('click', function () { tutorial.playInboxTutorial(); });
        jQuery('#composeTutorialLink').bind('click', function () { tutorial.playComposeTutorial(); });
        jQuery('#readTutorialLink').bind('click', function () { tutorial.playReadTutorial(); });

        // Setup the refresh code.
        /*
        var refreshOverlaysTimer = function () {
            self.refreshOverlays();
            window.setTimeout(function () { refreshOverlaysTimer(); }, 100);
        };
        refreshOverlaysTimer();
        */
        this.refreshOverlays();
        jQuery(document.body).observe('childlist subtree', function () {
            self.refreshOverlays();
        });

        
        // Show that we have been loaded.
        //this.showDialog('Pwm successfully loaded');

        // Run after everything is finished.
        window.setTimeout(function () {
            //self.bookmarkletClicked(false);
            self.pageScanner.tutorial.initialRun();
        }, 10);
    },

    /**
    * Shows a dialog giving status information.
    * @method showDialog
    * @ param text {String} Text to show.
    */
    showDialog: function (text) {
        /*
        jQuery('.pwmStatusDialog').hide();

        var dialog = document.createElement('div');
        dialog.className = 'pwmStatusDialog';
        dialog.style.cssText = 'display: none;';
        dialog.innerHTML = '<h1>' + text + '</h1>';
        document.body.appendChild(dialog);

        jQuery(dialog).show().delay(1000).fadeOut('slow');
        */
    }.protect(),

    /**
    * This method should be invoked when the page changes.  It will make calls to unload old
    * overlays and add any new overlays to the current page.
    * @method refreshOverlays
    */
    refreshOverlays: function () {
        if (this.refreshScheduled)
            return;
        this.refreshScheduled = true;

        var self = this;
        window.setTimeout(function () {
            self.overlaySubjects();

            var windowsToOverlay = [];

            self.overlayReadAreas(windowsToOverlay);
            self.overlayComposeAreas(windowsToOverlay);
            Overlayer.setOverlays(windowsToOverlay);
            self.refreshScheduled = false;
        }, 10);
    },

    /**
    * Adds an icon and unwraps subjects of Kiwi messages.
    * @method overlaySubjects
    * @return [function] a function which will add the kiwi lock with the given url for matching subjects
    */
    overlaySubjects: function () {
        var self = this;

        var secureIconFunction = function () {
            return function (item) {
                var element = jQuery(item.getElement());
                if (PackageWrapper.isWrappedSubject(item.getSubject()) && element.data('pwm-lock-inserted') !== true) {
                    element.data('pwm-lock-inserted', true);
                    var lockButton = new Element('div', { 'class': 'pwmLockDiv' });
                    lockButton.innerText = 'Encrypted';
                    jQuery(item.getElement()).before(lockButton);
                    item.setSubject(PackageWrapper.unwrapSubject(item.getSubject()));
                }
            };
        };

        this.pageScanner.getSubjectItems().each(secureIconFunction());
        this.pageScanner.getSubjectAreas().each(secureIconFunction());
    },

    /**
    * Puts an overlay over all secure read messages.
    * @method overlayReadAreas
    */
    overlayReadAreas: function (windowsToOverlay) {        
        var self = this;

        this.pageScanner.getReadAreas().each(function (item) {
            if (PackageWrapper.hasWrappedPackage(item.getBody())) {
                if (item.isClipped()) {
                    item.prepareClippedArea();
                    return;
                }

                var currentWindow = Overlayer.getWindowManager(item.getElement());
                if (currentWindow) {
                    windowsToOverlay.push(currentWindow);
                }
                else {
                    var newReadWindowManager = new ReadWindowManager(item);
                    windowsToOverlay.push(newReadWindowManager);
                }
            }
        });
    },

    /**
    * Puts a secure button on compose areas, and hooks up the handlers for when that button is pressed.
    * @method overlayComposeAreas
    */
    overlayComposeAreas: function (windowsToOverlay) {
        var self = this;

        this.pageScanner.getComposeAreas().each(function (item) {
            var element = jQuery(item.getElement());

            // If we have already handled this element.
            if (item.isSetup()) {
                var currentWindow = Overlayer.getWindowManager(item.getElement());
                if (currentWindow) {
                    windowsToOverlay.push(currentWindow);
                }

            }
            else {
                // Creates a handler for the lock icon being clicked.
                var addComposeOverlayCallback = function () {
                    // If we already have an overlay, we don't need to do anything.
                    if (Overlayer.getWindowManager(item.getElement()))
                        return;

                    // Register a method to remove the overlay.
                    var newWindowManager = new ComposeWindowManager(item, function (composeWindowManager) {
                        item.revertArea();
                        Overlayer.removeIndividualOverlay(composeWindowManager);
                    });

                    Overlayer.addIndividualOverlay(newWindowManager);
                    item.prepareArea();
                };

                item.setup(addComposeOverlayCallback);

                // If their is a package automatically overlay it.
                if (PackageWrapper.hasWrappedPackage(item.getBody()) || item.isEncryptedReply())
                    window.setTimeout(addComposeOverlayCallback, 0);
            }
        });
    },

    /**
    * Puts a secure button on compose areas, and hooks up the handlers for when that button is pressed.
    * @method bookmarkletClicked
    */
    bookmarkletClicked: function (showDialog) {
        /*
        if (showDialog)
            this.showDialog('Encrypting current messages...');
        this.pageScanner.getComposeAreas().each(function (item) {
            var lockIcon = item.enable();
        });
        */
    }

});