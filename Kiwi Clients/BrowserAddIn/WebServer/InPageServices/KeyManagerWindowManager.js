﻿/**
* Window manager for the KeyManagerWindow
* @class KeyManagerWindowManager
*/
var KeyManagerWindowManager = new Class({

    Extends: WindowManager,

    //#region Fields

    /**
    * List of transaction Ids of interest.
    * @property transactionIds
    * @type Array.<String>
    */
    transactionIds: null,

    /**
    * Handle to the other authentication window manager
    * @property otherAuthWindowManager
    * @type OtherAuthenticationWindowManager
    */
    otherAuthWindowManager: null,

    //#endregion

    //#region Constructor

    /**
    * Constructor
    * @method initialize
    * @param {Element} pageArea The read area that this secure window is securing.
    */
    initialize: function (pageArea, type) {
        this.parent(pageArea, Uris.keyManagerWindowUri, type);
        this.frame.className = "pwmHiddenOverlay";
        this.transactionIds = new Array();
        var self = this;

        // Fired when a password is requested.
        this.registerMessageHandler(WindowMessageTypes.GetSawTokens, function (windowMessage) {
            self.getSawTokens(windowMessage.contents);

            // For the next thirty seconds check email every second.
            var timerId = window.setInterval(function () { self.getSawTokens(); }, 1 * 1000);
            window.setTimeout(function () { window.clearInterval(timerId); }, 15 * 1000);
        });

        // Fired when an other auth token is requested
        this.registerMessageHandler(WindowMessageTypes.BeginGetOtherTokens, function (windowMessage) {
            if (self.otherAuthWindowManager == null)
                self.otherAuthWindowManager = new OtherAuthenticationWindowManager(windowMessage.contents);
            self.otherAuthWindowManager.show();
        });

        // Fired when an other auth token is received and processed
        this.registerMessageHandler(WindowMessageTypes.EndGetOtherTokens, function (windowMessage) {
            if (self.otherAuthWindowManager != null)
                self.otherAuthWindowManager.hide();
        });

        // Check e-mail every fifteen seconds for new tokens.
        window.setInterval(function () { self.getSawTokens(); }, 15 * 1000);
    },

    //#endregion

    //#region SAW

    /**
    * Gets saw tokens for this window.
    * @method checkForSawTokens
    * @param transactionId {String} A transaction Id that should be checked for in addition
    * to those already being waited for.
    */
    getSawTokens: function (transactionId) {
        var self = this;

        // Add this to the list of tokens being tracked.
        if (transactionId)
            this.transactionIds.push(transactionId);

        PageScanner.getNewEmailSubjects(this.transactionIds,
            function (subjects) {
                var tokens = new Array();

                // Tests the subjects for auth tokens.
                for (var i = 0; i < subjects.length; i++) {
                    var token = Saw.getSawPartsFromSubject(subjects[i]);
                    if (token != null)
                        tokens.push(token);
                }

                // Send a message with these tokens if any were found.
                if (tokens.length > 0) {
                    self.postMessage(WindowMessageTypes.GetSawTokens, tokens);
                }
            });
    }

    //#endregion

});
