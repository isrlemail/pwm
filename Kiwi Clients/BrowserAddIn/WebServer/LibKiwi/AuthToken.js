﻿/**
* @class AuthToken
* @constructor
*/
var AuthToken = new Class({

    /**
    * Size of the password key
    * @property passwordKeySize
    * @type int
    */
    passwordKeySize: 32,

    /**
    * User ID this token is for.
    * @property userId
    * @type String
    */
    userId: null,

    /**
    * The period for which the token is valid.
    * @property period
    * @type PeriodBase
    */
    period: null,

    /**
    * The actual bytes of the token
    * @property value
    * @type byte[]
    */
    value: null,

    /**
    * The method used for this token
    * @property method
    * @type AuthenticationMethod
    */
    method: AuthenticationMethod.None,

    /**
    * ID for this transaction
    * @property transactionId
    * @type String
    */
    transactionId: null,

    /**
    * Whether or not the token has been completed
    * @property isTokenComplete
    * @type bool
    */
    isTokenComplete: false,

    /**
    * URI of the auth server this token came from
    * @property authServeerUri
    * @type URI
    */
    authServerUri: null,

    /**
    * Constructor
    * @method initialize
    * @param {String} userId User id string
    * @param {PeriodBase} tokenPeriod Period the token is valid
    * @param {Byte[]} token Token value
    * @param {Uri} authUri URI of the auth server
    * @param {AuthenticationMethod} method Authentication method for the auth token
    * @param {String} transactionid Unique transaction ID
    * @param {Bool} isTokenComplete Whether this token is complete 
    */
    initialize: function (userId, tokenPeriod, value, authUri, method, transactionid, isTokenComplete) {
        this.userId = userId;
        this.period = tokenPeriod;
        this.value = value;
        this.authServerUri = authUri;
        this.transactionId = transactionid;
        this.isTokenComplete = isTokenComplete;
    },

    /**
    * Derives the key for a password using the HKDF
    * @method getPasswordKey
    * @param {String} password password to derive the key for.
    */
    getPasswordKey: function (password) {
        return Hkdf.derivePasswordKey(this.userId, password, this.passwordKeySize);
    },

    /**
    * Encrypts the token with a key derived from a password.
    * @method encryptWithPassword
    * @param {Byte[]} key Key to use.
    */
    encryptWithPasswordKey: function (key) {
        if (!key || key.length == 0) {
            throw "Key null or undefined: " + key;
        }

        var result = new AesSymmetricAlgorithm(SymmetricAlgorithmMode.CBC).encrypt(key, [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], this.value);
        this.isTokenComplete = false;
        return result;
    },

    /**
    * Decrypts the token with a key derived from a password.
    * @method decryptWithPassword
    * @param {Byte[]} key Key to use.
    */
    decryptWithPasswordKey: function (key) {
        if (!key || key.length == 0) {
            throw "Key null or undefined: " + key;
        }

        var result = new AesSymmetricAlgorithm(SymmetricAlgorithmMode.CBC).decrypt(key, [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], this.value);
        this.isTokenComplete = true;
        return result;
    },


    /**
    * Decrypts the token with a key derived from a password.
    * @method combine
    * @param {Byte[]} token Token to combine with this one
    * @param {Bool} isTokenComplete Whether the newly combined token is complete
    */
    combine: function (token, isTokenComplete) {
        // Check input values.
        if (!token || !this.value || token.length != this.value.length) {
            throw "Invalid argument passed to AuthTokens.combine";
        }

        // We can't do a single bitewise operator for the whole byte[] in JavaScript
        // like we can in other, nicer languages, so we iterate over every byte and
        // do the xor for each one.
        for (var i = 0; i < token.length; i++) {
            this.value[i] = this.value[i] ^ token[i];
        }
        this.isTokenComplete = isTokenComplete;
    },

    /**
    * Method to produce an object for use with JSON.stringify().
    * @method toJsonObject
    * @return {Object} The object to be serialized.
    */
    toJsonObject: function () {
        var jsonObject = {};
        jsonObject.UserId = this.userId;
        jsonObject.Period = this.period.toJsonObject();
        jsonObject.Value = Base64.encodeByteArray(this.value);
        if (this.method)
            jsonObject.Method = this.method;
        if (this.transactionId)
            jsonObject.TransactionId = this.transactionId;
        if (this.isTokenComplete)
            jsonObject.IsTokenComplete = this.isTokenComplete;
        jsonObject.AuthServerUri = this.authServerUri;
        return jsonObject;
    }
});

/**
* Method to retrieve this object from a JSON object.
* @method fromJsonObject
* @param jsonObject {Object} Object to parse.
* @return {AuthToken} AuthToken parsed from the passed object.
*/
AuthToken.fromJsonObject = function (jsonObject) {
    var parsedObject = new AuthToken();
    parsedObject.userId = jsonObject.UserId;
    parsedObject.period = PeriodBase.fromJsonObject(jsonObject.Period);
    parsedObject.value = Base64.decodeStringToByteArray(jsonObject.Value);
    if (jsonObject.Method)
        parsedObject.method = jsonObject.Method;
    if (jsonObject.TransactionId)
        parsedObject.transactionId = jsonObject.TransactionId;
    if (jsonObject.IsTokenComplete)
        parsedObject.isTokenComplete = true;
    parsedObject.authServerUri = jsonObject.AuthServerUri;
    return parsedObject;
};