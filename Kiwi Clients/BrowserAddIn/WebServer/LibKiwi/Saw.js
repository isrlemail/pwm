﻿/**
* Class for parsing saw packets.
* @class Saw
*/
var Saw = (function () {
    var saw = new Class({

        /**
        * The regular expressions for parsing saw tokens.
        * @property sawRegex
        * @type XRegExp
        */
        sawRegex: new XRegExp("\\[AuthToken\\](?<TransactionId>[^&]+)&(?<Token>[^\\s]+)"),

        /**
        * The regular expressions for parsing saw tokens.
        * @method getSawPartsFromSubject
        * @param subject {String} The subject to parse.
        * @return {Object} Object with the transactionId and value of the token.
        */
        getSawPartsFromSubject: function (subject) {
            var results = this.sawRegex.exec(subject);
            if (results)
                return { transactionId: results.TransactionId, value: Base64.decodeStringToByteArray(results.Token) };
            else return null;
        }

    });

    return new saw();
})();