﻿/**
* AttachmentInfo data class.
* @class AttachmentInfo
*/
var AttachmentInfo = new Class({

    /**
    * The original name of the attachment.
    * @property originalName
    * @type Array.<byte>
    */
    originalName: null,

    /**
    * The digest of the attachment.
    * @property digest
    * @type Array.<byte>
    */
    digest: null,

    /**
    * Method to produce an object for use with JSON.stringify().
    * @method toJsonObject
    * @return {Object} The object to be serialized.
    */
    toJsonObject: function () {
        var jsonObject = {};
        if (this.digest != null)
            jsonObject.Digest = Base64.encodeByteArray(this.digest);
        if (this.originalName != null)
            jsonObject.Name = this.originalName;
        return jsonObject;
    }

});

/**
* Method to retrieve this object from a JSON object.
* @method fromJsonObject
* @param jsonObject {Ojbect} Object to parse.
* @return {AttachmentInfo} AttachmentInfo parsed from the passed object.
*/
AttachmentInfo.fromJsonObject = function (jsonObject) {
    var parsedObject = new AttachmentInfo();
    if (jsonObject.Digest)
        parsedObject.digest = Base64.decodeStringToByteArray(jsonObject.Digest);
    if (jsonObject.Name)
        parsedObject.originalName = jsonObject.Name;
    return parsedObject;
};