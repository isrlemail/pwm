﻿/**
* @class PeriodBase
* @constructor
*/
var PeriodBase = new Class({
	/**
	* Start of the valid period
	* @property start
	* @type Date
	*/
	start: null,

	/**
	* End of the valid period
	* @property start
	* @type Date
	*/
	end: null,

	/**
	* Gets whether the period is valid at the current moment.
	* @method isCurrent
	*/
	isCurrent: function ()
	{
		// Note that new Date() defaults to today
		return this.validForDate(new Date());
	},

	/**
	* Gets whether the period is valid for the given time.
	* @method validForDate
	* @param date {Date} The date to check
	*/
	validForDate: function (date)
	{
		return date >= this.start && date <= this.end;
	}

});



/**
* Method to retrieve this object from a JSON object.
* @method fromJsonObject
* @param jsonObject {Ojbect} Object to parse.
* @return {PeriodBase} PeriodBase parsed from the passed object.
*/
PeriodBase.fromJsonObject = function (jsonObject)
{
	var type = jsonObject.__type;

	if (type == "MonthlyPeriod:#Kiwi.LibKiwi")
		return MonthlyPeriod.fromJsonObject(jsonObject);
	else if (type == "ExpirationPeriod:#Kiwi.LibKiwi")
		return ExpirationPeriod.fromJsonObject(jsonObject);

	return null;
};
