﻿/**
* KeyRequest data class.
* @class Message
*/
var KeyRequest = new Class({

    /**
    * Gets the auth token that authorizes this request to the key server.
    * @property authToken
    * @type AuthToken
    */
    authToken: null,

    /**
    * Gets the Id of the creator.
    * @property creatorId
    * @type String
    */
    creatorId: null,

    /**
    * Gets the Id of the viewer.  Not used for creator key requests.
    * @property viewerId
    * @type String
    */
    viewerId: null,

    /**
    * The date time for which the request is being made.
    * @property creatorId
    * @type Date
    */
    timestamp: null,

    /**
    * Gets the size of the key desired.
    * @property keySize
    * @type int
    */
    keySize: null,

    /**
    * Gets the type of the request, whether for a creator or viewer key.
    * @property keyType
    * @type KeyRequestType
    */
    keyType: null,

    /**
    * Constructor
    * @method initialize
    * @param authToken {AuthToken} Auth token that authorizes this request to the key server.
    * @param creatorId {String} Id of the creator.
    * @param viewerId {String} Id of the viewer.
    * @param timestamp {Date} Date time for which the request is being made.
    * @param keySize {Number} Size of the key desired.
    * @param keyType {KeyRequestType} Type of the request, whether for a creator or viewer key.
    */
    initialize: function (authToken, creatorId, viewerId, timestamp, keySize, keyType) {
        this.authToken = authToken;
        this.creatorId = creatorId;
        this.viewerId = viewerId;
        this.timestamp = timestamp;
        this.keySize = keySize;
        this.keyType = keyType;
    },

    /**
    * Method to produce an object for use with JSON.stringify().
    * @method toJsonObject
    * @return {Object} The object to be serialized.
    */
    toJsonObject: function () {
        var jsonObject = {};
        jsonObject.AuthToken = this.authToken.toJsonObject();
        jsonObject.CreatorId = this.creatorId;
        if (this.viewerId != null)
            jsonObject.ViewerId = this.viewerId;
        jsonObject.Timestamp = this.timestamp.toMSJson();
        jsonObject.KeySize = this.keySize;
        jsonObject.KeyType = this.keyType;
        return jsonObject;
    }

});

/**
* Method to retrieve this object from a JSON object.
* @method fromJsonObject
* @param jsonObject {Ojbect} Object to parse.
* @return {KeyRequest} KeyRequest parsed from the passed object.
*/
KeyRequest.fromJsonObject = function (jsonObject) {
    var parsedObject = new KeyRequest();
    parsedObject.authToken = AuthToken.fromJsonObject(jsonObject.AuthToken);
    parsedObject.creatorId = jsonObject.CreatorId;
    if (jsonObject.ViewerId)
        parsedObject.viewerId = jsonObject.ViewerId;
    parsedObject.timestamp = Date.parseJson(jsonObject.Timestamp);
    parsedObject.keySize = jsonObject.KeySize;
    parsedObject.keyType = jsonObject.KeyType;
    return parsedObject;
};
