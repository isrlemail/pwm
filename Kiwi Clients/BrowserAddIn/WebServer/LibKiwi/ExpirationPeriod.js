﻿/**
* Period valid until a given expiration date
* @class ExpirationPeriod
*/
var ExpirationPeriod = new Class({

	Extends: PeriodBase,

	/**
	* Constructor
	* @method initialize
	* @param {Date} expirationDate Date when this period expires
	*/
	initialize: function (expirationDate)
	{
		this.end = expirationDate;
	},


	/**
	* Check whether the specified date is valid.
	* @method validForDate
	* @param {Date} date The date to check if is valid.
	*/
	validForDate: function (date)
	{
		return date < this.end;
	},

	/**
	* Method to produce an object for use with JSON.stringify().
	* @method toJsonObject
	* @return {Object} The object to be serialized.
	*/
	toJsonObject: function ()
	{
		var jsonObject = {};
		jsonObject.__type = "ExpirationPeriod:#Kiwi.LibKiwi";
		jsonObject.End = this.end.toMSJson();
		return jsonObject;
	}

});

/**
* Method to retrieve this object from a JSON object.
* @method fromJsonObject
* @param jsonObject {Ojbect} Object to parse.
* @return {ExpirationPeriod} ExpirationPeriod parsed from the passed object.
*/
ExpirationPeriod.fromJsonObject = function(jsonObject)
{
	var date = Date.parseJson(jsonObject.End);
	return new ExpirationPeriod(date);
};