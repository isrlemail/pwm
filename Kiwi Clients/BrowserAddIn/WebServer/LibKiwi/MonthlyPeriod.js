﻿/**
* @class MonthlyPeriod
* @constructor
* @extends PeriodBase
*/
var MonthlyPeriod = new Class({
    Extends: PeriodBase,

    /**
    * Constructor
    * @method initialize
    * @param {int} month Month from 0-11
    * @param (int} year Four digit year value.  (If more digits are necessary, consider a more modern platform.  Hey, we all know how hard prototype code is to get rid of.)
    */
    initialize: function (year, month) {
        var startDate = new Date();
        startDate.setFullYear(year, month, 1);
        this.start = startDate;
    },

    /**
    * Check whether the specified date is valid.
    * @method validForDate
    * @param {Date} date The date to check if is valid.
    */
    validForDate: function (date) {
        return date.getFullYear() == this.start.getFullYear() &&
                date.getMonth() == this.start.getMonth();
    },

    /**
    * Method to produce an object for use with JSON.stringify().
    * @method toJsonObject
    * @return {Object} The object to be serialized.
    */
    toJsonObject: function () {
        var jsonObject = {};
        jsonObject.__type = "MonthlyPeriod:#Kiwi.LibKiwi";
        jsonObject.Start = this.start.toMSJson();
        return jsonObject;
    }

});

MonthlyPeriod.getPeriodForDate = function (date) {
    return new MonthlyPeriod(date.getFullYear(), date.getMonth());
};

MonthlyPeriod.Now = function () {
    var date = new Date();
    return new MonthlyPeriod(date.getFullYear(), date.getMonth());
};

/**
* Method to retrieve this object from a JSON object.
* @method fromJsonObject
* @param jsonObject {Ojbect} Object to parse.
* @return {MonthlyPeriod} MonthlyPeriod parsed from the passed object.
*/
MonthlyPeriod.fromJsonObject = function (jsonObject) {
    var date = Date.parseJson(jsonObject.Start);
    return new MonthlyPeriod(date.getFullYear(), date.getMonth());
};


