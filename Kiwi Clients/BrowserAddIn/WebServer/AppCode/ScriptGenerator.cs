﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using Yahoo.Yui.Compressor;
using System.Threading;

namespace Kiwi.WebServer
{

	#region Data contracts for serialized data

	/// <summary>
	/// The type of the controller the script is meant for.
	/// </summary>
	[DataContract]
	public enum ControllerType
	{
		[EnumMember]
		Any,

		[EnumMember]
		Email,

		[EnumMember]
		Chat
	}

	/// <summary>
	/// Root XML element.  Scripts are loaded in the order they are in this collection.
	/// </summary>
	[XmlRoot("ScriptGroup")]
	public sealed class ScriptGroups
	{

		/// <summary>
		/// Sets default values.
		/// </summary>
		public ScriptGroups()
		{
			BasePath = "~/";
			Groups = new List<Group>();
		}

		/// <summary>
		/// The base path for all files listed.
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		public string BasePath { get; set; }

		/// <summary>
		/// Gets the groups inside of the script group.
		/// </summary>
		[DataMember(IsRequired = true, EmitDefaultValue = false)]
		public List<Group> Groups { get; set; }

	}

	/// <summary>
	/// Individual groups of files that can be put in together.
	/// </summary>
	[DataContract(Name = "Group")]
	public sealed class Group
	{

		/// <summary>
		/// Sets default values.
		/// </summary>
		public Group()
		{
			ControllerType = ControllerType.Any;
			Location = ".*";
			Files = new List<FileItem>();
			Directories = new List<DirectoryItem>();
		}

		/// <summary>
		/// A name used for convience only.
		/// </summary>
		[DataMember(EmitDefaultValue = false), XmlAttribute]
		public string Name { get; set; }

		/// <summary>
		/// The type of controller for which this group should be included.
		/// </summary>
		[DataMember(IsRequired = true, EmitDefaultValue = true), XmlAttribute]
		public ControllerType ControllerType { get; set; }

		/// <summary>
		/// Regex that if set means the group should only be loaded when the document.location
		/// matches this string.
		/// </summary>
		[DataMember(EmitDefaultValue = false), XmlAttribute]
		public string Location { get; set; }

		/// <summary>
		/// List of files in this group.
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		public List<FileItem> Files { get; set; }

		/// <summary>
		/// List of directories in this group.
		/// </summary>
		[DataMember(EmitDefaultValue = false)]
		public List<DirectoryItem> Directories { get; set; }
	}

	/// <summary>
	/// A file in a group.
	/// </summary>
	[DataContract(Name = "File")]
	public sealed class FileItem
	{
		/// <summary>
		/// Path to the file on the server.
		/// </summary>
		[DataMember(IsRequired = true, EmitDefaultValue = false), XmlAttribute]
		public string Path { get; set; }

	}

	/// <summary>
	/// A directory in a group.
	/// </summary>
	[DataContract(Name = "Directory")]
	public sealed class DirectoryItem
	{
		/// <summary>
		/// Path to the directory on the server.
		/// </summary>
		[DataMember(IsRequired = true, EmitDefaultValue = false), XmlAttribute]
		public string Path { get; set; }
	}

	#endregion

	/// <summary>
	/// Summary description for ScriptGenerator
	/// </summary>
	public static class ScriptGenerator
	{

		#region Script Retreival Methods

		/// <summary>
		/// Gets the script for use in page services.
		/// </summary>
		/// <param name="controllerType">The controller type to get the script for.</param>
		/// <param name="windowLocation">The location that the script is being loaded into.</param>
		/// <param name="debug">Whether the script is optimized for debugging.</param>
		/// <returns>A script for the requested service.</returns>
		public static string GetInPageServicesScript(ControllerType controllerType, string windowLocation, bool debug)
		{
			var scriptBuilder = CombineScripts("~/App_Data/InPageServicesScriptGenerator.xml", controllerType, windowLocation, debug);

			scriptBuilder.Replace("@DEBUG", debug.ToString().ToLower());

			return BundleScript(scriptBuilder, debug);
		}

		/// <summary>
		/// Gets the script for use in key manager window services.
		/// </summary>
		/// <param name="windowLocation">The location that the script is being loaded into.</param>
		/// <param name="guid">The guid of the window.</param>
		/// <param name="debug">Whether the script is optimized for debugging.</param>
		/// <returns>A script for the requested service.</returns>
		public static string GetWindowServicesScript(string windowLocation, string guid, bool debug, ControllerType controllerType)
		{
			var scriptBuilder = CombineScripts("~/App_Data/WindowServicesScriptGenerator.xml", controllerType, windowLocation, debug);

			scriptBuilder.Replace("@DEBUG", debug.ToString().ToLower());
			scriptBuilder.Replace("@GUID", guid);

			return BundleScript(scriptBuilder, debug);
		}

		#endregion

		#region Common Methods

		/// <summary>
		/// Gets the script for the given XML document.
		/// </summary>
		/// <param name="path">Path to the XML file defining what to combine.</param>
		/// <param name="controllerType">The controller type to get the script for.</param>
		/// <param name="windowLocation">The location that the script is being loaded into.</param>
		/// <param name="debug">Whether the script is optimized for debugging.</param>
		/// <returns>Combined scripts as defined in the script XML.</returns>
		private static StringBuilder CombineScripts(string path, ControllerType controllerType, string windowLocation, bool debug)
		{
			ScriptGroups scriptGroups = GetGroups(path);
			StringBuilder scriptBuilder = new StringBuilder();

			// Add each group sequentially.
			foreach (Group group in scriptGroups.Groups)
			{
				if (group.ValidFor(controllerType, windowLocation))
				{
					// Get all the files and add them to the script.
					foreach (FileItem file in group.Files)
					{
						scriptBuilder.AppendLine(File.ReadAllText(scriptGroups.GetActualPath(file.Path)));
						scriptBuilder.AppendLine();
					}

					// Read each javascript from the given file directory.
					foreach (DirectoryItem directory in group.Directories)
					{
						DirectoryInfo directoryInfo = new DirectoryInfo(scriptGroups.GetActualPath(directory.Path));
						foreach (FileInfo file in directoryInfo.EnumerateFiles("*.js", SearchOption.TopDirectoryOnly))
						{
							scriptBuilder.AppendLine(File.ReadAllText(file.FullName));
							scriptBuilder.AppendLine();
						}
					}

				}
			}
            
			scriptBuilder.Replace("@SERVER_URI", Utils.ServerUri);
			scriptBuilder.Replace("@VERSION", Utils.Version);
			scriptBuilder.Replace("@DOMAIN_URI", Utils.DomainUri);
			scriptBuilder.Replace("@TRUSTED_KEY_SERVERS", string.Format("'{0}'", String.Join("', '", Utils.TrustedKeyServers)));
			return scriptBuilder;
		}

		/// <summary>
		/// Bundles the script using a javascript compressor.
		/// </summary>
		/// <param name="scriptBuilder">The builder for the current script.</param>
		/// <param name="debug">Whether the script is in debug mode or not.</param>
		/// <returns>The bundled script.</returns>
		private static string BundleScript(StringBuilder scriptBuilder, bool debug)
		{
			if (debug)
				return scriptBuilder.ToString();
			else
                return string.Format("(function(){{{0}}})();", new JavaScriptCompressor().Compress(scriptBuilder.ToString()));
		}

		/// <summary>
		/// Gets the groups from an XML file.
		/// </summary>
		/// <param name="path">Path to the XML file.</param>
		/// <returns>Groups object deserialized from the given file.</returns>
		private static ScriptGroups GetGroups(string path)
		{
			using (FileStream stream = File.Open(Utils.Server.MapPath(path), FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				XmlSerializer serializer = new XmlSerializer(typeof(ScriptGroups));
				return (ScriptGroups)serializer.Deserialize(stream);
			}
		}

		#endregion

		#region Extension Methods

		/// <summary>
		/// Checks a group to see if matches the controller and window location paramaters.
		/// </summary>
		/// <param name="controllerType">The controller type to get the script for.</param>
		/// <param name="windowLocation">The location that the script is being loaded into.</param>
		/// <returns>Whether the group matches</returns>
		private static bool ValidFor(this Group group, ControllerType controllerType, string windowLocation)
		{
			return Regex.IsMatch(windowLocation, group.Location, RegexOptions.IgnoreCase)
					&& (group.ControllerType == ControllerType.Any || controllerType == group.ControllerType);
		}

		/// <summary>
		/// Gets the actual path for an item listed under a group.
		/// </summary>
		/// <param name="scriptGroup">The groups base for the files.</param>
		/// <param name="relativePath">The relative file path to the scripts.</param>
		/// <returns>A string with the actual file path on the server.</returns>
		private static string GetActualPath(this ScriptGroups scriptGroup, string relativePath)
		{
			return Utils.Server.MapPath(Path.Combine(scriptGroup.BasePath, relativePath));
		}

		#endregion

	}
}