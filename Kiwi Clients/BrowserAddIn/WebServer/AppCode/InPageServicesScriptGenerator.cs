﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kiwi.WebServer
{

    /// <summary>
    /// Summary description for InPageServiceScript
    /// </summary>
    public class InPageServicesScriptGenerator : IHttpHandler
    {

        #region IHttpHandler

        /// <summary>
        /// Gets a value indicating whether another request can use the IHttpHandler instance.
        /// </summary>
        public bool IsReusable { get { return false; } }

        /// <summary>
        /// Processes the request to this handler.
        /// </summary>
        /// <param name="context">Context for the request.</param>
        public void ProcessRequest(HttpContext context)
        {
            // Parse response variables.
            ControllerType controllerType;
            if(!string.IsNullOrEmpty(context.Request["controllerType"])) {
                bool controllerTypeFound = Enum.TryParse(context.Request["controllerType"], true, out controllerType);
                if (!controllerTypeFound)
                    controllerType = ControllerType.Any;
            }
            else
                controllerType = ControllerType.Any;

            bool debug;
            if (!string.IsNullOrEmpty(context.Request["debug"]))
            {
                bool debugFound = bool.TryParse(context.Request["debug"], out debug);
                if (!debugFound)
                    debug = false;
            }
            else
                debug = false;

            string windowLocation = "";
            if (!string.IsNullOrEmpty(context.Request["windowLocation"]))
                windowLocation = context.Request["windowLocation"];

            // Set the caching settings.            
            context.Response.Cache.SetNoServerCaching();
            context.Response.Cache.SetNoStore();
            context.Response.ContentType = "text/javascript";

            // Write the response.
            context.Response.Write(ScriptGenerator.GetInPageServicesScript(controllerType, windowLocation, debug));
        }

        #endregion

    }

}