﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kiwi.WebServer
{

	/// <summary>
	/// Summary description for InPageServiceScript
	/// </summary>
	public class WindowServicesScriptGenerator : IHttpHandler
	{

		#region IHttpHandler

		/// <summary>
		/// Gets a value indicating whether another request can use the IHttpHandler instance.
		/// </summary>
		public bool IsReusable { get { return false; } }

		/// <summary>
		/// Processes the request to this handler.
		/// </summary>
		/// <param name="context">Context for the request.</param>
		public void ProcessRequest(HttpContext context)
		{
			bool debug;
			if (!string.IsNullOrEmpty(context.Request["debug"]))
			{
				bool debugFound = bool.TryParse(context.Request["debug"], out debug);
				if (!debugFound)
					debug = false;
			}
			else
				debug = false;

			string guid = "";
			if (!string.IsNullOrEmpty(context.Request["guid"]))
				guid = context.Request["guid"];

            ControllerType controllerType;
            if (!string.IsNullOrEmpty(context.Request["controllerType"]))
            {
                bool controllerTypeFound = Enum.TryParse(context.Request["controllerType"], true, out controllerType);
                if (!controllerTypeFound)
                    controllerType = ControllerType.Any;
            }
            else
                controllerType = ControllerType.Any;

			// Set the caching settings.
            if (debug)
            {
                context.Response.Cache.SetNoServerCaching();
                context.Response.Cache.SetNoStore();
            }
			context.Response.ContentType = "text/javascript";

			// Write the response.
			context.Response.Write(ScriptGenerator.GetWindowServicesScript(context.Request.UrlReferrer.ToString(), guid, debug, controllerType));
		}

		#endregion

	}

}