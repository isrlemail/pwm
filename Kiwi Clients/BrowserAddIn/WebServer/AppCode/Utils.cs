﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kiwi.WebServer
{

    /// <summary>
    /// Class with various utility functions that we use.
    /// </summary>
    /// <remarks>
    /// Most of these should be moved to the web config.  I just noticed to late, and
    /// since the project is mostly finished I don't feel like changing it, sorry.
    /// </remarks>
    public static class Utils
    {

        #region Server variables

        /// <summary>
        /// Gets a reference to the current server utility class.
        /// </summary>
        public static HttpServerUtility Server { get { return HttpContext.Current.Server; } }

        /// <summary>
        /// Gets the Uri of the current server.  Does not contain final /.
        /// </summary>
        public static string ServerUri
        {
            get
            {
                string serverUri = new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
                        VirtualPathUtility.ToAbsolute("~/")).ToString();
                return serverUri.Substring(0, serverUri.Length - 1);
            }
        }

        /// <summary>
        /// Gets the Uri of the current application.  This is also the version /.
        /// </summary>
        public static string Version
        {
            get
            {
                return VirtualPathUtility.ToAbsolute("~").Substring(1);
            }
        }

        /// <summary>
        /// Gets the Uri of the current domain.
        /// </summary>
        public static string DomainUri { get { return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority); } }

        /// <summary>
        /// Gets the list of trusted key servers.
        /// </summary>
        public static string[] TrustedKeyServers { get { return new string[] { @"https://pwm.byu.edu/ks/" }; } }

        /// <summary>
        /// List of destinations the proxy will retrieve documents form.
        /// </summary>
        public static string[] TrustedProxyDestinations { get { return (new string[] { @"https://pwm.byu.edu/auth/" }.Concat(TrustedKeyServers)).ToArray(); } }

        #endregion

        #region Remote library locations

        /// <summary>
        /// Gets the Uri where we get our JQuery library from.
        /// </summary>
        public static string JQueryLibraryUri { get { return "//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"; } }

        /// <summary>
        /// Gets the Uri where we get our MooTools library from.
        /// </summary>
        public static string MooToolsLibraryUri { get { return "//ajax.googleapis.com/ajax/libs/mootools/1.5.1/mootools-yui-compressed.js"; } }

        /// <summary>
        /// Gets the Uri where we stored the Bootstrap Tour library.
        /// </summary>
        public static string CSSFiles
        {
            get
            {
                return String.Format("'{0}'", String.Join("','",
                    new String[] {
                        new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
                            VirtualPathUtility.ToAbsolute("~/css/pwm.css")).ToString(),
                        new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
                            VirtualPathUtility.ToAbsolute("~/css/mask.css")).ToString(),
                        new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
                            VirtualPathUtility.ToAbsolute("~/css/bootstrap-tour-standalone.css")).ToString(),
                        new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
                            VirtualPathUtility.ToAbsolute("~/css/pwm-tour-standalone.css")).ToString()
                    }
                ));
            }
        }
        #endregion
    }
}