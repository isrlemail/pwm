﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Yahoo.Yui.Compressor;

namespace Kiwi.WebServer
{
	public class BookmarkletScriptGenerator : IHttpHandler
	{
		private const string TYPE_KEY = "type";

		public bool IsReusable
		{
			get { return false; }
		}

		public void ProcessRequest(HttpContext context)
		{
			// Parse response variables.
			ControllerType controllerType = ControllerType.Any;
			if (!string.IsNullOrEmpty(context.Request[TYPE_KEY]))
			{
				bool controllerTypeFound = Enum.TryParse(context.Request[TYPE_KEY], true, out controllerType);
				if (!controllerTypeFound)
					controllerType = ControllerType.Any;
			}

			if (controllerType == ControllerType.Any)
				throw new HttpException((int)HttpStatusCode.BadRequest, "You must specify a specific type in the query string (?type=(chat|email))");

			bool debug = false;
			if (!string.IsNullOrEmpty(context.Request["debug"]))
			{
				bool debugFound = bool.TryParse(context.Request["debug"], out debug);
				if (!debugFound)
					debug = false;
			}

			// Get the javascript.
			string bookmarkletJavascriptPath = context.Server.MapPath("~/InPageServices/Loader.url");
			string bookmarkletJavascipt = File.ReadAllText(bookmarkletJavascriptPath);

			// Minimize and change quotes to single quotes.
            string compressedJavascript = new JavaScriptCompressor().Compress(bookmarkletJavascipt).Replace('"', '\'')
                .Replace("@JQUERY", Utils.JQueryLibraryUri)
                .Replace("@SERVER_URI", Utils.ServerUri)
                .Replace("@VERSION", Utils.Version)
                .Replace("@MOOTOOLS", Utils.MooToolsLibraryUri)
                .Replace("@CSSFILES", Utils.CSSFiles);

			// Set the caching settings.            
			context.Response.Cache.SetNoServerCaching();
			context.Response.Cache.SetNoStore();
			context.Response.ContentType = "text/plain";

			context.Response.Write(InsertValues(compressedJavascript, controllerType.ToString(), debug, String.Format("Pwm{0}InPageServices", controllerType.ToString())));
		}

		/// <summary>
		/// Inserts the desired values into the javascript string.
		/// </summary>
		/// <param name="javascript">Javascript string to insert into.</param>
		/// <param name="type">Type of service requested.</param>
		/// <param name="debug">Whether to enable debugging or not.</param>
		/// <param name="serviceTypeId">ID of the service script.</param>
		/// <returns>Javascript to insert into page.</returns>
		private string InsertValues(string javascript, string type, bool debug, string serviceTypeId)
		{
			return javascript.
					Replace("@CONTROLLER_TYPE", type).
					Replace("@DEBUG", debug.ToString().ToLower()).
					Replace("@SERVICE_ID", serviceTypeId);
		}
	}
}