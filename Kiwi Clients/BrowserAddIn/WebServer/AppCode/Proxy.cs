﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace Kiwi.WebServer
{

    /// <summary>
    /// Class that handles proxy communication for AJAX requests.
    /// </summary>
    public class Proxy : IHttpAsyncHandler
    {

        #region State Object

        /// <summary>
        /// State object for the async calls.
        /// </summary>
        private class StateObject
        {

            /// <summary>
            /// Original HttpContext.
            /// </summary>
            public HttpContext Context { get; set; }

            /// <summary>
            /// Web request being made.
            /// </summary>
            public HttpWebRequest Request { get; set; }

        }

        #endregion

        #region IHttpAsyncHandler

        /// <summary>
        /// Starts a proxy request.
        /// </summary>
        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            string url = GetSafeUrl(context);
            if (url == null)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                context.Response.Flush();
                context.Response.End();
                return null;
            }

            HttpWebRequest request = CreateProxyRequest(url, context.Request);
            
            // Write anything neccessary to the request.
            if(request.ContentLength != -1)
                context.Request.InputStream.CopyTo(request.GetRequestStream());
            
            return request.BeginGetResponse(cb, new StateObject { Context = context, Request = request });
        }

        /// <summary>
        /// Ends the proxy request.
        /// </summary>
        public void EndProcessRequest(IAsyncResult result)
        {
            StateObject state = result.AsyncState as StateObject;
            try
            {
                using (HttpWebResponse response = state.Request.EndGetResponse(result) as HttpWebResponse)
                {
                    // Set the status code.
                    state.Context.Response.Clear();
                    state.Context.Response.StatusCode = (int)response.StatusCode;

                    // Copy the headers over.
                    foreach (var headerKey in response.Headers.AllKeys)
                        state.Context.Response.AddHeader(headerKey, response.Headers[headerKey]);
                    state.Context.Response.ContentType = response.ContentType;

                    // Write the content.
                    response.GetResponseStream().CopyTo(state.Context.Response.OutputStream);
                }
            }
            catch(WebException e)
            {
                state.Context.Response.StatusCode = (int)((HttpWebResponse)e.Response).StatusCode;
            }
        }

        /// <summary>
        /// Processes the request synchronously.
        /// </summary>
        public void ProcessRequest(HttpContext context)
        {
            string url = GetSafeUrl(context);
            if (url == null)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                context.Response.Flush();
                context.Response.End();
                return;
            }

            HttpWebRequest request = CreateProxyRequest(url, context.Request);
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Set the status code.
                context.Response.StatusCode = (int)response.StatusCode;

                // Copy the headers over.
                foreach (var headerKey in response.Headers.AllKeys)
                    context.Response.Headers[headerKey] = response.Headers[headerKey];

                // Write the content.
                response.GetResponseStream().CopyTo(context.Response.OutputStream);
            }
        }

        /// <summary>
        /// Whether this object is reusable.
        /// </summary>
        public bool IsReusable
        {
            get { return false; }
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Gets a safe URL, or returns null if this is an unsafe url to use.
        /// </summary>
        /// <param name="context">Context to get url from.</param>
        /// <returns>Safe url to do a proxy request for, or null if none exists.</returns>
        private string GetSafeUrl(HttpContext context)
        {
            // Only handle local requests.
            //if (!context.Request.)
            //    return null;

            // Ensure the url is a server we trust.
            string url = context.Request["url"];
            bool found = false;
            foreach (string server in Utils.TrustedProxyDestinations)
                if (url.StartsWith(server, StringComparison.OrdinalIgnoreCase))
                {
                    found = true;
                    break;
                }

            if (!found)
                return null;

            return url;
        }

        /// <summary>
        /// Create a proxy request from the original request to the given url.
        /// </summary>
        /// <param name="url">Url to request.</param>
        /// <param name="originalRequest">Original request object.</param>
        /// <returns>HttpWebRequest to make the proxy request.</returns>
        private HttpWebRequest CreateProxyRequest(string url, HttpRequest originalRequest)
        {
            HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;

            foreach (var headerKey in originalRequest.Headers.AllKeys)
            {
                switch (headerKey)
                {
                    case "Accept":
                        request.Accept = originalRequest.Headers[headerKey];
                        break;

                    case "Connection":
                        request.KeepAlive = originalRequest.Headers[headerKey].Equals("keep-alive");
                        break;

                    case "Content-Length":
                        request.ContentLength = originalRequest.ContentLength;
                        break;

                    case "Content-Type":
                        request.ContentType = originalRequest.Headers[headerKey];
                        break;

                    case "Date":
                        request.Date = DateTime.Parse(originalRequest.Headers[headerKey]);
                        break;

                    case "Expect":
                        request.Expect = originalRequest.Headers[headerKey];
                        break;

                    case "If-Modified-Since":
                        request.IfModifiedSince = DateTime.Parse(originalRequest.Headers[headerKey]);
                        break;

                    case "Referer":
                        request.Referer = originalRequest.Headers[headerKey];
                        break;

                    case "Transfer-Encoding":
                        request.TransferEncoding = originalRequest.Headers[headerKey];
                        break;

                    case "User-Agent":
                        request.UserAgent = originalRequest.Headers[headerKey];
                        break;

                    case "Host":
                    case "Range":
                        break;

                    default:
                        request.Headers[headerKey] = originalRequest.Headers[headerKey];
                        break;
                }

            }

            request.Method = originalRequest.HttpMethod;
            request.AutomaticDecompression = DecompressionMethods.None;
            return request;
        }

        #endregion

    }

}