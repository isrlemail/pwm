﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReadWindow.aspx.cs" Inherits="Kiwi.WebServer.ReadWindow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Secure Read Window</title>

    <script type="text/javascript" src="<%= Kiwi.WebServer.Utils.JQueryLibraryUri %>"></script>
    <script type="text/javascript" src="<%= Kiwi.WebServer.Utils.MooToolsLibraryUri %>"></script>
    
    <!-- Add our page specific stuff. -->
    <link rel="stylesheet" type="text/css" href='<%= Page.ResolveUrl("~/css/ReadWindow.css") %>' />
    <link rel="stylesheet" type="text/css" href='<%= Page.ResolveUrl("~/css/mask.css") %>' />
    <script type="text/javascript" src="<%= Page.ResolveUrl(string.Format("~/WindowServices.js?guid={0}&debug={1}", Request["guid"] ?? "", Request["debug"] ?? "false")) %>"></script>
</head>
<body>
    <font size="2" face="arial, helvetica, sans-serif">
        <div id="messageBodyArea"></div>
    </font>
</body>
</html>
