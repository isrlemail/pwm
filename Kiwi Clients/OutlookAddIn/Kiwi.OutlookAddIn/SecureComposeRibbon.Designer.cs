﻿namespace Kiwi.OutlookAddIn
{
    partial class SecureComposeRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public SecureComposeRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabNewMailMessage = this.Factory.CreateRibbonTab();
            this.GroupKiwiMail = this.Factory.CreateRibbonGroup();
            this.ToggleButtonSecureMessage = this.Factory.CreateRibbonToggleButton();
            this.TabNewMailMessage.SuspendLayout();
            this.GroupKiwiMail.SuspendLayout();
            // 
            // TabNewMailMessage
            // 
            this.TabNewMailMessage.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.TabNewMailMessage.ControlId.OfficeId = "TabNewMailMessage";
            this.TabNewMailMessage.Groups.Add(this.GroupKiwiMail);
            this.TabNewMailMessage.Label = "TabNewMailMessage";
            this.TabNewMailMessage.Name = "TabNewMailMessage";
            // 
            // GroupKiwiMail
            // 
            this.GroupKiwiMail.Items.Add(this.ToggleButtonSecureMessage);
            this.GroupKiwiMail.Label = "Kiwi Mail";
            this.GroupKiwiMail.Name = "GroupKiwiMail";
            this.GroupKiwiMail.Position = this.Factory.RibbonPosition.BeforeOfficeId("GroupClipboard");
            // 
            // ToggleButtonSecureMessage
            // 
            this.ToggleButtonSecureMessage.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.ToggleButtonSecureMessage.Image = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLock;
            this.ToggleButtonSecureMessage.Label = "Secure Message";
            this.ToggleButtonSecureMessage.Name = "ToggleButtonSecureMessage";
            this.ToggleButtonSecureMessage.Position = this.Factory.RibbonPosition.BeforeOfficeId("GroupClipboard");
            this.ToggleButtonSecureMessage.ScreenTip = "Secure the message";
            this.ToggleButtonSecureMessage.ShowImage = true;
            this.ToggleButtonSecureMessage.SuperTip = "Secure the message using the kiwi mail system.";
            this.ToggleButtonSecureMessage.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ToggleButtonSecureMessage_Click);
            // 
            // SecureComposeRibbon
            // 
            this.Name = "SecureComposeRibbon";
            this.RibbonType = "Microsoft.Outlook.Mail.Compose";
            this.Tabs.Add(this.TabNewMailMessage);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.SecureComposeRibbon_Load);
            this.TabNewMailMessage.ResumeLayout(false);
            this.TabNewMailMessage.PerformLayout();
            this.GroupKiwiMail.ResumeLayout(false);
            this.GroupKiwiMail.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab TabNewMailMessage;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup GroupKiwiMail;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton ToggleButtonSecureMessage;
    }

    partial class ThisRibbonCollection
    {
        internal SecureComposeRibbon SecureComposeRibbon
        {
            get { return this.GetRibbon<SecureComposeRibbon>(); }
        }
    }
}
