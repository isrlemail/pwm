﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools = Microsoft.Office.Tools.Outlook;
using Outlook = Microsoft.Office.Interop.Outlook;
using Kiwi.LibKiwi;
using System.Threading;
using Kiwi.KeyManagement;
using System.Globalization;
using System.Security;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Kiwi.OutlookAddIn
{

    /// <summary>
    /// This class is responsible for displaying secured messages.
    /// </summary>
    partial class SecureReadForm
    {

        #region Properties

        /// <summary>
        /// The message this form is being displayed for.
        /// </summary>
        private Outlook.MailItem MailItem { get; set; }

        /// <summary>
        /// The wrapper for the e-mail being parsed.
        /// </summary>
        private EmailPackageWrapper Wrapper { get; set; }

        #endregion

        #region Form Region Factory

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
        [Tools.FormRegionMessageClass(KiwiAddIn.KiwiMessageClass)]
        [Tools.FormRegionName("Kiwi.OutlookAddIn.SecureReadForm")]
        public partial class SecureReadFormFactory
        {
            // Occurs before the form region is initialized.
            // To prevent the form region from appearing, set e.Cancel to true.
            // Use e.OutlookItem to get a reference to the current Outlook item.
            private void SecureReadFormFactory_FormRegionInitializing(object sender, Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs e)
            {
                Outlook.MailItem mailItem = (Outlook.MailItem)e.OutlookItem;

                // Makes sure the package is actually wrapped.
                EmailPackageWrapper wrapper;
                if (!EmailPackageWrapper.IsWrappedMessage(mailItem.Subject, mailItem.HTMLBody))
                {
                    mailItem.MessageClass = Tools.FormRegionMessageClassAttribute.Note;
                    mailItem.Save();
                    e.Cancel = true;
                }
            }
        }

        #endregion

        #region Form events

        /// <summary>
        /// Fires before the form region is displayed.
        /// </summary>
        /// <remarks>
        /// Use this.OutlookItem to get a reference to the current Outlook item.
        /// Use this.OutlookFormRegion to get a reference to the form region.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void SecureReadForm_FormRegionShowing(object sender, System.EventArgs e)
        {
            MailItem = (Outlook.MailItem)OutlookItem;
                                
            // Gaurunteed to work since this form won't show up if it doesn't.
            EmailPackageWrapper localWrapper;
            EmailPackageWrapper.TryCreateWrapper(MailItem.Subject, MailItem.HTMLBody, out localWrapper);
            Wrapper = localWrapper;

            // Do this as a task so as not to block the main thread.
            Task.Factory.StartNew(() =>
            {
                try
                {
                    // Step 1 - Unwrap the basic information about the e-mail and show it to the user.
                    UpdateStatus("Reading e-mail");
                    SetupSubjectPanel();

                    string viewerId = null;
                    if (MailItem.MessageClass == KiwiAddIn.DraftMessageClass)
                        viewerId = MailItem.SendUsingAccount.SmtpAddress;
                    else
                    {
                        // We call it this way, or sometimes the property is not set.
                        string entryId = ((dynamic)MailItem).ReceivedByEntryID;
                        string[] parts = entryId.Split(new[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
                        viewerId = parts[parts.Length - 1].Trim();
                    }

                    // No valid creator id found.
                    if (viewerId == null)
                    {
                        ReportError("You are not one of the recipients allowed to read this message.  Please contact the sender if this is a mistake.");
                        return;
                    }

                    // Step 2 - Unwrap the package from the e-mail, still no decryption.
                    UpdateStatus("Unwrapping package");
                    Package package = null;
                    EmailPackager packager = null;
                    try
                    {

                        // Unwrap the message, and then decrypt.
                        package = Wrapper.Package;
                        packager = new EmailPackager(package);
                    }
                    catch
                    {
                        ReportError("Unable to unwrap package.");
                        return;
                    }

                    // Update the display with information from the package.
                    MessageBody.Invoke(new Action(() =>
                    {
                        CreatorId.Text = packager.CreatorId;
                        MessageTimestamp.Text = packager.Timestamp.ToString();
                        KeyServerUri.Text = packager.KeyServerUri.ToString();
                    }));


                    // Step 3 - Get a viewer key to read the message with.
                    UpdateStatus("Retreiving viewer key");
                    Globals.KiwiAddIn.KeyManager.GetViewerKeyAsync(new KeyRequestArgs(packager.KeyServerUri, packager.CreatorId, viewerId, packager.Timestamp, EmailPackager.PackageKeySize),
                        (success, value, args) =>
                        {
                            if (!success)
                            {
                                ReportError("Failed to retreive a key from the server.");
                                return;
                            }

                            // Step 4 - Decrypt the package.
                            UpdateStatus("Decrypting package");

                            try
                            {
                                packager.Unwrap(package, viewerId, value);
                            }
                            catch (CryptographicException)
                            {
                                ReportError("The package has been modified since it was sent.  Get a new copy from the sender.");
                                return;
                            }
                            catch (SecurityException)
                            {
                                ReportError("You are not one of the recipients allowed to read this message.  Please contact the sender if this is a mistake.");
                                return;
                            }
                            catch
                            {
                                ReportError("Failed to decrypt package.");
                                return;
                            }

                            SetMessageBody((string)packager.Message.Contents);
                            UpdateUIAsync(() => { LockIcon.Visible = true; });
                            UpdateStatus("Finished");
                        });
                }
                // This should only happen if the GUI has gone out of existance before we were done.
                catch {}
            });
        }

        /// <summary>
        /// Fires when the form region is closed.
        /// </summary>
        /// <remarks>
        /// Use this.OutlookItem to get a reference to the current Outlook item.
        /// Use this.OutlookFormRegion to get a reference to the form region.
        /// </remarks>
        private void SecureReadForm_FormRegionClosed(object sender, System.EventArgs e)
        {
        }

        #endregion

        #region GUI update methods

        /// <summary>
        /// Sets up the various items in the subject panel.
        /// </summary>
        private void SetupSubjectPanel()
        {
            UpdateUIAsync(() =>
            {
                Subject.Text = Wrapper.Subject;
                SenderAddress.Text = string.IsNullOrEmpty(MailItem.SenderName) ?
                    MailItem.SenderEmailAddress : string.Format(CultureInfo.CurrentCulture, "{0} [{1}]", MailItem.SenderName, MailItem.SenderEmailAddress);
                SentTime.Text = MailItem.SentOn.ToString("ddd M/d/yyyy h:mm tt", CultureInfo.CurrentUICulture);
                ToAddresses.Text = MailItem.To;

                // Show CC and BCC only if they have values.
                if (!string.IsNullOrEmpty(MailItem.CC))
                {
                    CCAddresses.Text = MailItem.CC;
                    SubjectTable.RowStyles[SubjectTable.GetRow(CCAddresses)].SizeType = System.Windows.Forms.SizeType.AutoSize;
                }

                if (!string.IsNullOrEmpty(MailItem.BCC))
                {
                    BccAddresses.Text = MailItem.BCC;
                    SubjectTable.RowStyles[SubjectTable.GetRow(BccAddresses)].SizeType = System.Windows.Forms.SizeType.AutoSize;
                }
            });
        }

        /// <summary>
        /// Updates the status message and progress bar.
        /// </summary>
        /// <param name="status">Status to update to.</param>
        /// <param name="finished">Whether we are now finished.</param>
        private void UpdateStatus(string status)
        {
            UpdateUIAsync(() =>
            {
                Status.Text = status;
                ProgressBar.PerformStep();
            });
        }

        /// <summary>
        /// Called to report an error to the user.
        /// </summary>
        /// <param name="error">The error to report to the user.</param>
        private void ReportError(string error)
        {
            UpdateUIAsync(() =>
            {
                Status.Text = "Error";
                LockIcon.Visible = false;
                ProgressBar.Value = ProgressBar.Maximum;
                ErrorMessage.Text = error;
                SetMessageBody(string.Format(CultureInfo.CurrentUICulture, "There was an error.  {0}", error));
            });
        }

        /// <summary>
        /// Sets the message body.
        /// </summary>
        /// <param name="contents">Contents to set.</param>
        /// <param name="isHtml">Whether the contents are HTML or plain text.</param>
        private void SetMessageBody(string contents)
        {
            UpdateUIAsync(() =>
            {
                MessageBody.DocumentText = contents;
            });
        }

        /// <summary>
        /// Used to update the UI asynchronously.
        /// </summary>
        /// <param name="action">Action to perform to update UI.</param>
        private void UpdateUIAsync(Action action)
        {
            MessageBody.Invoke(action);
        }

        #endregion

    }
}
