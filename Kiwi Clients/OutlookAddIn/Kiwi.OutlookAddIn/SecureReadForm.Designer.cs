﻿namespace Kiwi.OutlookAddIn
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class SecureReadForm : Microsoft.Office.Tools.Outlook.FormRegionBase
    {
        public SecureReadForm(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            : base(Globals.Factory, formRegion)
        {
            this.InitializeComponent();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.GroupBox DetailsGroup;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label BccLabel;
            System.Windows.Forms.Label CCLabel;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Panel BorderPanel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SecureReadForm));
            this.KeyServerUri = new System.Windows.Forms.Label();
            this.CreatorId = new System.Windows.Forms.Label();
            this.MessageTimestamp = new System.Windows.Forms.Label();
            this.MessageBody = new System.Windows.Forms.WebBrowser();
            this.MainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.SubjectTable = new System.Windows.Forms.TableLayoutPanel();
            this.SenderAddress = new System.Windows.Forms.Label();
            this.Subject = new System.Windows.Forms.Label();
            this.SentTime = new System.Windows.Forms.Label();
            this.ToAddresses = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.ErrorMessage = new System.Windows.Forms.Label();
            this.LockIcon = new System.Windows.Forms.PictureBox();
            this.CCAddresses = new System.Windows.Forms.Label();
            this.BccAddresses = new System.Windows.Forms.Label();
            DetailsGroup = new System.Windows.Forms.GroupBox();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            BccLabel = new System.Windows.Forms.Label();
            CCLabel = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            BorderPanel = new System.Windows.Forms.Panel();
            DetailsGroup.SuspendLayout();
            this.MainLayout.SuspendLayout();
            this.SubjectTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LockIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // DetailsGroup
            // 
            DetailsGroup.AutoSize = true;
            DetailsGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            DetailsGroup.Controls.Add(this.KeyServerUri);
            DetailsGroup.Controls.Add(this.CreatorId);
            DetailsGroup.Controls.Add(this.MessageTimestamp);
            DetailsGroup.Controls.Add(label3);
            DetailsGroup.Controls.Add(label2);
            DetailsGroup.Controls.Add(label1);
            DetailsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            DetailsGroup.Font = new System.Drawing.Font("Tahoma", 8.25F);
            DetailsGroup.Location = new System.Drawing.Point(3, 911);
            DetailsGroup.Name = "DetailsGroup";
            DetailsGroup.Size = new System.Drawing.Size(1327, 82);
            DetailsGroup.TabIndex = 12;
            DetailsGroup.TabStop = false;
            DetailsGroup.Text = "Message Details";
            // 
            // KeyServerUri
            // 
            this.KeyServerUri.AutoSize = true;
            this.KeyServerUri.Location = new System.Drawing.Point(133, 52);
            this.KeyServerUri.Name = "KeyServerUri";
            this.KeyServerUri.Size = new System.Drawing.Size(76, 13);
            this.KeyServerUri.TabIndex = 17;
            this.KeyServerUri.Text = "Not Encrypted";
            // 
            // CreatorId
            // 
            this.CreatorId.AutoSize = true;
            this.CreatorId.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.CreatorId.Location = new System.Drawing.Point(133, 16);
            this.CreatorId.Name = "CreatorId";
            this.CreatorId.Size = new System.Drawing.Size(76, 13);
            this.CreatorId.TabIndex = 15;
            this.CreatorId.Text = "Not Encrypted";
            // 
            // MessageTimestamp
            // 
            this.MessageTimestamp.AutoSize = true;
            this.MessageTimestamp.Location = new System.Drawing.Point(133, 34);
            this.MessageTimestamp.Name = "MessageTimestamp";
            this.MessageTimestamp.Size = new System.Drawing.Size(76, 13);
            this.MessageTimestamp.TabIndex = 16;
            this.MessageTimestamp.Text = "Not Encrypted";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(6, 52);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(64, 13);
            label3.TabIndex = 14;
            label3.Text = "Key Server:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(6, 34);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(107, 13);
            label2.TabIndex = 13;
            label2.Text = "Message Timestamp:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(6, 16);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(48, 13);
            label1.TabIndex = 12;
            label1.Text = "Creator:";
            // 
            // BccLabel
            // 
            BccLabel.AutoSize = true;
            BccLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            BccLabel.Font = new System.Drawing.Font("Tahoma", 8.25F);
            BccLabel.Location = new System.Drawing.Point(3, 66);
            BccLabel.Name = "BccLabel";
            BccLabel.Size = new System.Drawing.Size(34, 1);
            BccLabel.TabIndex = 21;
            BccLabel.Text = "BCC:";
            // 
            // CCLabel
            // 
            CCLabel.AutoSize = true;
            CCLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            CCLabel.Font = new System.Drawing.Font("Tahoma", 8.25F);
            CCLabel.Location = new System.Drawing.Point(3, 66);
            CCLabel.Name = "CCLabel";
            CCLabel.Size = new System.Drawing.Size(34, 1);
            CCLabel.TabIndex = 20;
            CCLabel.Text = "CC:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Dock = System.Windows.Forms.DockStyle.Fill;
            label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(3, 53);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(34, 13);
            label5.TabIndex = 14;
            label5.Text = "To:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Dock = System.Windows.Forms.DockStyle.Fill;
            label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(3, 40);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(34, 13);
            label4.TabIndex = 12;
            label4.Text = "Sent:";
            // 
            // BorderPanel
            // 
            BorderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            BorderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            BorderPanel.Location = new System.Drawing.Point(3, 80);
            BorderPanel.Name = "BorderPanel";
            BorderPanel.Size = new System.Drawing.Size(1327, 1);
            BorderPanel.TabIndex = 18;
            // 
            // MessageBody
            // 
            this.MessageBody.AllowNavigation = false;
            this.MessageBody.AllowWebBrowserDrop = false;
            this.MessageBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessageBody.IsWebBrowserContextMenuEnabled = false;
            this.MessageBody.Location = new System.Drawing.Point(3, 82);
            this.MessageBody.MinimumSize = new System.Drawing.Size(20, 20);
            this.MessageBody.Name = "MessageBody";
            this.MessageBody.ScriptErrorsSuppressed = true;
            this.MessageBody.Size = new System.Drawing.Size(1327, 823);
            this.MessageBody.TabIndex = 16;
            this.MessageBody.WebBrowserShortcutsEnabled = false;
            // 
            // MainLayout
            // 
            this.MainLayout.ColumnCount = 1;
            this.MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainLayout.Controls.Add(this.SubjectTable, 0, 1);
            this.MainLayout.Controls.Add(this.MessageBody, 0, 3);
            this.MainLayout.Controls.Add(DetailsGroup, 0, 4);
            this.MainLayout.Controls.Add(BorderPanel, 0, 2);
            this.MainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayout.Location = new System.Drawing.Point(0, 0);
            this.MainLayout.Name = "MainLayout";
            this.MainLayout.RowCount = 5;
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.MainLayout.Size = new System.Drawing.Size(1333, 996);
            this.MainLayout.TabIndex = 18;
            // 
            // SubjectTable
            // 
            this.SubjectTable.AutoSize = true;
            this.SubjectTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SubjectTable.ColumnCount = 4;
            this.SubjectTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.SubjectTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.SubjectTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.SubjectTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.SubjectTable.Controls.Add(this.SenderAddress, 0, 1);
            this.SubjectTable.Controls.Add(this.Subject, 0, 0);
            this.SubjectTable.Controls.Add(label4, 0, 2);
            this.SubjectTable.Controls.Add(this.SentTime, 1, 2);
            this.SubjectTable.Controls.Add(label5, 0, 3);
            this.SubjectTable.Controls.Add(this.ToAddresses, 1, 3);
            this.SubjectTable.Controls.Add(this.Status, 3, 0);
            this.SubjectTable.Controls.Add(this.ProgressBar, 3, 1);
            this.SubjectTable.Controls.Add(this.ErrorMessage, 3, 2);
            this.SubjectTable.Controls.Add(this.LockIcon, 2, 0);
            this.SubjectTable.Controls.Add(CCLabel, 0, 4);
            this.SubjectTable.Controls.Add(BccLabel, 0, 5);
            this.SubjectTable.Controls.Add(this.CCAddresses, 1, 4);
            this.SubjectTable.Controls.Add(this.BccAddresses, 1, 5);
            this.SubjectTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SubjectTable.Location = new System.Drawing.Point(3, 8);
            this.SubjectTable.Name = "SubjectTable";
            this.SubjectTable.RowCount = 4;
            this.SubjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.SubjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.SubjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.SubjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.SubjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.SubjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.SubjectTable.Size = new System.Drawing.Size(1327, 66);
            this.SubjectTable.TabIndex = 17;
            // 
            // SenderAddress
            // 
            this.SenderAddress.AutoSize = true;
            this.SubjectTable.SetColumnSpan(this.SenderAddress, 2);
            this.SenderAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SenderAddress.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SenderAddress.Location = new System.Drawing.Point(3, 19);
            this.SenderAddress.Name = "SenderAddress";
            this.SenderAddress.Size = new System.Drawing.Size(1081, 21);
            this.SenderAddress.TabIndex = 11;
            this.SenderAddress.Text = "Sender Address";
            // 
            // Subject
            // 
            this.Subject.AutoSize = true;
            this.SubjectTable.SetColumnSpan(this.Subject, 2);
            this.Subject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Subject.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Subject.Location = new System.Drawing.Point(3, 0);
            this.Subject.Name = "Subject";
            this.Subject.Size = new System.Drawing.Size(1081, 19);
            this.Subject.TabIndex = 10;
            this.Subject.Text = "Subject";
            // 
            // SentTime
            // 
            this.SentTime.AutoSize = true;
            this.SentTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SentTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SentTime.Location = new System.Drawing.Point(43, 40);
            this.SentTime.Name = "SentTime";
            this.SentTime.Size = new System.Drawing.Size(1041, 13);
            this.SentTime.TabIndex = 13;
            this.SentTime.Text = "SentTime";
            // 
            // ToAddresses
            // 
            this.ToAddresses.AutoSize = true;
            this.ToAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToAddresses.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToAddresses.Location = new System.Drawing.Point(43, 53);
            this.ToAddresses.Name = "ToAddresses";
            this.ToAddresses.Size = new System.Drawing.Size(1041, 13);
            this.ToAddresses.TabIndex = 15;
            this.ToAddresses.Text = "ToAddresses";
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Status.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Location = new System.Drawing.Point(1130, 0);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(194, 19);
            this.Status.TabIndex = 16;
            this.Status.Text = "Starting";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProgressBar.Location = new System.Drawing.Point(1130, 22);
            this.ProgressBar.Maximum = 5;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(194, 15);
            this.ProgressBar.Step = 1;
            this.ProgressBar.TabIndex = 17;
            // 
            // ErrorMessage
            // 
            this.ErrorMessage.AutoSize = true;
            this.ErrorMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ErrorMessage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.ErrorMessage.Location = new System.Drawing.Point(1130, 40);
            this.ErrorMessage.Name = "ErrorMessage";
            this.SubjectTable.SetRowSpan(this.ErrorMessage, 4);
            this.ErrorMessage.Size = new System.Drawing.Size(194, 26);
            this.ErrorMessage.TabIndex = 18;
            // 
            // LockIcon
            // 
            this.LockIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LockIcon.Image = ((System.Drawing.Image)(resources.GetObject("LockIcon.Image")));
            this.LockIcon.Location = new System.Drawing.Point(1090, 3);
            this.LockIcon.Name = "LockIcon";
            this.SubjectTable.SetRowSpan(this.LockIcon, 6);
            this.LockIcon.Size = new System.Drawing.Size(34, 60);
            this.LockIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LockIcon.TabIndex = 19;
            this.LockIcon.TabStop = false;
            this.LockIcon.Visible = false;
            // 
            // CCAddresses
            // 
            this.CCAddresses.AutoSize = true;
            this.CCAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CCAddresses.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.CCAddresses.Location = new System.Drawing.Point(43, 66);
            this.CCAddresses.Name = "CCAddresses";
            this.CCAddresses.Size = new System.Drawing.Size(1041, 1);
            this.CCAddresses.TabIndex = 22;
            this.CCAddresses.Text = "CCAddresses";
            // 
            // BccAddresses
            // 
            this.BccAddresses.AutoSize = true;
            this.BccAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BccAddresses.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.BccAddresses.Location = new System.Drawing.Point(43, 66);
            this.BccAddresses.Name = "BccAddresses";
            this.BccAddresses.Size = new System.Drawing.Size(1041, 1);
            this.BccAddresses.TabIndex = 23;
            this.BccAddresses.Text = "BccAddresses";
            // 
            // SecureReadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainLayout);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Name = "SecureReadForm";
            this.Size = new System.Drawing.Size(1333, 996);
            this.FormRegionShowing += new System.EventHandler(this.SecureReadForm_FormRegionShowing);
            this.FormRegionClosed += new System.EventHandler(this.SecureReadForm_FormRegionClosed);
            DetailsGroup.ResumeLayout(false);
            DetailsGroup.PerformLayout();
            this.MainLayout.ResumeLayout(false);
            this.MainLayout.PerformLayout();
            this.SubjectTable.ResumeLayout(false);
            this.SubjectTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LockIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        #region Form Region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private static void InitializeManifest(Microsoft.Office.Tools.Outlook.FormRegionManifest manifest, Microsoft.Office.Tools.Outlook.Factory factory)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SecureReadForm));
            manifest.Description = "Used to display secure Kiwi messages.";
            manifest.FormRegionName = "SecureReadForm";
            manifest.FormRegionType = Microsoft.Office.Tools.Outlook.FormRegionType.ReplaceAll;
            manifest.Icons.Default = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Encrypted = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Forwarded = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Page = ((System.Drawing.Image)(resources.GetObject("SecureReadForm.Manifest.Icons.Page")));
            manifest.Icons.Read = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Recurring = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Replied = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Signed = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Submitted = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Unread = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Unsent = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.Icons.Window = global::Kiwi.OutlookAddIn.Properties.Resources.GlassLockIcon;
            manifest.ShowInspectorCompose = false;
            manifest.Title = "SecureReadForm";

        }

        #endregion

        private System.Windows.Forms.Label KeyServerUri;
        private System.Windows.Forms.Label CreatorId;
        private System.Windows.Forms.Label MessageTimestamp;
        private System.Windows.Forms.WebBrowser MessageBody;
        private System.Windows.Forms.TableLayoutPanel MainLayout;
        private System.Windows.Forms.TableLayoutPanel SubjectTable;
        private System.Windows.Forms.Label SenderAddress;
        private System.Windows.Forms.Label Subject;
        private System.Windows.Forms.Label SentTime;
        private System.Windows.Forms.Label ToAddresses;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.Label ErrorMessage;
        private System.Windows.Forms.PictureBox LockIcon;
        private System.Windows.Forms.Label CCAddresses;
        private System.Windows.Forms.Label BccAddresses;



        public partial class SecureReadFormFactory : Microsoft.Office.Tools.Outlook.IFormRegionFactory
        {
            public event Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler FormRegionInitializing;

            private Microsoft.Office.Tools.Outlook.FormRegionManifest _Manifest;

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public SecureReadFormFactory()
            {
                this._Manifest = Globals.Factory.CreateFormRegionManifest();
                SecureReadForm.InitializeManifest(this._Manifest, Globals.Factory);
                this.FormRegionInitializing += new Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler(this.SecureReadFormFactory_FormRegionInitializing);
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public Microsoft.Office.Tools.Outlook.FormRegionManifest Manifest
            {
                get
                {
                    return this._Manifest;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.IFormRegion Microsoft.Office.Tools.Outlook.IFormRegionFactory.CreateFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            {
                SecureReadForm form = new SecureReadForm(formRegion);
                form.Factory = this;
                return form;
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            byte[] Microsoft.Office.Tools.Outlook.IFormRegionFactory.GetFormRegionStorage(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                throw new System.NotSupportedException();
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            bool Microsoft.Office.Tools.Outlook.IFormRegionFactory.IsDisplayedForItem(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                if (this.FormRegionInitializing != null)
                {
                    Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs cancelArgs = Globals.Factory.CreateFormRegionInitializingEventArgs(outlookItem, formRegionMode, formRegionSize, false);
                    this.FormRegionInitializing(this, cancelArgs);
                    return !cancelArgs.Cancel;
                }
                else
                {
                    return true;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.FormRegionKindConstants Microsoft.Office.Tools.Outlook.IFormRegionFactory.Kind
            {
                get
                {
                    return Microsoft.Office.Tools.Outlook.FormRegionKindConstants.WindowsForms;
                }
            }
        }
    }

    partial class WindowFormRegionCollection
    {
        internal SecureReadForm SecureReadForm
        {
            get
            {
                foreach (var item in this)
                {
                    if (item.GetType() == typeof(SecureReadForm))
                        return (SecureReadForm)item;
                }
                return null;
            }
        }
    }
}
