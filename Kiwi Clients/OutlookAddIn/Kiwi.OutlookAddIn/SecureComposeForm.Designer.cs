﻿namespace Kiwi.OutlookAddIn
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class SecureComposeForm : Microsoft.Office.Tools.Outlook.FormRegionBase
    {
        public SecureComposeForm(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            : base(Globals.Factory, formRegion)
        {
            this.InitializeComponent();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TableLayoutPanel MainLayout;
            System.Windows.Forms.Label label1;
            this.ErrorMessage = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.Status = new System.Windows.Forms.Label();
            this.AddKeyServer = new System.Windows.Forms.Button();
            this.DeleteKeyServer = new System.Windows.Forms.Button();
            this.MakeDefault = new System.Windows.Forms.Button();
            this.KeyServerList = new System.Windows.Forms.ComboBox();
            MainLayout = new System.Windows.Forms.TableLayoutPanel();
            label1 = new System.Windows.Forms.Label();
            MainLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainLayout
            // 
            MainLayout.AutoSize = true;
            MainLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            MainLayout.ColumnCount = 6;
            MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            MainLayout.Controls.Add(this.ErrorMessage, 0, 2);
            MainLayout.Controls.Add(this.ProgressBar, 2, 1);
            MainLayout.Controls.Add(this.Status, 0, 1);
            MainLayout.Controls.Add(label1, 0, 0);
            MainLayout.Controls.Add(this.AddKeyServer, 2, 0);
            MainLayout.Controls.Add(this.DeleteKeyServer, 3, 0);
            MainLayout.Controls.Add(this.MakeDefault, 4, 0);
            MainLayout.Controls.Add(this.KeyServerList, 1, 0);
            MainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            MainLayout.Location = new System.Drawing.Point(0, 0);
            MainLayout.Name = "MainLayout";
            MainLayout.RowCount = 3;
            MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            MainLayout.Size = new System.Drawing.Size(500, 90);
            MainLayout.TabIndex = 0;
            // 
            // ErrorMessage
            // 
            this.ErrorMessage.AutoSize = true;
            MainLayout.SetColumnSpan(this.ErrorMessage, 6);
            this.ErrorMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ErrorMessage.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.ErrorMessage.Location = new System.Drawing.Point(3, 53);
            this.ErrorMessage.Name = "ErrorMessage";
            this.ErrorMessage.Size = new System.Drawing.Size(494, 37);
            this.ErrorMessage.TabIndex = 19;
            // 
            // ProgressBar
            // 
            MainLayout.SetColumnSpan(this.ProgressBar, 3);
            this.ProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProgressBar.Location = new System.Drawing.Point(279, 35);
            this.ProgressBar.Maximum = 3;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(144, 15);
            this.ProgressBar.Step = 1;
            this.ProgressBar.TabIndex = 18;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            MainLayout.SetColumnSpan(this.Status, 2);
            this.Status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Status.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Location = new System.Drawing.Point(3, 32);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(270, 21);
            this.Status.TabIndex = 17;
            this.Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Dock = System.Windows.Forms.DockStyle.Fill;
            label1.Location = new System.Drawing.Point(3, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(64, 32);
            label1.TabIndex = 0;
            label1.Text = "Key Server:";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AddKeyServer
            // 
            this.AddKeyServer.AutoSize = true;
            this.AddKeyServer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AddKeyServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddKeyServer.Image = global::Kiwi.OutlookAddIn.Properties.Resources.Add;
            this.AddKeyServer.Location = new System.Drawing.Point(279, 3);
            this.AddKeyServer.Name = "AddKeyServer";
            this.AddKeyServer.Size = new System.Drawing.Size(26, 26);
            this.AddKeyServer.TabIndex = 20;
            this.AddKeyServer.UseVisualStyleBackColor = true;
            this.AddKeyServer.Click += new System.EventHandler(this.AddKeyServer_Click);
            // 
            // DeleteKeyServer
            // 
            this.DeleteKeyServer.AutoSize = true;
            this.DeleteKeyServer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DeleteKeyServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeleteKeyServer.Image = global::Kiwi.OutlookAddIn.Properties.Resources.Delete;
            this.DeleteKeyServer.Location = new System.Drawing.Point(311, 3);
            this.DeleteKeyServer.Name = "DeleteKeyServer";
            this.DeleteKeyServer.Size = new System.Drawing.Size(26, 26);
            this.DeleteKeyServer.TabIndex = 21;
            this.DeleteKeyServer.UseVisualStyleBackColor = true;
            this.DeleteKeyServer.Click += new System.EventHandler(this.DeleteKeyServer_Click);
            // 
            // MakeDefault
            // 
            this.MakeDefault.AutoSize = true;
            this.MakeDefault.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MakeDefault.Dock = System.Windows.Forms.DockStyle.Left;
            this.MakeDefault.Location = new System.Drawing.Point(343, 3);
            this.MakeDefault.Name = "MakeDefault";
            this.MakeDefault.Size = new System.Drawing.Size(80, 26);
            this.MakeDefault.TabIndex = 22;
            this.MakeDefault.Text = "Make Default";
            this.MakeDefault.UseVisualStyleBackColor = true;
            this.MakeDefault.Click += new System.EventHandler(this.MakeDefault_Click);
            // 
            // KeyServerList
            // 
            this.KeyServerList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeyServerList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.KeyServerList.FormattingEnabled = true;
            this.KeyServerList.Location = new System.Drawing.Point(73, 3);
            this.KeyServerList.Name = "KeyServerList";
            this.KeyServerList.Size = new System.Drawing.Size(200, 21);
            this.KeyServerList.TabIndex = 23;
            this.KeyServerList.SelectedIndexChanged += new System.EventHandler(this.KeyServerList_SelectedIndexChanged);
            // 
            // SecureComposeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(MainLayout);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Name = "SecureComposeForm";
            this.Size = new System.Drawing.Size(500, 90);
            this.FormRegionShowing += new System.EventHandler(this.SecureComposeForm_FormRegionShowing);
            this.FormRegionClosed += new System.EventHandler(this.SecureComposeForm_FormRegionClosed);
            MainLayout.ResumeLayout(false);
            MainLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Form Region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private static void InitializeManifest(Microsoft.Office.Tools.Outlook.FormRegionManifest manifest, Microsoft.Office.Tools.Outlook.Factory factory)
        {
            manifest.FormRegionName = "Secure Compose Form";
            manifest.FormRegionType = Microsoft.Office.Tools.Outlook.FormRegionType.Adjoining;
            manifest.ShowInspectorRead = false;
            manifest.ShowReadingPane = false;

        }

        #endregion

        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.Label ErrorMessage;
        private System.Windows.Forms.Button AddKeyServer;
        private System.Windows.Forms.Button DeleteKeyServer;
        private System.Windows.Forms.Button MakeDefault;
        private System.Windows.Forms.ComboBox KeyServerList;


        public partial class SecureComposeFormFactory : Microsoft.Office.Tools.Outlook.IFormRegionFactory
        {
            public event Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler FormRegionInitializing;

            private Microsoft.Office.Tools.Outlook.FormRegionManifest _Manifest;

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public SecureComposeFormFactory()
            {
                this._Manifest = Globals.Factory.CreateFormRegionManifest();
                SecureComposeForm.InitializeManifest(this._Manifest, Globals.Factory);
                this.FormRegionInitializing += new Microsoft.Office.Tools.Outlook.FormRegionInitializingEventHandler(this.SecureComposeFormFactory_FormRegionInitializing);
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public Microsoft.Office.Tools.Outlook.FormRegionManifest Manifest
            {
                get
                {
                    return this._Manifest;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.IFormRegion Microsoft.Office.Tools.Outlook.IFormRegionFactory.CreateFormRegion(Microsoft.Office.Interop.Outlook.FormRegion formRegion)
            {
                SecureComposeForm form = new SecureComposeForm(formRegion);
                form.Factory = this;
                return form;
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            byte[] Microsoft.Office.Tools.Outlook.IFormRegionFactory.GetFormRegionStorage(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                throw new System.NotSupportedException();
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            bool Microsoft.Office.Tools.Outlook.IFormRegionFactory.IsDisplayedForItem(object outlookItem, Microsoft.Office.Interop.Outlook.OlFormRegionMode formRegionMode, Microsoft.Office.Interop.Outlook.OlFormRegionSize formRegionSize)
            {
                if (this.FormRegionInitializing != null)
                {
                    Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs cancelArgs = Globals.Factory.CreateFormRegionInitializingEventArgs(outlookItem, formRegionMode, formRegionSize, false);
                    this.FormRegionInitializing(this, cancelArgs);
                    return !cancelArgs.Cancel;
                }
                else
                {
                    return true;
                }
            }

            [System.Diagnostics.DebuggerNonUserCodeAttribute()]
            Microsoft.Office.Tools.Outlook.FormRegionKindConstants Microsoft.Office.Tools.Outlook.IFormRegionFactory.Kind
            {
                get
                {
                    return Microsoft.Office.Tools.Outlook.FormRegionKindConstants.WindowsForms;
                }
            }
        }
    }

    partial class WindowFormRegionCollection
    {
        internal SecureComposeForm SecureComposeForm
        {
            get
            {
                foreach (var item in this)
                {
                    if (item.GetType() == typeof(SecureComposeForm))
                        return (SecureComposeForm)item;
                }
                return null;
            }
        }
    }
}
