﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kiwi.OutlookAddIn
{
    internal partial class PasswordPrompt : Form
    {

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="authUri">Uri requesting a password.</param>
        public PasswordPrompt(Uri authUri)
        {
            InitializeComponent();

            this.requestUri.Text = authUri.Authority;        
        }


        public string Password { get { return password.Text; } }

    }
}
