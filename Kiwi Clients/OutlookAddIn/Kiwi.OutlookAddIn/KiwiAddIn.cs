﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using Kiwi.KeyManagement;
using Kiwi.LibKiwi;
using System.Threading.Tasks;
using System.Threading;

namespace Kiwi.OutlookAddIn
{

    /// <summary>
    /// The main Kiwi add in class.
    /// </summary>
    public partial class KiwiAddIn
    {

        #region Constants

        /// <summary>
        /// The class for Kiwi messages.
        /// </summary>
        internal const string KiwiMessageClass = "IPM.Note.Kiwi";

        /// <summary>
        /// The message class of drafts.
        /// </summary>
        internal const string DraftMessageClass = "IPM.Note.Kiwi.Draft";

        /// <summary>
        /// The Uri of the default key server.
        /// </summary>
        internal static readonly Uri DefaultKeyServerUri = new Uri("https://kiwi.byu.edu/ks/");

        #endregion

        #region Public properties

        /// <summary>
        /// Gets the key manager.
        /// </summary>
        public KeyManager KeyManager { get; private set; }

        #endregion

        #region Private properties

        /// <summary>
        /// The list of transaction Ids being watched for in saw emails.
        /// </summary>
        private HashSet<String> mTransactionIds = new HashSet<string>(); 

        #endregion

        #region Startup / Shutdown

        /// <summary>
        /// Fired when the addin starts up.
        /// </summary>
        private void KiwiAddIn_Startup(object sender, System.EventArgs e)
        {
            // Setup the key manager.
            KeyManager = new KeyManager();
            KeyManager.PreferredAuthenticationMethod = AuthenticationMethod.Saw;
            KeyManager.PasswordHandler = GetPasswordForAuthentication;
            KeyManager.SawTokenRequested += SawTokenRequested;
            Application.NewMailEx += HandleSawEmail;

            // Use the key manager to attempt and pre-fetch creator keys for the various accounts.
            // TODO: Make this happen for the other key servers as well.
            foreach (Outlook.Account account in Application.Session.Accounts)
            {
                if (!string.IsNullOrEmpty(account.SmtpAddress))
                    KeyManager.GetCreatorKeyAsync(new KeyRequestArgs(DefaultKeyServerUri, account.SmtpAddress, null, DateTime.Now, EmailPackager.PackageKeySize), (a, b, c) => { });

                break;
            }

            // Register operations that deal with messages that are to be read.
            Application.NewMailEx += MarkNewKiwiMessages;
            //Task.Factory.StartNew(ScanFoldersForKiwiMessages, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// Fired when the addin shuts down.
        /// </summary>
        private void KiwiAddIn_Shutdown(object sender, System.EventArgs e)
        {
            
        }


        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(KiwiAddIn_Startup);
            this.Shutdown += new System.EventHandler(KiwiAddIn_Shutdown);
        }

        #endregion

        #endregion

        #region Authentication Methods

        /// <summary>
        /// Gets the password for the given auth URI.
        /// </summary>
        /// <param name="uri">URI to get password for use in authentication.</param>
        /// <returns>Null if the operation was cancled, otherwise the password as typed.</returns>
        private string GetPasswordForAuthentication(Uri uri)
        {
            lock (this)
            {
                using (PasswordPrompt prompt = new PasswordPrompt(uri))
                {
                    System.Windows.Forms.NativeWindow nativeWindow = new System.Windows.Forms.NativeWindow();
                    nativeWindow.AssignHandle(System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle);
                    var result = prompt.ShowDialog(nativeWindow);
                    nativeWindow.ReleaseHandle();

                    if (result == System.Windows.Forms.DialogResult.OK)
                        return prompt.Password;
                    else
                        return null;
                }
            }
        }

        /// <summary>
        /// Fired when an e-mail is received.  Checks to see if it is a saw message.
        /// </summary>
        /// <param name="EntryIDCollection">Collection of ids of received emails.</param>
        private void HandleSawEmail(string EntryIDCollection)
        {
            foreach (string entryId in EntryIDCollection.Split(new char[] { ',' }))
            {
                Outlook.MailItem mailItem = Application.Session.GetItemFromID(entryId) as Outlook.MailItem;
                if (mailItem == null)
                    continue;
                
                var sawInfo = Saw.GetSawPartsFromSubject(mailItem.Subject);
                if (sawInfo != null)
                {
                    if (!mTransactionIds.Contains(sawInfo.Item1))
                        continue;

                    KeyManager.RegisterSawToken(sawInfo.Item1, sawInfo.Item2);
                    mailItem.Delete();
                    mTransactionIds.Remove(sawInfo.Item1);
                }
            }
        }

        /// <summary>
        /// Fired when a saw token has been requested.
        /// </summary>
        /// <param name="userId">The user id the token was requested for.</param>
        private void SawTokenRequested(string userId, string trascationId)
        {
            mTransactionIds.Add(trascationId);
            Application.Session.SendAndReceive(false);
        }

        #endregion

        #region Read item methods

        /// <summary>
        /// Mark new incoming messages as Kiwi messages.
        /// </summary>
        /// <param name="EntryIDCollection">Collection of ids of received emails.</param>
        private void MarkNewKiwiMessages(string EntryIDCollection)
        {
            foreach (string entryId in EntryIDCollection.Split(new char[] { ',' }))
            {
                try
                {
                    Outlook.MailItem mailItem = Application.Session.GetItemFromID(entryId) as Outlook.MailItem;
                    if (mailItem == null)
                        continue;

                    if (EmailPackageWrapper.IsWrappedSubject(mailItem.Subject))
                    {
                        mailItem.MessageClass = KiwiAddIn.KiwiMessageClass;
                        mailItem.Save();
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Scans folders for Kiwi messages and marks them as such.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void ScanFoldersForKiwiMessages()
        {
            // If the collections are changed while we are iterating we might throw and errors,
            // but I am not sure.
            try
            {
                // Add the root folders.
                Queue<Outlook.Folder> folders = new Queue<Outlook.Folder>();
                foreach (Outlook.Folder folder in Application.Session.Folders)
                    folders.Enqueue(folder);

                // Iterate over all folders and check their items for messages.  Also
                // add sub folders to the iteration.
                while (folders.Count != 0)
                {
                    Outlook.Folder folder = folders.Dequeue();

                    // Scan the items for matching mail items.
                    foreach (object outlookItem in folder.Items)
                    {
                        Outlook.MailItem mailItem = outlookItem as Outlook.MailItem;
                        if (mailItem == null)
                            continue;

                        if (!mailItem.MessageClass.StartsWith(KiwiMessageClass, StringComparison.Ordinal))
                        {
                            // Makes sure the package is actually wrapped.
                            EmailPackageWrapper wrapper;
                            if (EmailPackageWrapper.IsWrappedMessage(mailItem.Subject, mailItem.HTMLBody))
                            {
                                mailItem.MessageClass = KiwiAddIn.KiwiMessageClass;
                                mailItem.Save();
                            }
                        }
                    }

                    // Add the sub folders.
                    foreach (Outlook.Folder subFolder in folder.Folders)
                        folders.Enqueue(subFolder);
                }
            }
            catch { }
        }

        #endregion

    }
}
