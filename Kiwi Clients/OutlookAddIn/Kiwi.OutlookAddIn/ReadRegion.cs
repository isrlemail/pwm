﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;
using Tools = Microsoft.Office.Tools.Outlook;

namespace Kiwi.OutlookAddIn
{
    partial class ReadRegion
    {
        #region Form Region Factory

        [Microsoft.Office.Tools.Outlook.FormRegionMessageClass(Microsoft.Office.Tools.Outlook.FormRegionMessageClassAttribute.Note)]
        [Microsoft.Office.Tools.Outlook.FormRegionName("Kiwi.OutlookAddIn.ReadRegion")]
        public partial class ReadRegionFactory
        {
            // Occurs before the form region is initialized.
            // To prevent the form region from appearing, set e.Cancel to true.
            // Use e.OutlookItem to get a reference to the current Outlook item.
            private void ReadRegionFactory_FormRegionInitializing(object sender, Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs e)
            {
                Outlook.MailItem mailItem = (Outlook.MailItem)e.OutlookItem;

                // Makes sure the package is actually wrapped.
                EmailPackageWrapper wrapper;
                if (EmailPackageWrapper.IsWrappedMessage(mailItem.Subject, mailItem.HTMLBody))
                {
                    mailItem.MessageClass = KiwiAddIn.KiwiMessageClass;
                    mailItem.Save();
                }
            }
        }

        #endregion

        // Occurs before the form region is displayed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void ReadRegion_FormRegionShowing(object sender, System.EventArgs e)
        {
            
        }

        // Occurs when the form region is closed.
        // Use this.OutlookItem to get a reference to the current Outlook item.
        // Use this.OutlookFormRegion to get a reference to the form region.
        private void ReadRegion_FormRegionClosed(object sender, System.EventArgs e)
        {
        }
    }
}
