﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using Kiwi.LibKiwi;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Globalization;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using HtmlAgilityPack;

namespace Kiwi.OutlookAddIn
{

    /// <summary>
    /// Class that is responsible for wrapping packages
    /// for use in email.
    /// </summary>
    internal sealed class EmailPackageWrapper
    {

        #region Constants

        /// <summary>
        /// The message included with emails for boot strapping purposes.
        /// </summary>
        private const string BootStrapMessage =
@"
<div name='packageWrapper'>
<div style='display: block;'>
You have received an encrypted e-mail.  In order to decrypt this e-mail please go to 
<a href='https://kiwi.byu.edu/ws/'>https://kiwi.byu.edu/ws/</a> and download either 
a plugin or bookmarklet in order to read this message.
</div>

<br />
Encrypted Message:
<div name='package' style='border: solid 1px #808080; font-family: monospace; font-size:13px; padding: 5px; background-color: white; word-wrap:break-word;'>
@PACKAGE
</div>
</div>
";

        /// <summary>
        /// The wrapper for a pre-amble that user can optionally set to explain what this message is.
        /// </summary>
        private const string Preamble =
@"
<div style='display: block;'>
@PREAMBLE
</div>
<br />
";

        /// <summary>
        /// The header for a kiwi subject.
        /// </summary>
        private const string SubjectHeader = "[Kiwi] ";

        /// <summary>
        /// Serializer used to deal with packages.
        /// </summary>
        private static readonly DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Package), new Type[] { typeof(EncryptedData) });

        #endregion

        #region Fields

        /// <summary>
        /// The subject of the message.
        /// </summary>
        private string subject;

        /// <summary>
        /// The wrapped subject of the message.
        /// </summary>
        private string wrappedSubject;

        #endregion

        #region Regular Expressions

        /// <summary>
        /// A regular expression to parse subjects.
        /// </summary>
        private static readonly Regex subjectRegex = new Regex(string.Format(CultureInfo.InvariantCulture, @"(<?MessageType>((?<Reply>re:)|(?<Forward>fwd:))?\s*)?{0}(?<Subject>.*)", Regex.Escape(SubjectHeader)), RegexOptions.IgnoreCase);

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the subject of the message.
        /// </summary>
        public string Subject
        {
            get { return subject; }

            private set
            {
                Contract.Requires(value != null);
                Contract.Ensures(subject == value);
                Contract.Ensures(IsWrappedSubject(wrappedSubject));

                subject = value;

                // Only wrap unwrapped subject.
                Match match = subjectRegex.Match(subject);
                if (match.Success)
                    wrappedSubject = subject;
                else
                    wrappedSubject = string.Format(CultureInfo.CurrentUICulture, "{0}{1}", SubjectHeader, value);
            }
        }

        /// <summary>
        /// Gets or sets the wrapped subject of the message.
        /// </summary>
        public string WrappedSubject
        {
            get { return wrappedSubject; }

            private set
            {
                Contract.Requires(wrappedSubject != null);
                Contract.Requires(IsWrappedSubject(wrappedSubject));
                Contract.Ensures(wrappedSubject == value);
                Contract.Ensures(!IsWrappedSubject(subject));

                wrappedSubject = value;
                
                // Remove the kiwi part from the subject.
                Match match = subjectRegex.Match(wrappedSubject);
                subject = String.Format(CultureInfo.CurrentUICulture, "{0}{1}", match.Groups["MessageType"].Value, match.Groups["Subject"].Value);
            }
        }

        /// <summary>
        /// The package that is to be wrapped.
        /// </summary>
        public Package Package { get; private set; }

        /// <summary>
        /// The wrapped body of the message.
        /// </summary>
        public string WrappedBody { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a package wrapper from the unwrapped subject and body.
        /// </summary>
        /// <param name="subject">Subject of the message.</param>
        /// <param name="preamble">Preamble to be put before the boot strap message.</param>
        /// <param name="package">Package that is going to be wrapped.</param>
        public EmailPackageWrapper(string subject, string preamble, Package package)
        {
            Contract.Requires(subject != null);
            Contract.Requires(preamble != null);
            Contract.Requires(package != null);

            Subject = subject;
            Package = package;
            WrapPackage(preamble);
        }

        private EmailPackageWrapper(string wrappedSubject, string wrappedBody)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(subject));
            Contract.Requires(IsWrappedSubject(subject));
            Contract.Requires(wrappedBody != null);

            WrappedSubject = wrappedSubject;
            WrappedBody = wrappedBody;
            UnwrapPackage();
        }

        #endregion

        #region Body Methods

        /// <summary>
        /// Wraps the package so that it is ready to be sent in email.
        /// </summary>
        /// <param name="preamble">A pre-amble the user may wish to add to the start of a message
        /// explaining what it is.</param>
        private void WrapPackage(string preamble)
        {
            Contract.Requires(preamble != null);
            Contract.Requires(Package != null);
            Contract.Ensures(!string.IsNullOrEmpty(WrappedBody));

            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, Package);
                stream.Position = 0;

                StringBuilder message = new StringBuilder();
                if (!string.IsNullOrEmpty(preamble))
                    message.Append(preamble.Replace("@PREAMBLE", preamble));
                message.Append(BootStrapMessage.Replace("@PACKAGE", Convert.ToBase64String(stream.ToArray(), Base64FormattingOptions.InsertLineBreaks)));
                WrappedBody = message.ToString();
            }
        }

        /// <summary>
        /// Unwraps a package from a body that contains a package.
        /// </summary>
        private void UnwrapPackage()
        {
            Contract.Requires(WrappedBody != null);
            Contract.Ensures(Contract.Result<Package>() != null);

            HtmlDocument html = new HtmlDocument();
            html.LoadHtml(WrappedBody);
            var node = html.DocumentNode.SelectSingleNode("//div[@name='package']");
            
            // Get the bytes a deserialize them.
            byte[] packageBytes = Convert.FromBase64String(node.InnerText);
            using (MemoryStream stream = new MemoryStream(packageBytes))
            {
                Package = (Package)serializer.ReadObject(stream);
            }
        }

        #endregion

        #region Static methods

        /// <summary>
        /// Returns whether the given subject is wrapped or not.
        /// </summary>
        /// <param name="subject">The subject to test to see if it is wrapped.</param>
        /// <returns>Whehther the subject is already wrapped.</returns>
        [Pure]
        public static bool IsWrappedSubject(string subject)
        {
            Contract.Requires(subject != null);

            return subjectRegex.IsMatch(subject);
        }

        /// <summary>
        /// Checks wether the given body is wrapped or not.
        /// </summary>
        /// <param name="body">The body to check for a wrapped package.</param>
        /// <returns>Wether the body has a wrapped package element.</returns>
        [Pure]
        public static bool HasWrappedPackage(string body)
        {
            Contract.Requires(body != null);

            HtmlDocument html = new HtmlDocument();
            html.LoadHtml(body);
            var node = html.DocumentNode.SelectSingleNode("//div[@name='package']");
            return node != null;
        }

        /// <summary>
        /// Checks to see if the subject and body are both wrapped.
        /// </summary>
        /// <param name="subject">The subject of the message.</param>
        /// <param name="body">The body of the message.</param>
        /// <returns>Whether the message has a wrapped subject and body.</returns>
        public static bool IsWrappedMessage(string subject, string body)
        {
            Contract.Requires(subject != null);
            Contract.Requires(body != null);

            return IsWrappedSubject(subject) && HasWrappedPackage(body);
        }

        /// <summary>
        /// Tries and create a package wrapped from a wrapped subject and body.
        /// </summary>
        /// <param name="wrappedSubject">A wrapped kiwi subject.</param>
        /// <param name="wrappedBody">A wrapped body.</param>
        /// <param name="wrapper">The wrapper that is parsed from this information.</param>
        /// <returns>Whether a wrapper was successfuly created.</returns>
        public static bool TryCreateWrapper(string wrappedSubject, string wrappedBody, out EmailPackageWrapper wrapper)
        {
            Contract.Requires(wrappedSubject != null);
            Contract.Requires(wrappedBody != null);

            try
            {
                wrapper = new EmailPackageWrapper(wrappedSubject, wrappedBody);
                return true;
            }
            catch
            {
                wrapper = null;
                return false;
            }
        }

        #endregion

    }
}
