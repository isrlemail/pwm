﻿using System;
using System.Threading;
using Kiwi.KeyManagement;
using Kiwi.LibKiwi;
using Outlook = Microsoft.Office.Interop.Outlook;
using Tools = Microsoft.Office.Tools.Outlook;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;

namespace Kiwi.OutlookAddIn
{

    /// <summary>
    /// This form takes care of enabling the authoring of secure messages.
    /// </summary>
    partial class SecureComposeForm
    {

        #region Fields

        /// <summary>
        /// Backing for IsMessageSecured.
        /// </summary>
        private bool isMessageSecured;

        #endregion

        #region Properties

        /// <summary>
        /// The message this form is being displayed for.
        /// </summary>
        private Outlook.MailItem MailItem { get; set; }

        /// <summary>
        /// Gets or sets whether the message is secured.
        /// </summary>
        public bool IsMessageSecured
        {
            get
            {
                return isMessageSecured;
            }

            set
            {
                isMessageSecured = value;
                MailItem.MessageClass = isMessageSecured ? KiwiAddIn.DraftMessageClass : Tools.FormRegionMessageClassAttribute.Note;
                MailItem.Subject = MailItem.Subject;

                // Try to pre-fetch a key if possible.
                if (isMessageSecured && !string.IsNullOrEmpty(MailItem.SendUsingAccount.SmtpAddress))
                {
                    Uri keyServer = (Uri)KeyServerList.SelectedItem ?? KiwiAddIn.DefaultKeyServerUri;
                    Globals.KiwiAddIn.KeyManager.GetCreatorKeyAsync(
                        new KeyRequestArgs(keyServer, MailItem.SendUsingAccount.SmtpAddress, null, DateTime.Now, EmailPackager.PackageKeySize),
                        (a, b, c) => {});
                }

                if (IsMessageSecuredChanged != null)
                    IsMessageSecuredChanged(this, null);
            }
        }

        /// <summary>
        /// Tracks whether the message is currently being sent or not.
        /// </summary>
        private bool IsSending { get; set; }

        /// <summary>
        /// Tracks whether the form is closing or not.
        /// </summary>
        private bool IsClosing { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Event that is fired when the status of the message being secured is changed.
        /// </summary>
        public event EventHandler<EventArgs> IsMessageSecuredChanged;

        #endregion

        #region Form Region Factory

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
        [Tools.FormRegionMessageClass(Tools.FormRegionMessageClassAttribute.Note)]
        [Tools.FormRegionName("Kiwi.OutlookAddIn.SecureComposeForm")]
        public partial class SecureComposeFormFactory
        {
            // Occurs before the form region is initialized.
            // To prevent the form region from appearing, set e.Cancel to true.
            // Use e.OutlookItem to get a reference to the current Outlook item.
            private void SecureComposeFormFactory_FormRegionInitializing(object sender, Tools.FormRegionInitializingEventArgs e)
            {
            }
        }

        #endregion

        #region Form Display Events

        /// <summary>
        /// Fires before the form region is displayed.
        /// </summary>
        /// <remarks>
        /// Use this.OutlookItem to get a reference to the current Outlook item.
        /// Use this.OutlookFormRegion to get a reference to the form region.
        /// </remarks>
        private void SecureComposeForm_FormRegionShowing(object sender, System.EventArgs e)
        {
            // Register e-mail item.
            MailItem = (Outlook.MailItem)OutlookItem;
            ((Outlook.ItemEvents_10_Event)MailItem).Close += MailItem_Close;
            MailItem.Write += MailItem_Write;
            ((Outlook.ItemEvents_10_Event)MailItem).Send += MailItem_Send;
            MailItem.AfterWrite += MailItem_AfterWrite;

            // Setup key servers.
            var keyServers = Globals.KiwiAddIn.KeyManager.GetKeyServers();
            if (!keyServers.Any())
            {
                Globals.KiwiAddIn.KeyManager.AddKeyServer(KiwiAddIn.DefaultKeyServerUri);
                Globals.KiwiAddIn.KeyManager.SetDefaultKeyServer(KiwiAddIn.DefaultKeyServerUri);
            }
            KeyServerList.DataSource = keyServers.ToList();
            KeyServerList.SelectedItem = Globals.KiwiAddIn.KeyManager.GetDefaultKeyServer();

            // If the current message is already encrypted we need to decrypt it first.
            EmailPackageWrapper wrapper;
            if (EmailPackageWrapper.TryCreateWrapper(MailItem.Subject, MailItem.HTMLBody, out wrapper))
            {

                try
                {
                    EmailPackager packager = new EmailPackager(wrapper.Package);
                    using (Semaphore sempahore = new Semaphore(0, 1))
                    {
                        Globals.KiwiAddIn.KeyManager.GetViewerKeyAsync(new KeyRequestArgs(packager.KeyServerUri, packager.CreatorId, MailItem.SendUsingAccount.SmtpAddress, packager.Timestamp, EmailPackager.PackageKeySize),
                            (success, value, args) =>
                            {
                                packager.Unwrap(wrapper.Package, args.ViewerId, value);
                                sempahore.Release();
                            });
                        sempahore.WaitOne();
                    }

                    IsMessageSecured = true;
                    MailItem.HTMLBody = (string)packager.Message.Contents;
                    MailItem.Subject = wrapper.Subject;
                }
                catch
                {
                    ReportError("Unable to unwrap package.");
                    return;
                }
                

            }
        }

        /// <summary>
        /// Fires when the form region is closed.
        /// </summary>
        /// <remarks>
        /// Use this.OutlookItem to get a reference to the current Outlook item.
        /// Use this.OutlookFormRegion to get a reference to the form region.
        /// </remarks>
        private void SecureComposeForm_FormRegionClosed(object sender, System.EventArgs e)
        {
        }

        #endregion

        #region Mail Item Events

        /// <summary>
        /// Fired when the mail item is being sent.
        /// </summary>
        /// <param name="Cancel">Whether or not to cancel the default close operation.</param>
        void MailItem_Send(ref bool Cancel)
        {
            IsSending = true;
        }

        /// <summary>
        /// Fired when the dialog is being closed.
        /// </summary>
        /// <param name="Cancel">Whether or not to cancel the default close operation.</param>
        void MailItem_Close(ref bool Cancel)
        {
            // In this case the dialog is closing and we will already deal with it.
            if (IsSending)
                return;

            IsClosing = true;
        }

        /// <summary>
        /// Fired when the mail item is written.  Used so we can write a secure mail item
        /// instead of what the user is typing.
        /// </summary>
        /// <param name="Cancel">Whether or not to cancel the default close operation.</param>
        private void MailItem_Write(ref bool Cancel)
        {
            if (!IsMessageSecured)
                return;

            // RTF messages are not supported.
            if (MailItem.BodyFormat == Outlook.OlBodyFormat.olFormatRichText)
            {
                ReportError("Cannot use RTF messages.");
                Cancel = true;
                return;
            }

            // Get the basic information needed for encryption.
            UpdateStatus("Writing message", 0);
            Uri keyServer = (Uri)KeyServerList.SelectedItem;
            DateTime timestamp = DateTime.Now;
            string creatorId = MailItem.SendUsingAccount.SmtpAddress;

            if (string.IsNullOrEmpty(creatorId))
            {
                ReportError("Not a valid account to send secure email with.");
                Cancel = true;
                return;
            }

            // Get the creator key.
            bool keyRetrieved = false;
            byte[] creatorKey = null;

            UpdateStatus("Retrieving encryption key");
            using (Semaphore semaphore = new Semaphore(0, 1))
            {
                Globals.KiwiAddIn.KeyManager.GetCreatorKeyAsync(
                    new KeyRequestArgs(keyServer, creatorId, null, timestamp, EmailPackager.PackageKeySize),
                    (success, value, args) =>
                    {
                        keyRetrieved = success;
                        creatorKey = value;
                        semaphore.Release();
                    });

                semaphore.WaitOne();
            }

            if (!keyRetrieved)
            {
                ReportError("Unable to get a key to encrypt the email.");
                Cancel = true;
                return;
            }

            // Wrap the package up.
            UpdateStatus("Encrypting message");
            string subject = MailItem.Subject;
            string messageBody = MailItem.HTMLBody;
            bool isHtmlMessage = MailItem.BodyFormat == Outlook.OlBodyFormat.olFormatHTML;

            EmailPackager packager = new EmailPackager()
            {
                CreatorId = creatorId,
                KeyServerUri = keyServer,
                Message = new Message(messageBody),
                Timestamp = timestamp,
                CreatorKey = creatorKey,
            };

            // Add the recipients.
            foreach (Outlook.Recipient recipient in MailItem.Recipients)
            {
                try
                {
                    if (!string.IsNullOrEmpty(recipient.AddressEntry.Address))
                        packager.ViewerIds.Add(recipient.AddressEntry.Address);
                }
                catch { }
            }


            // Change the message contents.
            Package package = packager.Wrap();
            EmailPackageWrapper wrapper = new EmailPackageWrapper(subject, string.Empty, package);
            MailItem.Subject = wrapper.WrappedSubject;
            MailItem.HTMLBody = wrapper.WrappedBody;
            UpdateStatus("Message secured");

            // If not closing or sending we need to save and restore the message to its old state so it can still be read.
            if (!IsSending && !IsClosing)
            {
                Cancel = true;
                MailItem.Save();

                MailItem.Subject = subject;
                MailItem.HTMLBody = (string)packager.Message.Contents;
            }

            // If the message is being sent then we need to change back the message class,
            // otherwise outlook will add an attachment to the message.
            if (IsSending)
            {
                Cancel = true;
                MailItem.MessageClass = Tools.FormRegionMessageClassAttribute.Note;
                MailItem.Save();
                MailItem.Send();
            }
        }

        /// <summary>
        /// Fired after the item is written.
        /// </summary>
        private void MailItem_AfterWrite()
        {
            IsSending = false;
            IsClosing = false;
        }

        #endregion

        #region GUI update methods

        /// <summary>
        /// Updates the status message and progress bar.
        /// </summary>
        /// <param name="status">Status to update to.</param>
        /// <param name="value">The value for the progress bar.</param>
        private void UpdateStatus(string status, int value = -1)
        {
            Status.Text = status;
            if (value == -1)
                ProgressBar.PerformStep();
            else
                ProgressBar.Value = value;        
        }

        /// <summary>
        /// Called to report an error to the user.
        /// </summary>
        /// <param name="error">The error to report to the user.</param>
        private void ReportError(string error)
        {
            Status.Text = "Error";
            ProgressBar.Value = ProgressBar.Maximum;
            ErrorMessage.Text = error;
        }

        #endregion

        #region GUI events

        /// <summary>
        /// Fired when a new key server has been selected.
        /// </summary>
        private void KeyServerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Try to pre-fetch a key if possible.
            if (isMessageSecured && !string.IsNullOrEmpty(MailItem.SendUsingAccount.SmtpAddress) && KeyServerList.SelectedItem != null)
            {
                Globals.KiwiAddIn.KeyManager.GetCreatorKeyAsync(
                    new KeyRequestArgs((Uri)KeyServerList.SelectedItem, MailItem.SendUsingAccount.SmtpAddress, null, DateTime.Now, EmailPackager.PackageKeySize),
                    (a, b, c) => { });
            }
        }

        /// <summary>
        /// Adds a key server.
        /// </summary>
        private void AddKeyServer_Click(object sender, EventArgs e)
        {
            // Get a Uri and add it.
            using (AddKeyServerPrompt prompt = new AddKeyServerPrompt())
            {
                var result = prompt.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Uri keyServerUri;
                    bool correctUri = Uri.TryCreate(prompt.KeyServer, UriKind.Absolute, out keyServerUri);

                    if (!correctUri)
                    {
                        System.Windows.Forms.MessageBox.Show("Not a valid URI.", "Invalid operation", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        return;
                    }

                    Globals.KiwiAddIn.KeyManager.AddKeyServer(keyServerUri);
                    KeyServerList.DataSource = Globals.KiwiAddIn.KeyManager.GetKeyServers().ToList();
                    KeyServerList.SelectedItem = keyServerUri;
                }
            }
        }

        /// <summary>
        /// Deletes the selected key server.
        /// </summary>
        private void DeleteKeyServer_Click(object sender, EventArgs e)
        {
            if (KeyServerList.Items.Count == 1)
            {
                System.Windows.Forms.MessageBox.Show("Can't delete last key server.", "Invalid operation", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }

            Globals.KiwiAddIn.KeyManager.DeleteKeyServer((Uri)KeyServerList.SelectedItem);
            KeyServerList.DataSource = Globals.KiwiAddIn.KeyManager.GetKeyServers().ToList();
        }
        
        /// <summary>
        /// Makes the selected server the default key server.
        /// </summary>
        private void MakeDefault_Click(object sender, EventArgs e)
        {
            Globals.KiwiAddIn.KeyManager.SetDefaultKeyServer((Uri)KeyServerList.SelectedItem);
        }

        #endregion

    }
}
