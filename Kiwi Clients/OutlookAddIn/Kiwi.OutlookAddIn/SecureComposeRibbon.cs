﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Kiwi.OutlookAddIn
{
    public partial class SecureComposeRibbon
    {

        #region Properties

        /// <summary>
        /// Reference to the secure compose form.
        /// </summary>
        private SecureComposeForm SecureComposeForm { get; set; }

        #endregion

        #region Events

        /// <summary>
        /// Fired when the ribbon is loaded.
        /// </summary>
        private void SecureComposeRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            // Set the intial state of the toggle button.
            SecureComposeForm = Globals.FormRegions[(Outlook.Inspector)Context].SecureComposeForm;
            
            ToggleButtonSecureMessage.Checked = SecureComposeForm.IsMessageSecured;
            SecureComposeForm.IsMessageSecuredChanged += new EventHandler<EventArgs>(SecureComposeForm_IsMessageSecuredChanged);
        }
        
        /// <summary>
        /// Fired when the form changes the secured status of the item.
        /// </summary>
        void SecureComposeForm_IsMessageSecuredChanged(object sender, EventArgs e)
        {
            ToggleButtonSecureMessage.Checked = SecureComposeForm.IsMessageSecured;
        }

        /// <summary>
        /// Fired when the secured button is clicked.
        /// </summary>
        private void ToggleButtonSecureMessage_Click(object sender, RibbonControlEventArgs e)
        {
            SecureComposeForm.IsMessageSecured = ToggleButtonSecureMessage.Checked;
        }

        #endregion

    }
}
