﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kiwi.OutlookAddIn
{
    internal partial class AddKeyServerPrompt : Form
    {

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AddKeyServerPrompt()
        {
            InitializeComponent();      
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the string typed into the form.
        /// </summary>
        public string KeyServer { get { return KeyServerUri.Text; } }

        #endregion

    }
}
