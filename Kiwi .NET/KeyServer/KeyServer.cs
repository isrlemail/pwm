﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Security;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Kiwi.KeyServer.Properties;
using Kiwi.LibKiwi;
using System.Diagnostics.Contracts;
using Facebook;

namespace Kiwi.KeyServer
{

	/// <summary>
	/// Handles the behavoir of the key server.
	/// </summary>
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
	public sealed class KeyServer
	{

		#region Fields

		/// <summary>
		///  The dictionary that connects auther servers and their corresponding keys.
		/// </summary>
		private Dictionary<Uri, byte[]> authServerSharedKeys;

		/// <summary>
		///  The dictionary that connects auther servers and their corresponding token size.
		/// </summary>
		private Dictionary<Uri, int> authServerTokenSizes;

		/// <summary>
		/// The key server's key.  Used by the Hkdf to derive the creator key.
		/// </summary>
		private byte[] keyServerKey = Convert.FromBase64String(Settings.Default.KeyServerKey);

		#endregion

		#region Constructor

		/// <summary>
		/// Creates the key server service.
		/// </summary>
		private KeyServer()
		{
			// The valid auth servers are stored as a JSON array of servers and their shared keys
			// base 64 encoded.  Here we deserialize that list and then convert the dictionary
			// so it is the unencoded bytes of the base 64 encoded key.
			DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Dictionary<Uri, string>));
			using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(Settings.Default.AuthServerSharedKeys)))
			{
				authServerSharedKeys = ((Dictionary<Uri, string>)serializer.ReadObject(stream)).
						ToDictionary(item => item.Key, item => Convert.FromBase64String(item.Value));
			}

			using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(Settings.Default.AuthServerTokenSizes)))
			{
				serializer = new DataContractJsonSerializer(typeof(Dictionary<Uri, int>));
				authServerTokenSizes = (Dictionary<Uri, int>)serializer.ReadObject(stream);
			}
		}

		#endregion

		#region Web Methods

		/// <summary>
		/// Gets the list of auth servers this key server accepts tokens from.
		/// </summary>
		/// <returns>List of auth servers this key server accepts tokens from.</returns>
		[WebGet(UriTemplate = "AuthServers", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
		public IEnumerable<Uri> GetAuthServers()
		{
			return authServerSharedKeys.Keys;
		}


		/// <summary>
		/// Gets the key for the given key request.
		/// </summary>
		/// <param name="request">Request for the key.</param>
		/// <returns>Base64 encoded key requested.</returns>
		[WebInvoke(UriTemplate = "Key", Method = "POST",
						RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare)]
		public Tuple<string, PeriodBase> GetKey(KeyRequest request)
        {
            CheckIsValidToken(request);
            
			// Generate the requested key.
			PeriodBase period = MonthlyPeriod.GetPeriodForDateTime(request.Timestamp);
			byte[] creatorKey = Hkdf.DeriveCreatorKey(keyServerKey, request.CreatorId, period, request.KeySize);
			switch (request.KeyType)
			{
				case KeyType.CreatorKey:
					return new Tuple<string, PeriodBase>(Convert.ToBase64String(creatorKey), period);

				case KeyType.ViewerKey:
					return new Tuple<string, PeriodBase>(Convert.ToBase64String(Hkdf.DeriveViewerKey(creatorKey, request.ViewerId, request.KeySize)), period);
			}

			return null;
		}

		/// <summary>
		/// Checks the auth token for validity
		/// </summary>
		/// <param name="request"></param>
		private void CheckIsValidToken(KeyRequest request)
		{
			// Verify the token.
            // Not working with new year. Remove fix later.
            //if (!request.AuthToken.Period.IsCurrent())
            //    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                //throw new WebFaultException<SecurityException>(new SecurityException("Out of date auth token."), System.Net.HttpStatusCode.BadRequest);
            
			// Validate the Facebook user
			if (request.AuthToken.Method == AuthenticationMethod.Other)
			{
				try
				{
					FacebookClient client = new FacebookClient(Encoding.UTF8.GetString(request.AuthToken.Value));
					dynamic me = client.Get("me");
					if (!request.AuthToken.UserId.Equals(me.id, StringComparison.OrdinalIgnoreCase))
                        throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
						//throw new WebFaultException<SecurityException>(new SecurityException("Facebook access token invalid."), System.Net.HttpStatusCode.BadRequest);
				}
				catch (Exception)
				{
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                    //throw new WebFaultException<SecurityException>(new SecurityException("Facebook access token invalid."), System.Net.HttpStatusCode.BadRequest);
				}
			}
			else // Validate everything else
			{
				if (!authServerSharedKeys.ContainsKey(request.AuthToken.AuthServerUri))
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                    //throw new WebFaultException<SecurityException>(new SecurityException("Auth token is not from a recognized auth server."), System.Net.HttpStatusCode.BadRequest);

                /** Not working with the new year, remove fix later.
                if (!request.AuthToken.Value.SequenceEqual(Hkdf.DeriveAuthenticationToken(
                                            authServerSharedKeys[request.AuthToken.AuthServerUri], request.AuthToken.UserId,
                                            request.AuthToken.Period, authServerTokenSizes[request.AuthToken.AuthServerUri])))*/
                if (!request.AuthToken.Value.SequenceEqual(Hkdf.DeriveAuthenticationToken(
                                        authServerSharedKeys[request.AuthToken.AuthServerUri], request.AuthToken.UserId,
                                        MonthlyPeriod.Now, authServerTokenSizes[request.AuthToken.AuthServerUri])))
                    throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                    //throw new WebFaultException<SecurityException>(new SecurityException("Invalid auth token."), System.Net.HttpStatusCode.BadRequest);
			}

			switch (request.KeyType)
			{
				case KeyType.CreatorKey:
					if (!request.AuthToken.UserId.Equals(request.CreatorId, StringComparison.OrdinalIgnoreCase))
                        throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                        //throw new WebFaultException<SecurityException>(new SecurityException("Auth token for incorrect user."), System.Net.HttpStatusCode.BadRequest);
					break;

				case KeyType.ViewerKey:
					if (!request.AuthToken.UserId.Equals(request.ViewerId, StringComparison.OrdinalIgnoreCase))
                        throw new WebFaultException(System.Net.HttpStatusCode.BadRequest);
                        //throw new WebFaultException<SecurityException>(new SecurityException("Auth token for invalid user."), System.Net.HttpStatusCode.BadRequest);
					break;
			}
		}

		/// <summary>
		/// Verifies that the claimed creator did create this message.
		/// </summary>
		/// <param name="request">Verification request object wrapper.</param>
		/// <returns>Whether the creator created this e-mail message.</returns>
		[WebInvoke(UriTemplate = "VerifyEmailCreator", Method = "POST",
						RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare)]
		public bool VerifyEmailCreator(Package package)
		{
			EmailPackager packager = new EmailPackager(package);
			packager.CreatorKey = Hkdf.DeriveCreatorKey(keyServerKey, packager.CreatorId, MonthlyPeriod.GetPeriodForDateTime(packager.Timestamp), packager.KeySize);
			byte[] viewerKey = Hkdf.DeriveViewerKey(packager.CreatorKey, packager.CreatorId, packager.KeySize);

			try
			{
				packager.Unwrap(package, packager.CreatorId, viewerKey);
				return true;
			}
			catch
			{
				return false;
			}
		}

		#endregion

	}
}
