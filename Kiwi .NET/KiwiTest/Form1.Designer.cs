﻿namespace KiwiTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sawRegisterToken = new System.Windows.Forms.Button();
            this.sawSubject = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sawRegisterToken);
            this.groupBox1.Controls.Add(this.sawSubject);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 96);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SAW";
            // 
            // sawRegisterToken
            // 
            this.sawRegisterToken.Location = new System.Drawing.Point(9, 58);
            this.sawRegisterToken.Name = "sawRegisterToken";
            this.sawRegisterToken.Size = new System.Drawing.Size(107, 23);
            this.sawRegisterToken.TabIndex = 4;
            this.sawRegisterToken.Text = "Register Token";
            this.sawRegisterToken.UseVisualStyleBackColor = true;
            this.sawRegisterToken.Click += new System.EventHandler(this.sawRegisterToken_Click);
            // 
            // sawSubject
            // 
            this.sawSubject.Location = new System.Drawing.Point(9, 32);
            this.sawSubject.Name = "sawSubject";
            this.sawSubject.Size = new System.Drawing.Size(311, 20);
            this.sawSubject.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Subject:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 120);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button sawRegisterToken;
        private System.Windows.Forms.TextBox sawSubject;

    }
}

