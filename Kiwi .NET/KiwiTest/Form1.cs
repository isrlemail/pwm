﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Kiwi.KeyManagement;
using Kiwi.LibKiwi;
using System.IO;
using System.Runtime.Serialization.Json;

namespace KiwiTest
{
    public partial class Form1 : Form
    {

        private KeyManager manager;

        public Form1()
        {
            InitializeComponent();

            MonthlyPeriod month = MonthlyPeriod.Now;
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(PeriodBase));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, month);
            stream.Flush();


            manager = new KeyManager();
            manager.PreferredAuthenticationMethod = AuthenticationMethod.Saw;

            manager.PasswordHandler = (Uri uri) =>
            {
                PasswordPrompt prompt = new PasswordPrompt(uri);
                var result = prompt.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                    return prompt.Password;
                else
                    return null;
            };

            CreateMessage(manager);

        }

        private static void CreateMessage(KeyManager keyManager)
        {
            Uri keyServerUri = new Uri(@"https://pwm.byu.edu/ks/");
            string userId = "mathflair@gmail.com";

            EmailPackager packager = new EmailPackager()
            {
                CreatorId = userId,
                Timestamp = DateTime.Now,
                KeyServerUri = keyServerUri,
                Message = new Kiwi.LibKiwi.Message("How are you doing today?")
            };
            packager.ViewerIds.Add("scott.ruoti@gmail.com");

            // Get the creator key, and then wrap the package.
            keyManager.GetCreatorKeyAsync(new KeyRequestArgs(keyServerUri, "mathflair@gmail.com", null, DateTime.Now, EmailPackager.PackageKeySize),
                (success, value, args) =>
                {
                    packager.CreatorKey = value;
                    Package package = packager.Wrap();
                    MemoryStream output = new MemoryStream();
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Package), new Type[] { typeof(EncryptedData) });
                    serializer.WriteObject(output, package);
                    output.Flush();
                    output.Seek(0, SeekOrigin.Begin);
                    StreamReader reader = new StreamReader(output);
                    string str = reader.ReadToEnd();

                    ReadMessage(output, serializer, keyManager);
                });
        }

        private static void ReadMessage(MemoryStream output, DataContractJsonSerializer serializer, KeyManager keyManager)
        {
            //// Read the package.
            output.Position = 0;
            Package package = (Package)serializer.ReadObject(output);
            EmailPackager packager = new EmailPackager(package);

            keyManager.GetViewerKeyAsync(new KeyRequestArgs(packager.KeyServerUri, packager.CreatorId, "scott.ruoti@gmail.com", packager.Timestamp, EmailPackager.PackageKeySize),
                (success, value, args) =>
                {
                    packager.Unwrap(package, "scott.ruoti@gmail.com", value);

                    object message = packager.Message.Contents;

                });


        }

        private void sawRegisterToken_Click(object sender, EventArgs e)
        {
            var sawInfo = Saw.GetSawPartsFromSubject(sawSubject.Text);
            if(sawInfo != null)
                manager.RegisterSawToken(sawInfo.Item1, sawInfo.Item2);
        }

    }
}
