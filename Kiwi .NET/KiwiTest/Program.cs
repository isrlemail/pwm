﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using Kiwi.LibKiwi;

namespace KiwiTest
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);


            var serializer = new DataContractJsonSerializer(typeof(KeyRequest));
            MemoryStream stream = new MemoryStream(File.ReadAllBytes(@"C:\temp.txt"));
            KeyRequest request = (KeyRequest)serializer.ReadObject(stream);


			//Application.Run(new Form1());
			//Application.Run(new HKDFTest());
		}
	}
}
