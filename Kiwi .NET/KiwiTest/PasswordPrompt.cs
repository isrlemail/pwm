﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KiwiTest
{
    public partial class PasswordPrompt : Form
    {
        public PasswordPrompt(Uri authUri)
        {
            InitializeComponent();

            this.requestUri.Text = authUri.Authority;        
        }


        public string Password { get { return password.Text; } }

    }
}
