﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Kiwi.LibKiwi;

namespace KiwiTest
{
	public partial class HKDFTest : Form
	{
		public HKDFTest()
		{
			InitializeComponent();
		}

		private void btnGenerate_Click(object sender, EventArgs e)
		{
			byte[] passwordKey = Hkdf.DerivePasswordKey(txtUsername.Text, txtPassword.Text, 32);
			txtOutput.Text = String.Join(" ", passwordKey);
		}
	}
}
