﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Diagnostics.Contracts;
using Kiwi.LibKiwi;

namespace Kiwi.KeyManagement
{
    
    /// <summary>
    /// Static class for making requests to the various servers.
    /// </summary>
    [Pure]
    internal static class KiwiServerOperations
    {

        #region Auth Server

        /// <summary>
        /// Gets the list of methods and their respective uri supported by the auth server.
        /// </summary>
        /// <param name="authServerUri">Uri of the auth server.</param>
        /// <returns>List of methods and their respective uri supported by the auth server.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static IEnumerable<Tuple<AuthenticationMethod, Uri>> GetAuthMethodsAndUris(Uri authServerUri)
        {
            Contract.Requires(authServerUri != null);
            Contract.Ensures(Contract.Result<IEnumerable<Uri>>() != null);

            using (HttpClient client = new HttpClient(authServerUri))
            {
                using (HttpResponseMessage response = client.Get("Info/MethodsAndUris"))
                {
                    response.EnsureSuccessStatusCode();

                    return response.Content.ReadDataContractJsonContent<IEnumerable<Tuple<AuthenticationMethod, Uri>>>();
                }
            }
        }

        /// <summary>
        /// Sends an auth token request.
        /// </summary>
        /// <param name="authServerUri">Uri of the server.</param>
        /// <param name="userId">Id of the user.</param>
        /// <returns>Auth token retreived from the server.</returns>
        public static AuthToken GetAuthToken(Uri authServerUri, string userId)
        {
            Contract.Requires(authServerUri != null);
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Ensures(Contract.Result<AuthToken>() != null);

            using (HttpClient client = new HttpClient(authServerUri))
            {
                using (HttpResponseMessage response = client.Post("", userId.GetDataContractJsonContent()))
                {
                    response.EnsureSuccessStatusCode();

                    return response.Content.ReadDataContractJsonContent<AuthToken>();
                }
            }
        }

        #endregion

        #region Key Server

        /// <summary>
        /// Gets the list of auth servers used by the key server.
        /// </summary>
        /// <param name="keyServerUri">Uri of the key server.</param>
        /// <returns>List of the auth servers used by the key server.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static IEnumerable<Uri> GetAuthServers(Uri keyServerUri)
        {
            Contract.Requires(keyServerUri != null);
            Contract.Ensures(Contract.Result<IEnumerable<Uri>>() != null);

            using (HttpClient client = new HttpClient(keyServerUri))
            {
                using (HttpResponseMessage response = client.Get("AuthServers"))
                {
                    response.EnsureSuccessStatusCode();

                    return response.Content.ReadDataContractJsonContent<IEnumerable<Uri>>();
                }
            }
        }

        /// <summary>
        /// Gets the creator key from the key server.
        /// </summary>
        /// <param name="keyServerUri">Uri of the key server.</param>
        /// <param name="authToken">Auth token giving permission to use the key server.</param>
        /// <param name="creatorId">Id of the creator.</param>
        /// <param name="timestamp">DateTime for the key.</param>
        /// <param name="keySize">Size of the key.</param>
        /// <returns>The key requested from the server.</returns>
        public static Tuple<byte[], PeriodBase> GetCreatorKey(Uri keyServerUri, AuthToken authToken, string creatorId, DateTime timestamp, int keySize)
        {
            Contract.Requires(keyServerUri != null);
            Contract.Requires(authToken != null);
            Contract.Requires(!string.IsNullOrEmpty(creatorId));
            Contract.Requires(timestamp != null);
            Contract.Requires(keySize > 0);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>() != null);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>().Item1 != null);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>().Item1.Length == keySize);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>().Item2 != null);

            KeyRequest request = new KeyRequest(authToken, creatorId, null, timestamp, keySize, KeyType.CreatorKey);

            using (HttpClient client = new HttpClient(keyServerUri))
            {
                using (HttpResponseMessage response = client.Post("Key", request.GetDataContractJsonContent()))
                {
                    response.EnsureSuccessStatusCode();

                    var data = response.Content.ReadDataContractJsonContent<Tuple<string, PeriodBase>>();
                    return new Tuple<byte[], PeriodBase>(Convert.FromBase64String(data.Item1), data.Item2);
                }
            }
        }

        /// <summary>
        /// Gets the viewer key from the key server.
        /// </summary>
        /// <param name="keyServerUri">Uri of the key server.</param>
        /// <param name="authToken">Auth token giving permission to use the key server.</param>
        /// <param name="creatorId">Id of the creator.</param>
        /// <param name="viewerId">Id of the viewer.</param>
        /// <param name="timestamp">DateTime for the key.</param>
        /// <param name="keySize">Size of the key.</param>
        /// <returns>The key requested from the server.</returns>
        public static Tuple<byte[], PeriodBase> GetViewerKey(Uri keyServerUri, AuthToken authToken, string creatorId, string viewerId, DateTime timestamp, int keySize)
        {
            Contract.Requires(keyServerUri != null);
            Contract.Requires(authToken != null);
            Contract.Requires(!string.IsNullOrEmpty(creatorId));
            Contract.Requires(!string.IsNullOrEmpty(viewerId));
            Contract.Requires(timestamp != null);
            Contract.Requires(keySize > 0);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>() != null);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>().Item1 != null);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>().Item1.Length == keySize);
            Contract.Ensures(Contract.Result<Tuple<byte[], PeriodBase>>().Item2 != null);

            KeyRequest request = new KeyRequest(authToken, creatorId, viewerId, timestamp, keySize, KeyType.ViewerKey);

            using (HttpClient client = new HttpClient(keyServerUri))
            {
                using (HttpResponseMessage response = client.Post("Key", request.GetDataContractJsonContent()))
                {
                    response.EnsureSuccessStatusCode();

                    var data = response.Content.ReadDataContractJsonContent<Tuple<string, PeriodBase>>();
                    return new Tuple<byte[], PeriodBase>(Convert.FromBase64String(data.Item1), data.Item2);
                }
            }
        }

        /// <summary>
        /// Checks if the given package produced by an email packager
        /// is really from the stated creator.
        /// </summary>
        /// <param name="keyServerUri">Uri of the key server.</param>
        /// <param name="package">Package to check.</param>
        /// <returns>Whether the package really did come from the creator Id
        /// in the package.</returns>
        public static bool IsEmailPackageVerified(Uri keyServerUri, Package package)
        {
            Contract.Requires(keyServerUri != null);
            Contract.Requires(package != null);

            using (HttpClient client = new HttpClient(keyServerUri))
            {
                using (HttpResponseMessage response = client.Post("VerifyEmailCreator", package.GetDataContractJsonContent()))
                {
                    response.EnsureSuccessStatusCode();

                    return response.Content.ReadDataContractJsonContent<bool>();
                }
            }
        }

        #endregion

    }

}
