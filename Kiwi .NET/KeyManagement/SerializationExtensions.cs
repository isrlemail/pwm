﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Kiwi.KeyManagement
{

    /// <summary>
    /// Class that adds extension methods relative to HttpClient.
    /// </summary>
    [Pure]
    internal static class SerializationExtensions
    {

        #region HttpContent related serialization

        /// <summary>
        /// Gets content that is the serialized object.
        /// </summary>
        /// <typeparam name="T">Type of the object to serialize.</typeparam>
        /// <param name="item">Item to serialize.</param>
        /// <param name="serializer">Serializer to use.</param>
        /// <param name="mediaType">Media type of the content produced by the serializer.</param>
        /// <returns>HttpContent object with the serialized object.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static HttpContent GetSerializedContent<T>(this T item, XmlObjectSerializer serializer, string mediaType)
        {
            Contract.Requires(item != null);
            Contract.Requires(serializer != null);
            Contract.Ensures(Contract.Result<HttpContent>() != null);
            Contract.Ensures(Contract.Result<HttpContent>().Headers.ContentType.MediaType.Equals(mediaType, StringComparison.Ordinal));

            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.WriteObject(stream, item);
                stream.Position = 0;

                StreamContent content = new StreamContent(stream);
                content.Headers.ContentType = new MediaTypeHeaderValue(mediaType);
                return content;
            }
            catch
            {
                stream.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Gets content that is the serialized item.
        /// </summary>
        /// <typeparam name="T">Type of the item.</typeparam>
        /// <param name="item">Item to serialize.</param>
        /// <param name="knownTypes">Known types that cannot be resolved by the
        /// data contract serializer.</param>
        /// <returns>HttpContent with the serialized object.</returns>
        public static HttpContent GetDataContractJsonContent<T>(this T item, Type[] knownTypes = null)
        {
            Contract.Requires(item != null);
            Contract.Ensures(Contract.Result<HttpContent>() != null);
            Contract.Ensures(Contract.Result<HttpContent>().Headers.ContentType.MediaType.Equals("application/json", StringComparison.Ordinal));

            return GetSerializedContent(item, new DataContractJsonSerializer(typeof(T), knownTypes), "application/json");
        }

        /// <summary>
        /// Reads a serialized object from the content object.
        /// </summary>
        /// <typeparam name="T">Type of the item to read.</typeparam>
        /// <param name="serializer">Serializer to use to deserialize the items.</param>
        /// <returns>Deserialized item.</returns>
        public static T ReadSerializedContent<T>(this HttpContent content, XmlObjectSerializer serializer)
        {
            Contract.Requires(content != null);
            Contract.Requires(content.ContentReadStream != null);
            Contract.Requires(serializer != null);
            Contract.Ensures(Contract.Result<T>() != null);

            return (T)serializer.ReadObject(content.ContentReadStream);
        }

        /// <summary>
        /// Reads a serialized object from the content object.
        /// </summary>
        /// <typeparam name="T">Type of the item to read.</typeparam>
        /// <param name="content">Content object to read from.</param>
        /// <param name="knownTypes">Types that are known during </param>
        /// <returns>Deserialized item.</returns>
        public static T ReadDataContractJsonContent<T>(this HttpContent content, Type[] knownTypes = null)
        {
            Contract.Requires(content != null);
            Contract.Requires(content.ContentReadStream != null);
            Contract.Ensures(Contract.Result<T>() != null);

            return ReadSerializedContent<T>(content, new DataContractJsonSerializer(typeof(T), knownTypes));
        }

        #endregion

        #region Json string serialization

        /// <summary>
        /// Converts the object to a JSON string.
        /// </summary>
        /// <typeparam name="T">Type of the object.</typeparam>
        /// <param name="item">Item to be serialized.</param>
        /// <param name="knownTypes">Known types that cannot be resolved by the
        /// data contract serializer.</param>
        /// <returns>A JSON string.</returns>
        public static string ToJsonString<T>(this T item, Type[] knownTypes = null)
        {
            Contract.Requires(item != null);
            Contract.Ensures(Contract.Result<string>() != null);

            var serializer = new DataContractJsonSerializer(typeof(T), knownTypes);

            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, item);
                stream.Position = 0;

                StreamReader reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Gets the object that has been serialized to JSON.
        /// </summary>
        /// <typeparam name="T">Type of the object.</typeparam>
        /// <param name="item">String to be deserialized.</param>
        /// <param name="knownTypes">Known types that cannot be resolved by the
        /// data contract serializer.</param>
        /// <returns>The unserialized object.</returns>
        public static T FromJsonString<T>(this string item, Type[] knownTypes = null)
        {
            Contract.Requires(item != null);
            Contract.Ensures(Contract.Result<T>() != null);

            var serializer = new DataContractJsonSerializer(typeof(T), knownTypes);
            using (MemoryStream stream = new MemoryStream())
            {
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(item);
                writer.Flush();
                stream.Position = 0;
                return (T)serializer.ReadObject(stream);
            }
        }

        #endregion

        #region Enum

        /// <summary>
        /// Parse the string as an enumeration.
        /// </summary>
        /// <typeparam name="T">Type of the enumeration.</typeparam>
        /// <param name="value">Value to be parse.</param>
        /// <param name="ignoreCase">Whether to ignore case when parsing.</param>
        /// <returns>Enum with the given value.</returns>
        public static T ParseEnum<T>(this string value, bool ignoreCase = true)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }

        #endregion

    }

}
