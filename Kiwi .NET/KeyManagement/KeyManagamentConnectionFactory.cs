﻿using System;
using System.IO;
using System.Diagnostics.Contracts;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Data.Common;
using System.Data.EntityClient;

namespace Kiwi.KeyManagement
{

    /// <summary>
    /// Factory class for creating entity connections for use with the key manager.
    /// </summary>
    [Pure]
    internal static class KeyManagerConnectionFactory
    {

        #region Path Properties

        /// <summary>
        /// Name of the Kiwi folder in the user's AppData/Local path.
        /// </summary>
        private const string KiwiFolderName = "Kiwi";

        /// <summary>
        /// Full path to the Kiwi Appdir.
        /// </summary>
        private static readonly string AppDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), KiwiFolderName);

        #endregion

        #region Static Constructor

        /// <summary>
        /// Ensures the existance of the app directory used by connection created in this class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
        static KeyManagerConnectionFactory()
        {
            if (!Directory.Exists(AppDirectory))
                Directory.CreateDirectory(AppDirectory);
        }

        #endregion

        #region Common Operations

        /// <summary>
        /// Creates the entity connection for key manager.
        /// </summary>
        /// <param name="provider">Provider to use.</param>
        /// <param name="providerConnectionString">Provider's connection string to DB.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static EntityConnection CreateEntityConnection(string provider, string providerConnectionString)
        {
            Contract.Requires(!string.IsNullOrEmpty(provider));
            Contract.Requires(!string.IsNullOrEmpty(providerConnectionString));
            Contract.Ensures(Contract.Result<EntityConnection>() != null);

            return new EntityConnection(
                new EntityConnectionStringBuilder()
                {
                    Provider = provider,
                    ProviderConnectionString = providerConnectionString,
                    Metadata = @"res://*/KeyManagement.csdl|res://*/KeyManagement.ssdl|res://*/KeyManagement.msl"
                }.ToString());
        }

        /// <summary>
        /// Gets the Db's password from the appropriate file.
        /// </summary>
        /// <returns>The password for use in the database.</returns>
        private static string GetDbPassword(string dbPasswordFilePath)
        {
            Contract.Requires(!string.IsNullOrEmpty(dbPasswordFilePath));
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

            byte[] passwordBytes = null;

            // If the password file doesn't exist, create a random password and save it.
            if (!File.Exists(dbPasswordFilePath))
            {
                // Generate a password
                using (RandomNumberGenerator random = RandomNumberGenerator.Create())
                {
                    passwordBytes = new byte[16];
                    random.GetNonZeroBytes(passwordBytes);
                }

                // Encrypt the data as the current user.
                byte[] protectedBytes = ProtectedData.Protect(passwordBytes, null, DataProtectionScope.CurrentUser);

                // Write the data.
                File.WriteAllBytes(dbPasswordFilePath, protectedBytes);
            }
            else
            {
                // Retreive the password from the file.
                byte[] protectedBytes = File.ReadAllBytes(dbPasswordFilePath);
                passwordBytes = ProtectedData.Unprotect(protectedBytes, null, DataProtectionScope.CurrentUser);
            }

            return Convert.ToBase64String(passwordBytes);
        }

        #endregion

        #region SQL CE

        /// <summary>
        /// Gets the path to the database.
        /// </summary>
        private static readonly string SqlCeDbPath = Path.Combine(AppDirectory, "KeyManagerStorage.sdf");

        /// <summary>
        /// Gets the path to the database's password.
        /// </summary>
        private static readonly string SqlCeDbPasswordPath = Path.Combine(AppDirectory, "KeyManagerStorage.sdf.password");

        /// <summary>
        /// Gets the provider.
        /// </summary>
        private const string SqlCeProvider = "System.Data.SqlServerCe.4.0";

        /// <summary>
        /// The static instance of the SqlCeConnection.
        /// </summary>
        private static EntityConnection SqlCeConnection { get; set; }

        /// <summary>
        /// Gets a SqlCEConnection
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public static EntityConnection GetSqlCeConnection()
        {
            Contract.Ensures(Contract.Result<EntityConnection>() != null);

            // Construct the connection if it hasn't been initialized yet.
            if (SqlCeConnection == null)
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = SqlCeDbPath;
                builder.Password = GetDbPassword(SqlCeDbPasswordPath);

                SqlCeConnection = CreateEntityConnection(SqlCeProvider, builder.ToString());
            }

            return SqlCeConnection;
        }

        #endregion

    }

}
