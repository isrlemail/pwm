﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Kiwi.LibKiwi;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;

namespace Kiwi.KeyManagement
{

    #region KeyRetrievedDelegate

    /// <summary>
    /// Delegate that is fired when a key has been retreived.
    /// </summary>
    /// <param name="success">Whether or not retrieval was successful.</param>
    /// <param name="args">The args that were passed in to get a key.</param>
    public delegate void KeyRequestFinished(bool success, byte[] key, KeyRequestArgs args);

    /// <summary>
    /// Arguments associated with retrieving keys.
    /// </summary>
    public sealed class KeyRequestArgs : IEquatable<KeyRequestArgs>
    {

        #region Properties

        /// <summary>
        /// The Uri of the key server.
        /// </summary>
        public Uri KeyServerUri { get; private set; }

        /// <summary>
        /// Gets the id of the creator.
        /// </summary>
        public string CreatorId { get; private set; }

        /// <summary>
        /// Gets the id of the viewer.
        /// </summary>
        public string ViewerId { get; private set; }

        /// <summary>
        /// Gets the timestamp for the key retrieval arguments.
        /// </summary>
        public DateTime Timestamp { get; private set; }

        /// <summary>
        /// Gets the size of the key.
        /// </summary>
        public int KeySize { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructors a key retrieval args object.
        /// </summary>
        /// <param name="keyServerUri">The Uri for the key server the key comes from.</param>
        /// <param name="creatorId">The id of the creator for the key.</param>
        /// <param name="viewerId">The Id of the viewer for the key.  Null for creator keys.</param>
        /// <param name="timestamp">The timestamp for the key.</param>
        /// <param name="keySize">The size of the key.</param>
        public KeyRequestArgs(Uri keyServerUri, string creatorId, string viewerId, DateTime timestamp, int keySize)
        {
            Contract.Requires(keyServerUri != null);
            Contract.Requires(!string.IsNullOrEmpty(creatorId));
            Contract.Requires(creatorId != null);
            Contract.Requires(viewerId == null || !string.IsNullOrEmpty(viewerId));
            Contract.Requires(keySize > 0);
            Contract.Ensures(KeyServerUri == keyServerUri);
            Contract.Ensures(CreatorId == creatorId);
            Contract.Ensures(ViewerId == viewerId);
            Contract.Ensures(Timestamp == timestamp);
            Contract.Ensures(KeySize == keySize);

            KeyServerUri = keyServerUri;
            CreatorId = creatorId;
            ViewerId = viewerId;
            Timestamp = timestamp;
            KeySize = keySize;
        }

        #endregion

        #region Equality Operators

        /// <summary>
        /// Checks for equality of two key request args, using the given period base instead of timestamp.
        /// </summary>
        /// <param name="other">Key request args to test for equality.</param>
        /// <returns>True if the key request args are for the same.</returns>
        [Pure]
        public bool Matches(KeyRequestArgs other, PeriodBase period)
        {
            return
                other != null &&
                this.KeyServerUri.Equals(other.KeyServerUri) &&
                this.CreatorId.Equals(other.CreatorId, StringComparison.OrdinalIgnoreCase) &&
                (this.ViewerId == null ? other.ViewerId == null : this.ViewerId.Equals(other.ViewerId, StringComparison.OrdinalIgnoreCase)) &&
                period.ValidForDateTime(this.Timestamp) &&
                this.KeySize == other.KeySize;
        }

       /// <summary>
        /// Checks equality by calling IEquatable{KeyRequestArgs}.Equals().
        /// </summary>
        /// <param name="obj">Object to test for equality.</param>
        /// <returns>True if the key periods are for the same period.</returns>
        [Pure]
        public override bool Equals(object obj)
        {
            return Equals(obj as PeriodBase);
        }

        /// <summary>
        /// Checks for equality of two key request args..
        /// </summary>
        /// <param name="other">Key request args to test for equality.</param>
        /// <returns>True if the key request args are for the same.</returns>
        [Pure]
        public bool Equals(KeyRequestArgs other)
        {
            return 
                other != null &&
                this.KeyServerUri.Equals(other.KeyServerUri) &&
                this.CreatorId.Equals(other.CreatorId, StringComparison.OrdinalIgnoreCase) &&
                (this.ViewerId == null ? other.ViewerId == null : this.ViewerId.Equals(other.ViewerId, StringComparison.OrdinalIgnoreCase)) &&
                this.Timestamp == other.Timestamp &&
                this.KeySize == other.KeySize;
        }

        /// <summary>
        /// Gets the hash code for the key request args.
        /// </summary>
        /// <returns>Hashcode for the key request args.</returns>
        [Pure]
        public override int GetHashCode()
        {
            return
                this.KeyServerUri.GetHashCode() ^
                this.CreatorId.GetHashCode() ^
                (ViewerId == null ? 0 : ViewerId.GetHashCode()) ^
                this.Timestamp.GetHashCode() ^
                this.KeySize.GetHashCode();
        }

        /// <summary>
        /// Compares equality of the two key request args.
        /// </summary>
        /// <param name="lhs">First key request args.</param>
        /// <param name="rhs">Second key request args.</param>
        /// <returns>Equality.</returns>
        [Pure]
        public static bool operator ==(KeyRequestArgs lhs, KeyRequestArgs rhs)
        {
            if (object.Equals(lhs, null))
                return object.Equals(rhs, null);
            else
                return lhs.Equals(rhs);
        }

        /// <summary>
        /// Compares equality of the two key request args.
        /// </summary>
        /// <param name="lhs">First key request args.</param>
        /// <param name="rhs">Second key request args.</param>
        /// <returns>Equality.</returns>
        [Pure]
        public static bool operator !=(KeyRequestArgs lhs, KeyRequestArgs rhs)
        {
            if (object.Equals(lhs, null))
                return !object.Equals(rhs, null);
            else
                return !lhs.Equals(rhs);
        }

        #endregion       

    }

    #endregion

    #region Auth Token Delegates

    /// <summary>
    /// Delegate for password requests.
    /// </summary>
    /// <param name="authServerUri">The Uri that requested the password.</param>
    /// <returns>The password typed in.</returns>
    public delegate string GetPasswordRequest(Uri authServerUri);

    /// <summary>
    /// Delegate to be fired when a saw token is requested for the given user.
    /// </summary>
    /// <param name="userId">Id of the user the token was requested for.</param>
    public delegate void SawTokenRequest(string userId, string transactionId);

    #endregion

    public sealed class KeyManager
    {

        #region Key Request Type

        /// <summary>
        /// Enumeration for the type of key requested.
        /// </summary>
        private enum KeyRequestType 
        {
            CreatorKey,
            ViewerKey
        }

        #endregion

        #region Constants

        /// <summary>
        /// The amount of time in milliseconds to wait for a saw request.
        /// </summary>
        private const int SawWaitTime = 30000;

        #endregion

        #region Properties / Fields

        /// <summary>
        /// Gets or sets the preferred authentication method.
        /// </summary>
        public AuthenticationMethod PreferredAuthenticationMethod { get; set; }

        /// <summary>
        /// Instance of KeyManagerStorageContainer for interacting with the data store.
        /// </summary>
        private KeyManagementContainer Container { get; set; }

        /// <summary>
        /// The set of requests currently waiting to be filled, and the callbacks to be called
        /// when the request is finished.
        /// </summary>
        private Dictionary<KeyRequestArgs, KeyRequestFinished> RequestCallbacks { get; set; }

        /// <summary>
        /// The set of requests currently waiting to be filled, and their associated tasks.
        /// </summary>
        private Dictionary<KeyRequestArgs, CancellationTokenSource> RequestTasks { get; set; }

         /// <summary>
        /// The set of auth requests currently waiting to be filled, and their associated tasks.
        /// </summary>
        private Dictionary<Tuple<string, Uri>, Task<AuthToken>> AuthRequestTasks { get; set; }
 
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a key manager.
        /// </summary>
        public KeyManager()
        {
            Contract.Ensures(RequestCallbacks != null);
            Contract.Ensures(RequestTasks != null);
            Contract.Ensures(AuthRequestTasks != null);
            
            RequestCallbacks = new Dictionary<KeyRequestArgs, KeyRequestFinished>();
            RequestTasks = new Dictionary<KeyRequestArgs, CancellationTokenSource>();
            AuthRequestTasks = new Dictionary<Tuple<string, Uri>, Task<AuthToken>>();

            // Creates the container and ensures that the database exists.
            Container = new KeyManagementContainer(KeyManagerConnectionFactory.GetSqlCeConnection());
            if (!Container.DatabaseExists())
                Container.CreateDatabase();
        }

        #endregion

        #region Password auth properties

        /// <summary>
        /// Delegate to handle requested passwords.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly",
            Justification = "This value only needs to be set, no one but its viewer needs know what it is.")]
        public GetPasswordRequest PasswordHandler { private get; set; }
        
        #endregion

        #region Saw auth properties

        /// <summary>
        /// Delegate that is called when a saw token is received.
        /// </summary>
        /// <param name="transactionId">ID of the transaction the SAW token is for.</param>
        /// <param name="value">Value of the SAW token.</param>
        private delegate void SawTokenReceivedDelegate(string transactionId, byte[] value);

        /// <summary>
        /// The internal event fired when saw token received.
        /// </summary>
        private event SawTokenReceivedDelegate sawTokenReceived;

        /// <summary>
        /// An object used in lock statements.
        /// </summary>
        private object sawTokenReceivedLock = new object();

        /// <summary>
        /// Proxy for registering events to sawTokenReceived, since it will add synchronization.
        /// </summary>
        private event SawTokenReceivedDelegate SawTokenReceived
        {
            add
            {
                lock (sawTokenReceivedLock)
                {
                    sawTokenReceived += value;
                }
            }

            remove
            {
                lock (sawTokenReceivedLock)
                {
                    sawTokenReceived -= value;
                }
            }
        }

        /// <summary>
        /// Register a SAW token received through email.
        /// </summary>
        /// <param name="transactionId">The transaction ID.</param>
        /// <param name="value">The value of the token.</param>
        public void RegisterSawToken(string transactionId, byte[] value)
        {
            lock(sawTokenReceivedLock)
            {
                if (sawTokenReceived != null)
                {
                    foreach (SawTokenReceivedDelegate @delegate in sawTokenReceived.GetInvocationList())
                        @delegate.BeginInvoke(transactionId, value, null, null);
                }
            }
        }

        /// <summary>
        /// Fired when a saw token is requested.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix")]
        public event SawTokenRequest SawTokenRequested;

        /// <summary>
        /// Fired when a token has been requested.
        /// </summary>
        /// <param name="userId">Id of the user.</param>
        private void OnSawTokenRequested(string userId, string transactionId)
        {
            if (SawTokenRequested != null)
            {
                SawTokenRequested.BeginInvoke(userId, transactionId, null, null);
            }
        }

        #endregion

        #region Key Server Methods

        /// <summary>
        /// Gets the list of all key servers.
        /// </summary>
        /// <returns>Key servers in the database.</returns>
        public IEnumerable<Uri> GetKeyServers()
        {
            Contract.Ensures(Contract.Result<IEnumerable<Uri>>() != null);

            return
                from server in Container.KeyServers.ToList()
                select server.ActualUri;
        }

        /// <summary>
        /// Ads the given uri to the key server.
        /// </summary>
        /// <param name="uri"></param>
        public void AddKeyServer(Uri uri)
        {
            Contract.Requires(uri != null);

            if (Container.KeyServers.ToList().Any(item => item.ActualUri.Equals(uri)))
                return;

            Container.KeyServers.AddObject(new KeyServer()
            {
                ActualUri = uri,
                IsDefault = false,
            });

            Container.SaveChanges();
        }

        /// <summary>
        /// Attemps to delete the given key server.
        /// </summary>
        /// <param name="uri">Uri of the key server.</param>
        public void DeleteKeyServer(Uri uri)
        {
            Contract.Requires(uri != null);

            var keyServer = Container.KeyServers.ToList().Find(item => item.ActualUri.Equals(uri));

            if (keyServer != null)
                Container.KeyServers.DeleteObject(keyServer);

            Container.SaveChanges();
        }

        /// <summary>
        /// Gets the Uri for the default key server.
        /// </summary>
        /// <returns>Uri of the default key server.</returns>
        public Uri GetDefaultKeyServer()
        {
            var keyServer = Container.KeyServers.ToList().Find(item => item.IsDefault);
            if (keyServer == null)
                keyServer = Container.KeyServers.FirstOrDefault();

            if (keyServer == null)
                return null;
            else
                return keyServer.ActualUri;
        }

        /// <summary>
        /// Sets the default key server.
        /// </summary>
        /// <param name="uri">Uri of the key server.</param>
        public void SetDefaultKeyServer(Uri uri)
        {
            Contract.Requires(uri != null);

            var keyServer = Container.KeyServers.ToList().Find(item => item.ActualUri.Equals(uri));

            if (keyServer != null)
            {
                Container.KeyServers.ToList().ForEach(item => item.IsDefault = false);
                keyServer.IsDefault = true;
            }

            Container.SaveChanges();
        }

        #endregion

        #region Key retrieval wrappers

        /// <summary>
        /// Begins an async request for the creator key.
        /// </summary>
        /// <param name="args">Arguments about the key needing retrieval.</param>
        /// <param name="callback">Callback to be called when finished.</param>
        [Pure]
        public void GetCreatorKeyAsync(KeyRequestArgs args, KeyRequestFinished callback)
        {
            Contract.Requires(args != null);
            Contract.Requires(callback != null);
            Contract.Requires(args.ViewerId == null);

            BeginKeyRequest(args, callback, KeyRequestType.CreatorKey);
        }

        /// <summary>
        /// Begins an async request for the viewer key.
        /// </summary>
        /// <param name="args">Arguments about the key needing retrieval.</param>
        /// <param name="callback">Callback to be called when finished.</param>
        [Pure]
        public void GetViewerKeyAsync(KeyRequestArgs args, KeyRequestFinished callback)
        {
            Contract.Requires(args != null);
            Contract.Requires(callback != null);
            Contract.Requires(!string.IsNullOrEmpty(args.ViewerId));

            BeginKeyRequest(args, callback, KeyRequestType.ViewerKey);
        }

        /// <summary>
        /// Starts the given key request as a seperate task.
        /// </summary>
        /// <param name="args">Arguments about the key needing retrieval.</param>
        /// <param name="callback">Callback to be called when finished.</param>
        /// <param name="type">The type of key requested.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "We don't know when to dispose the tasks.")]
        [Pure]
        private void BeginKeyRequest(KeyRequestArgs args, KeyRequestFinished callback, KeyRequestType type)
        {
            Contract.Requires(args != null);
            Contract.Requires(callback != null);
            Contract.Requires(type == KeyRequestType.CreatorKey ? args.ViewerId == null : !string.IsNullOrEmpty(args.ViewerId));

            // Launch the task to get a viewer key.
            // If the request already exists register to be notified when
            // it is done, otherwise create one.
            Task task = null;
            lock (this)
            {
                if (RequestCallbacks.ContainsKey(args))
                {
                    RequestCallbacks[args] += callback;
                }
                else
                {
                    CancellationTokenSource cancelationTokenSource = new CancellationTokenSource();
                    CancellationToken token = cancelationTokenSource.Token;

                    task = new Task(() =>
                    {
                        try
                        {
                            switch (type)
                            {
                                case KeyRequestType.CreatorKey:
                                    GetCreatorKey(args, token);
                                    break;
                                case KeyRequestType.ViewerKey:
                                    GetViewerKey(args, token);
                                    break;
                            }
                        }
                        catch (OperationCanceledException)
                        {
                            return;
                        }
                        catch
                        {
                            EndKeyRequest(false, null, null, args, type);
                        }
                    }, token, TaskCreationOptions.LongRunning);

                    RequestCallbacks[args] = callback;
                    RequestTasks[args] = cancelationTokenSource;
                }
            }

            if (task != null)
                task.Start();
    
        }

        /// <summary>
        /// Called when a given task was finished.
        /// </summary>
        /// <param name="success">Whether the operation was successful or not.</param>
        /// <param name="value">The value of the retrieved key.</param>
        /// <param name="args">Arguments about the key needing retrieval.</param>
        /// <param name="type">The type of key requested.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        [Pure]
        private void EndKeyRequest(bool success, byte[] value, PeriodBase period, KeyRequestArgs args, KeyRequestType type)
        {
            Contract.Requires(success ? value != null : true);
            Contract.Requires(success ? value.Length == args.KeySize : true);
            Contract.Requires(success ? period.ValidForDateTime(args.Timestamp) : true);
            Contract.Requires(args != null);
            Contract.Requires(type == KeyRequestType.CreatorKey ? args.ViewerId == null : !string.IsNullOrEmpty(args.ViewerId));

            KeyRequestFinished callbackDelegate = null;

            lock (this)
            {
                // If there are no callbacks for this args, it means
                // they have already been notified by another key request.
                if (!RequestCallbacks.ContainsKey(args))
                    return;

                callbackDelegate = RequestCallbacks[args];
                RequestCallbacks.Remove(args);
                RequestTasks.Remove(args);

                // If the request was succesful, find other tasks that would be interested
                // in this key, cancel them and call their delegates.
                if (success)
                {
                    // Insert the key.
                    lock (Container)
                    {
                        switch (type)
                        {
                            case KeyRequestType.CreatorKey:
                                Container.CreatorKeys.AddObject(new CreatorKey()
                                {
                                    CreatorId = args.CreatorId,
                                    KeyServerUri = args.KeyServerUri.ToString(),
                                    ActualPeriod = period,
                                    Value = value,
                                    KeySize = args.KeySize
                                });

                                // Also add the viewer key needed to view drafts.
                                byte[] viewerKey = Hkdf.DeriveViewerKey(value, args.CreatorId, args.KeySize);
                                Container.ViewerKeys.AddObject(new ViewerKey()
                                {
                                    CreatorId = args.CreatorId,
                                    ViewerId = args.CreatorId,
                                    KeyServerUri = args.KeyServerUri.ToString(),
                                    ActualPeriod = period,
                                    Value = viewerKey,
                                    KeySize = args.KeySize
                                });
                                break;

                            case KeyRequestType.ViewerKey:
                                Container.ViewerKeys.AddObject(new ViewerKey()
                                {
                                    CreatorId = args.CreatorId,
                                    ViewerId = args.ViewerId,
                                    KeyServerUri = args.KeyServerUri.ToString(),
                                    ActualPeriod = period,
                                    Value = value,
                                    KeySize = args.KeySize
                                });
                                break;
                        }

                        Container.SaveChanges();
                    }

                    // Find and respond to matching callbacks.
                    var matchingKeys =
                        from arg in RequestCallbacks.Keys
                        where arg.Matches(args, period)
                        select arg;

                    foreach(var key in matchingKeys)
                    {
                        callbackDelegate += RequestCallbacks[key];
                        RequestCallbacks.Remove(key);
                        RequestTasks[key].Cancel();
                        RequestTasks.Remove(key);
                    }
                }
            }

            // Fire an event to the delegates that are interested in this event.
            // We catch erros because it doesn't matter if they throw them.
            try
            {
                foreach (KeyRequestFinished @delegate in callbackDelegate.GetInvocationList())
                    @delegate.BeginInvoke(success, value, args, null, null);
            }
            catch {}
        }

        #endregion

        #region Key retrieval methods

        /// <summary>
        /// Retrieves a creator key.
        /// </summary>
        /// <param name="args">The args for the creator key.</param>
        /// <param name="token">A token that can be called to cancel a thread.</param>
        [Pure]
        private void GetCreatorKey(KeyRequestArgs args, CancellationToken token)
        {
            Contract.Requires(args != null);
            Contract.Requires(args.ViewerId == null);
            Contract.Requires(token != null);

            if (token.IsCancellationRequested)
                throw new OperationCanceledException(token);

            // Attempt to find a matching key.
            CreatorKey matchingKey = null;
            lock (Container)
            {
                string serverUriString = args.KeyServerUri.ToString();
                matchingKey = (
                    from key in
                        (
                            from key in Container.CreatorKeys
                            where args.CreatorId.Equals(key.CreatorId, StringComparison.OrdinalIgnoreCase) &&
                                  serverUriString.Equals(key.KeyServerUri, StringComparison.Ordinal) &&
                                  args.KeySize == key.KeySize
                            select key).ToList()
                    where key.ActualPeriod.ValidForDateTime(args.Timestamp)
                    select key).FirstOrDefault();
            }

            if (matchingKey != null)
            {
                EndKeyRequest(true, matchingKey.Value, matchingKey.ActualPeriod, args, KeyRequestType.CreatorKey);
                return;
            }

            if (token.IsCancellationRequested)
                throw new OperationCanceledException(token);

            // No matching key, so get an auth token so we can request a key.
            AuthToken authToken = GetAuthToken(args.KeyServerUri, args.CreatorId, token);

            if (authToken == null)
                throw new InvalidOperationException("No auth token to get key with.");

            if (token.IsCancellationRequested)
                throw new OperationCanceledException(token);

            // Attempt to retrieve the key from the server.
            Tuple<byte[], PeriodBase> creatorKey = KiwiServerOperations.GetCreatorKey(args.KeyServerUri, authToken, args.CreatorId, args.Timestamp, args.KeySize);
            EndKeyRequest(true, creatorKey.Item1, creatorKey.Item2, args, KeyRequestType.CreatorKey);
        }

        /// <summary>
        /// Retrieves a viewer key.
        /// </summary>
        /// <param name="args">The args for the creator key.</param>
        /// <param name="token">A token that can be called to cancel a thread.</param>
        [Pure]
        private void GetViewerKey(KeyRequestArgs args, CancellationToken token)
        {
            Contract.Requires(args != null);
            Contract.Requires(!string.IsNullOrEmpty(args.ViewerId));
            Contract.Requires(token != null);

            if (token.IsCancellationRequested)
                throw new OperationCanceledException(token);

            // Attempt to find a matching key.
            ViewerKey matchingKey = null;
            lock (Container)
            {
                string serverUriString = args.KeyServerUri.ToString();
                matchingKey = (
                    from key in
                        (
                            from key in Container.ViewerKeys
                            where args.CreatorId.Equals(key.CreatorId, StringComparison.OrdinalIgnoreCase) &&
                                  args.ViewerId.Equals(key.ViewerId, StringComparison.OrdinalIgnoreCase) &&
                                  serverUriString.Equals(key.KeyServerUri, StringComparison.Ordinal) &&
                                  args.KeySize == key.KeySize
                            select key).ToList()
                    where key.ActualPeriod.ValidForDateTime(args.Timestamp)
                    select key).FirstOrDefault();
            }

            if (matchingKey != null)
            {
                EndKeyRequest(true, matchingKey.Value, matchingKey.ActualPeriod, args, KeyRequestType.ViewerKey);
                return;
            }

            if (token.IsCancellationRequested)
                throw new OperationCanceledException(token);

            // No matching key, so get an auth token so we can request a key.
            AuthToken authToken = GetAuthToken(args.KeyServerUri, args.ViewerId, token);

            if (authToken == null)
                throw new InvalidOperationException("No auth token to get key with.");

            if (token.IsCancellationRequested)
                throw new OperationCanceledException(token);

            // Attempt to retrieve the key from the server.
            Tuple<byte[], PeriodBase> viewerKey = KiwiServerOperations.GetViewerKey(args.KeyServerUri, authToken, args.CreatorId, args.ViewerId, args.Timestamp, args.KeySize);
            EndKeyRequest(true, viewerKey.Item1, viewerKey.Item2, args, KeyRequestType.ViewerKey);
        }

        #endregion

        #region Auth token retrieval methods

        /// <summary>
        /// Gets the authentication token for the given key server.
        /// </summary>
        /// <param name="keyServerUri">Key server to get token for.</param>
        /// <param name="userId">ID of the user the token is for.</param>
        /// <param name="cancellationToken">A token that can be used to cancel waiting on the task.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope"), Pure]
        private AuthToken GetAuthToken(Uri keyServerUri, string userId, CancellationToken cancellationToken)
        {
            Contract.Requires(keyServerUri != null);
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Requires(cancellationToken != null);
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().IsTokenComplete);
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().Period.IsCurrent());
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().UserId.Equals(userId, StringComparison.OrdinalIgnoreCase));

            // Spawn off a task, or use one that already exists to get the auth token.
            var key = new Tuple<string, Uri>(userId, keyServerUri);
            Task<AuthToken> task = null;
            bool startTask = false;
            lock (this)
            {
                if (AuthRequestTasks.ContainsKey(key))
                    task = AuthRequestTasks[key];
                else
                {
                    task = new Task<AuthToken>(() => { return GetAuthTokenAsync(keyServerUri, userId); }, TaskCreationOptions.LongRunning);
                    AuthRequestTasks[key] = task;
                    startTask = true;
                }
            }

            // If the task is new start it.
            if (startTask)
                task.Start();

            // Wait for the task to complete, and then get the result.
            if(!task.IsCompleted)
                task.Wait(cancellationToken);

            if (cancellationToken.IsCancellationRequested)
                throw new OperationCanceledException(cancellationToken);

            // If the task has failed to find a token, report an error, but also
            // remove it from the list so further attempts will work.
            if (task.Result == null)
            {
                lock (this)
                {
                    if(AuthRequestTasks.ContainsKey(key))
                        AuthRequestTasks.Remove(key);
                }

                return null;
            }
            else
                return task.Result;
        }

        /// <summary>
        /// Gets the authentication token for the given key server.  Run in a different task
        /// from the main thread.
        /// </summary>
        /// <param name="keyServerUri">Key server to get token for.</param>
        /// <param name="userId">ID of the user the token is for.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "We don't really care why it failed, just that it did.")]
        [Pure]
        private AuthToken GetAuthTokenAsync(Uri keyServerUri, string userId)
        {
            Contract.Requires(keyServerUri != null);
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().IsTokenComplete);
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().Period.IsCurrent());
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().UserId.Equals(userId, StringComparison.OrdinalIgnoreCase));

            // Get the list of valid auth servers for the given key server, and then
            // check if their is a valid auth token we could use for any of these.
            var authServers = KiwiServerOperations.GetAuthServers(keyServerUri);
            var authServerUriStrings = authServers.Select(uri => uri.ToString());

            AuthToken validToken = GetAuthTokenFromDb(userId, authServerUriStrings);
            if (validToken != null)
                return validToken;

            // If there are no valid token, we will ask all the servers serially what methods they support,
            // until we find one that supports the prefferred method.  If none support the preffered method,
            // or if all preferred methods fail, then we will attempt to use other methods.
            var remainingMethods = new List<Tuple<AuthenticationMethod, Uri>>();

            foreach (var authServerUri in authServers)
            {
                try
                {
                    var methodsAndUris = KiwiServerOperations.GetAuthMethodsAndUris(authServerUri);

                    foreach (var value in methodsAndUris)
                    {
                        if (value.Item1 == PreferredAuthenticationMethod)
                        {
                            AuthToken token = TryGetAuthToken(userId, value.Item2, value.Item1);
                            if (token != null)
                                return token;
                        }
                        else
                            remainingMethods.Add(value);
                    }
                }
                catch { }
            }

            foreach (var value in remainingMethods)
            {
                AuthToken token = TryGetAuthToken(userId, value.Item2, value.Item1);
                if (token != null)
                    return token;
            }

            // Check one last time to see if any outstanding tokens were completed.
            validToken = GetAuthTokenFromDb(userId, authServerUriStrings);
            if (validToken != null)
                return validToken;

            // Otherwise all forms of authentication have failed.
            return null;
        }

        /// <summary>
        /// Tries and get an auth token.
        /// </summary>
        /// <param name="userId">Id of the user to try with.</param>
        /// <param name="method">Type of authentication being done.</param>
        /// <param name="authUri">The URI to do authentication against.</param>
        /// <returns>The auth token retrieved if one was obtained.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
            Justification = "We don't really care why it failed, just that it did.")]
        private AuthToken TryGetAuthToken(string userId, Uri authUri, AuthenticationMethod method)
        {
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Requires(authUri != null);
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().IsTokenComplete);
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().Period.IsCurrent());
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().UserId.Equals(userId, StringComparison.OrdinalIgnoreCase));

            try
            {
                AuthToken token = null;

                // Switch what we do based on the method.  SAW is different enough from the other two
                // in its operations that more operations cannot be moved out of this switch.
                switch(method) {

                    case AuthenticationMethod.Saw:
                        // Setup a handler to accept the second half of the token.
                        Semaphore semaphore = new Semaphore(0, 1);

                        SawTokenReceivedDelegate handler = null;
                        handler = (string transactionId, byte[] value) =>
                        {
                            if (token.TransactionId.Equals(transactionId, StringComparison.Ordinal))
                            {
                                // Complete the token.
                                token.Combine(value, true);
                                InsertAuthToken(token);

                                // Signal that the token is complete.
                                semaphore.Release();
                                SawTokenReceived -= handler;
                            }
                        };

                        SawTokenReceived += handler;

                        // We wait to get the auth token till here so we don't get the token
                        // before we are ready to accept it.
                        token = KiwiServerOperations.GetAuthToken(authUri, userId);
                        OnSawTokenRequested(userId, token.TransactionId);

                        if (semaphore.WaitOne(SawWaitTime))
                            return token;
                        else
                            return null;
                
                    case AuthenticationMethod.Password:
                        // Get the token and then get a password to unwrap it.
                        if (PasswordHandler == null)
                            return null;

                        token = KiwiServerOperations.GetAuthToken(authUri, userId);

                        string password = PasswordHandler(authUri);
                        if (password == null)
                            return null;

                        token.DecryptWithPasswordKey(token.GetPasswordKey(password));
                        InsertAuthToken(token);
                        return token;

                    case AuthenticationMethod.None:
                        // Since their is no authentication, jsut get the token and return it.
                        token = KiwiServerOperations.GetAuthToken(authUri, userId);
                        InsertAuthToken(token);
                        return token;
                }
            }
            catch {}

            // No token retrieved.
            return null;
        }

        /// <summary>
        /// Gets an auth token from the db.
        /// </summary>
        /// <param name="userId">The user the auth token is from.</param>
        /// <param name="authServers">The list of auth servers the token must be for.</param>
        /// <returns>An auth token if one exists.</returns>
        private AuthToken GetAuthTokenFromDb(string userId, IEnumerable<string> authServers)
        {
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Requires(authServers != null);
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().IsTokenComplete);
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().Period.IsCurrent());
            Contract.Ensures(Contract.Result<AuthToken>() == null || Contract.Result<AuthToken>().UserId.Equals(userId, StringComparison.OrdinalIgnoreCase));

            lock (Container)
            {
                var validToken = (
                    from token in
                        (
                            from token in Container.AuthTokens
                            where token.UserId.Equals(userId, StringComparison.OrdinalIgnoreCase)
                            select token).ToList()
                    where authServers.Contains(token.AuthServerUri, StringComparer.Ordinal)&&
                          token.ActualPeriod.IsCurrent()
                    select token).FirstOrDefault();

                return validToken == null ? null : validToken.GetAuthToken();
            }
        }

        /// <summary>
        /// Inserts the auth token into the storage container.
        /// </summary>
        /// <param name="token">Auth token to insert.</param>
        private void InsertAuthToken(AuthToken token)
        {
            Contract.Requires(token != null);

            lock (Container)
            {
                Container.AuthTokens.AddObject(new AuthTokenDb(token));
                Container.SaveChanges();
            }
        }

        #endregion

    }
}
