﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kiwi.LibKiwi;
using System.Diagnostics.Contracts;

namespace Kiwi.KeyManagement
{

    /// <summary>
    /// Extensions for CreatorKey class.
    /// </summary>
    internal partial class CreatorKey
    {

        #region Properties

        /// <summary>
        /// An internal value that caches the value of the period.
        /// </summary>
        [NonSerialized]
        private PeriodBase mPeriod;

        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        internal PeriodBase ActualPeriod
        {
            get
            {
                if (mPeriod == null)
                    mPeriod = Period.FromJsonString<PeriodBase>();
                return mPeriod;
            }

            set
            {
                Contract.Requires(value != null);

                mPeriod = value;
                Period = value.ToJsonString();
            }
        }

        #endregion

    }

    /// <summary>
    /// Extensions for ViewerKey class.
    /// </summary>
    internal partial class ViewerKey
    {

        #region Properties

        /// <summary>
        /// An internal value that caches the value of the period.
        /// </summary>
        [NonSerialized]
        private PeriodBase mPeriod;

        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        internal PeriodBase ActualPeriod
        {
            get
            {
                if (mPeriod == null)
                    mPeriod = Period.FromJsonString<PeriodBase>();
                return mPeriod;
            }

            set
            {
                Contract.Requires(value != null);

                mPeriod = value;
                Period = value.ToJsonString();
            }
        }

        #endregion

    }

    /// <summary>
    /// Extensions for AuthTokenDb class.
    /// </summary>
    internal partial class AuthTokenDb
    {

        #region Properties

        /// <summary>
        /// An internal value that caches the value of the period.
        /// </summary>
        [NonSerialized]
        private PeriodBase mPeriod;

        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        internal PeriodBase ActualPeriod
        {
            get
            {
                if (mPeriod == null)
                    mPeriod = Period.FromJsonString<PeriodBase>();
                return mPeriod;
            }

            set
            {
                Contract.Requires(value != null);

                mPeriod = value;
                Period = value.ToJsonString();
            }
        }

        #endregion

        #region Transform Methods

        /// <summary>
        /// Default contsructor.
        /// </summary>
        public AuthTokenDb() {}

        /// <summary>
        /// Creates an auth token DB record from an AuthToken instance.
        /// </summary>
        /// <param name="token">AuthToken instance represented by this class.</param>
        public AuthTokenDb(AuthToken token)
        {
            Contract.Requires(token != null);

            UserId = token.UserId;
            ActualPeriod = token.Period;
            Value = token.Value;
            AuthServerUri = token.AuthServerUri.ToString();
            Method = token.Method.ToString();
        }

        /// <summary>
        /// Gets an auth token representation of this DB class.
        /// </summary>
        /// <returns>AuthToken.</returns>
        public AuthToken GetAuthToken()
        {
            Contract.Requires(!string.IsNullOrEmpty(UserId));
            Contract.Requires(ActualPeriod != null);
            Contract.Requires(Value != null && Value.Length != 0);
            Contract.Requires(!string.IsNullOrEmpty(AuthServerUri));
            Contract.Requires(Enum.GetNames(typeof(AuthenticationMethod)).Contains(Method, StringComparer.OrdinalIgnoreCase));
            Contract.Ensures(Contract.Result<AuthToken>() != null);

            return new AuthToken(UserId, ActualPeriod, Value, new Uri(AuthServerUri), Method.ParseEnum<AuthenticationMethod>());

        }

        #endregion

    }

    /// <summary>
    /// Extensions for KeyServer.
    /// </summary>
    internal partial class KeyServer
    {

        #region Properties

        /// <summary>
        /// An internal value that caches the value of the KeyServerUri.
        /// </summary>
        [NonSerialized]
        private Uri mKeyServerUri;

        /// <summary>
        /// Gets or sets the KeyServerUri.
        /// </summary>
        internal Uri ActualUri
        {
            get
            {
                if (mKeyServerUri == null)
                    mKeyServerUri = new Uri(KeyServerUri);
                return mKeyServerUri;
            }

            set
            {
                Contract.Requires(value != null);

                mKeyServerUri = value;
                KeyServerUri = value.ToString();
            }
        }

        #endregion

    }

}
