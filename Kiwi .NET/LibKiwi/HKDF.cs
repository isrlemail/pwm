﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Security.Cryptography;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// HKDF uses a key derivation function based upon the article 
    /// at http://eprint.iacr.org/2010/264.pdf.  The public functions
    /// of this class are used by other parts of the kiwi system to
    /// generate the keys that they use.
    /// (RFC: http://tools.ietf.org/search/rfc5869)
    /// 
    /// This HKDF does not use a salt, and assumes that passed in keys
    /// are cryptographically random.  Uses SHA-512 as the base of its
    /// HMAC function.
    /// </summary>
    [Pure]
    public static class Hkdf
    {

        #region Constants

        /// <summary>
        /// The key length for the HMAC function we are using.  This value
        /// is the number of bytes.
        /// </summary>
        private const int hmacOutputLength = 64;

        /// <summary>
        /// The salt value used by this class.
        /// </summary>
        private static readonly byte[] salt = new byte[] 
        {
            0xe0, 0x66, 0x4c, 0xd5, 0xd7, 0x80, 0x49, 0x60, 
            0x1f, 0xe3, 0x1f, 0xb3, 0xf8, 0x8c, 0xd5, 0x4e, 
            0xe0, 0x38, 0x30, 0x2a, 0x87, 0xcf, 0x1c, 0x6c, 
            0x52, 0x51, 0x24, 0x3b, 0xe1, 0x26, 0x02, 0xa5, 
            0x44, 0x8a, 0xb1, 0xf6, 0xe0, 0x04, 0xce, 0xa0, 
            0xf6, 0x44, 0x89, 0xdd, 0xe4, 0x5a, 0x68, 0x67, 
            0x87, 0xa2, 0x6a, 0x11, 0xd9, 0x4d, 0x7f, 0x33, 
            0x3f, 0x59, 0x65, 0x4d, 0xfc, 0x93, 0xe0, 0x52
        }; 

        #endregion

        #region Shortcuts to the HKDF

        /// <summary>
        /// Creates an authentication token.
        /// </summary>
        /// <param name="authenticationServerKey">Authentication server's key.</param>
        /// <param name="userId">ID of the user to generate a token for.</param>
        /// <param name="period">Period for which this token is valid.</param>
        /// <returns>The authentication token.</returns>
        public static byte[] DeriveAuthenticationToken(byte[] authenticationServerKey, string userId, PeriodBase period, int tokenSize)
        {
            Contract.Requires(authenticationServerKey != null);
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Requires(period != null);
            Contract.Requires(tokenSize > 0);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == tokenSize);

            return HkdfExpand(
                GetCorrectLengthKey(authenticationServerKey),
                Encoding.UTF8.GetBytes("DeriveAuthenticationToken" + userId.ToUpperInvariant().Trim() + period.ToString()),
                tokenSize);
        }

        /// <summary>
        /// Creates a creator key.
        /// </summary>
        /// <param name="keyServerKey">Key server's key.</param>
        /// <param name="creatorId">ID of the user to generate a creator key for.</param>
        /// <param name="keyPeriod">Period for which this token is valid.</param>
        /// <returns>Creator key.</returns>
        public static byte[] DeriveCreatorKey(byte[] keyServerKey, string creatorId, PeriodBase period, int keySize)
        {
            Contract.Requires(keyServerKey != null);
            Contract.Requires(!string.IsNullOrEmpty(creatorId));
            Contract.Requires(period != null);
            Contract.Requires(keySize > 0);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == keySize);

            return HkdfExpand(
                GetCorrectLengthKey(keyServerKey),
                Encoding.UTF8.GetBytes("DeriveCreatorKey" + creatorId.ToUpperInvariant().Trim() + period.ToString()),
                keySize);
        }

        /// <summary>
        /// Creates a viewer key for the given creator.
        /// </summary>
        /// <param name="creatorKey">Creator's key.</param>
        /// <param name="viewerId">ID of viewer of the creator's sent content.</param>
        /// <returns>Viewer key.</returns>
        public static byte[] DeriveViewerKey(byte[] creatorKey, string viewerId, int keySize)
        {
            Contract.Requires(creatorKey != null);
            Contract.Requires(!string.IsNullOrEmpty(viewerId));
            Contract.Requires(keySize > 0);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == keySize);

            return HkdfExpand(
                GetCorrectLengthKey(creatorKey),
                Encoding.UTF8.GetBytes("DeriveViewerKey" + viewerId.ToUpperInvariant().Trim()),
                keySize);
        }

        /// <summary>
        /// Derives a set of keys based on the viewer key with the given nonce.
        /// </summary>
        /// <param name="viewerKey">Viewer key.</param>
        /// <param name="nonce">Nonce that adds randomness.</param>
        /// <param name="count">Total number of viewer key based keys desired.</param>
        /// <returns>Array with various viewer key based keys.</returns>
        public static byte[][] DeriveViewerKeyBasedKeys(byte[] viewerKey, byte[] nonce, int count, int keySize)
        {
            Contract.Requires(viewerKey != null);
            Contract.Requires(count >= 1);
            Contract.Requires(keySize > 0);
            Contract.Ensures(Contract.Result<byte[][]>() != null);
            Contract.Ensures(Contract.Result<byte[][]>().Length == count);
            Contract.Ensures(Contract.ForAll(Contract.Result<byte[][]>(), array => array != null));
            Contract.Ensures(Contract.ForAll(Contract.Result<byte[][]>(), array => array.Length == keySize));

            int[] keySizes = new int[count];
            for (int i = 0; i < count; i++)
                keySizes[i] = keySize;

            return DeriveViewerKeyBasedKeys(viewerKey, nonce, keySizes);
        }

        /// <summary>
        /// Derives a key based on the viewer key with the given nonce.
        /// </summary>
        /// <param name="viewerKey">Viewer key.</param>
        /// <param name="nonce">Nonce that adds randomness.</param>
        /// <param name="keySizes">The sizes of the keys desired.</param>
        /// <returns>Array with various viewer key based keys.</returns>
        public static byte[][] DeriveViewerKeyBasedKeys(byte[] viewerKey, byte[] nonce, int[] keySizes)
        {
            Contract.Requires(viewerKey != null);
            Contract.Requires(keySizes.Length >= 1);
            Contract.Requires(Contract.ForAll(keySizes, keySize => keySize >= 0));
            Contract.Ensures(Contract.Result<byte[][]>() != null);
            Contract.Ensures(Contract.Result<byte[][]>().Length == keySizes.Length);
            Contract.Ensures(Contract.ForAll(Contract.Result<byte[][]>(), array => array != null));
            Contract.Ensures(Contract.ForAll(0, keySizes.Length, i => Contract.Result<byte[][]>()[i].Length == keySizes[i]));

            byte[] keyMaterial = HkdfExpand(
                GetCorrectLengthKey(viewerKey),
                ConcatArrays(Encoding.UTF8.GetBytes("DeriveViewerBasedKeys"), nonce ?? new byte[0], new[] {(byte)keySizes.Length}),
                keySizes.Sum());

            // Break up the key, creating a key y keys of x length,
            // where y is the number of keys given by the length of
            // keySizes, and x is the key size provided by keySizes
            // at index i.
            byte[][] keys = new byte[keySizes.Length][];
            int position = 0;
            for (int i = 0; i < keys.Length; i++)
            {
                keys[i] = new byte[keySizes[i]];
                Array.Copy(keyMaterial, position, keys[i], 0, keySizes[i]);
                position += keySizes[i];
            }

            return keys;
        }

        /// <summary>
        /// Creates a key based on a password.
        /// </summary>
        /// <param name="password">Bytes of the password</param>
        /// <param name="keySize">Key size desired of the password.</param>
        /// <returns>Key based on the password.</returns>
        public static byte[] DerivePasswordKey(string userId, string password, int keySize)
        {
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Requires(password != null);
            Contract.Requires(keySize > 0);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == keySize);

            var keyMaterial = Encoding.UTF8.GetBytes(password);

            return HkdfExpand(
                GetCorrectLengthKey(keyMaterial),
                Encoding.UTF8.GetBytes("DerivePasswordKey" + userId.ToUpperInvariant().Trim()),
                keySize);
        }

        #endregion

        #region Actual HKDF functions

        /// <summary>
        /// Extract operation of the HKDF.  Used to add entropy to the input
        /// keyring material.  Not used currently because all the input keyring
        /// material we have already has maximum entropy.
        /// </summary>
        /// <param name="salt">Salt value for the HKDF.  Can be null, in which case
        /// a salt of all zeroes is used.  The salt is preferable the same lenfth as
        /// the HMAC output length.</param>
        /// <param name="inputKeyingMaterial">Key ring material to add entropy to.</param>
        /// <returns>A psuedorandom key with the required entropy.</returns>
        private static byte[] HkdfExtract(byte[] salt, byte[] inputKeyingMaterial)
        {
            Contract.Requires(inputKeyingMaterial != null);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == hmacOutputLength);

            using (HMAC hmac = GetHmacInstance(salt ?? new byte[hmacOutputLength]))
            {
                return hmac.ComputeHash(inputKeyingMaterial);
            }
        }

        /// <summary>
        /// Expands the psudeorandom key given to provide the material for any keys needed.
        /// </summary>
        /// <param name="pseudorandomKey">Psuedorandom key of at least hmacOutputLength.</param>
        /// <param name="info">Optional context and application specific information.</param>
        /// <param name="length">Length of output keying material.</param>
        /// <returns>Output keying material.</returns>
        private static byte[] HkdfExpand(byte[] pseudorandomKey, byte[] info, int length)
        {
            Contract.Requires(pseudorandomKey != null);
            Contract.Requires(pseudorandomKey.Length >= hmacOutputLength);
            Contract.Requires(length > 0);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == length);

            int numberOfRounds = (int)Math.Ceiling(((double)length) / hmacOutputLength);
            byte[] outputKeyMaterial = new byte[length];
            byte[] roundResult = new byte[0];

            using (HMAC hmac = GetHmacInstance(pseudorandomKey))
            {
                // Run all except the last round.
                for (int i = 1; i < numberOfRounds; i++)
                {
                    roundResult = hmac.ComputeHash(ConcatArrays(roundResult, info ?? new byte[0], new[] { (byte)i }));
                    Array.Copy(roundResult, 0, outputKeyMaterial, (i - 1) * hmacOutputLength, hmacOutputLength);
                }

                // Calculate the last round.
                roundResult = hmac.ComputeHash(ConcatArrays(roundResult, info ?? new byte[0], new[] { (byte)numberOfRounds }));
                if(length % hmacOutputLength == 0)
                    Array.Copy(roundResult, 0, outputKeyMaterial, (numberOfRounds - 1) * hmacOutputLength, hmacOutputLength);
                else
                    Array.Copy(roundResult, 0, outputKeyMaterial, (numberOfRounds - 1) * hmacOutputLength, length % hmacOutputLength);
            }

            return outputKeyMaterial;
        }

        /// <summary>
        /// Concatinates the given arrays together.
        /// </summary>
        /// <typeparam name="T">Type of the array.</typeparam>
        /// <param name="args">List of arrays to concatinate together.</param>
        /// <returns>Array with values from the other rays concatinated together.</returns>
        private static T[] ConcatArrays<T>(params T[][] args)
        {
            Contract.Requires(Contract.ForAll(args, arg => arg != null));
            Contract.Ensures(Contract.Result<T[]>() != null);

            T[] concatination = new T[args.Sum(arg => arg.Length)];

            int currentIndex = 0;
            foreach (T[] arg in args)
            {
                Array.Copy(arg, 0, concatination, currentIndex, arg.Length);
                currentIndex += arg.Length;
            }

            return concatination;
        }

        /// <summary>
        /// Gets a new instance of the HMAC used in the HKDF.
        /// </summary>
        /// <returns>HMAC used by the HKDF.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static HMAC GetHmacInstance(byte[] key)
        {
            Contract.Requires(key != null);
            Contract.Requires(key.Length == hmacOutputLength);
            Contract.Ensures(Contract.Result<HMAC>() != null);

            return new HMACSHA512(key);
        }

        /// <summary>
        /// Gets a key with the correct length for use in this HKDF,
        /// meaning it has size equal to our greater to hmacOutputLength.
        /// </summary>
        /// <param name="key">Key to get a key of correct size for.</param>
        /// <returns>Key with length >= hmacOutputLength.</returns>
        private static byte[] GetCorrectLengthKey(byte[] key)
        {
            Contract.Requires(key != null);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length >= hmacOutputLength);

            if (key.Length < hmacOutputLength)
                return HkdfExtract(salt, key);
            else
                return key;
        }

        #endregion

    }
}
