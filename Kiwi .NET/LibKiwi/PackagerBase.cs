﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Diagnostics.Contracts;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Security;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// The base class for all packages.
    /// </summary>
    /// <remarks>
    /// Packagers are responsible for either creating a single package, or unwraping the package.
    /// Each package wrapper or unwrapped should use its own instance of package.  Packagers should
    /// have one constructor that creates a Packager for wrapping that takes minimal information,
    /// and one that takes a package unwraps the basic attributes to make them accessible.  This
    /// information should be enough to get a viewer-key, which can then be used to unwrap a package.
    /// 
    /// By default when this class creates an encrypted data element it does so by
    /// treating EncryptionInfo as the IV.  If this is not the desired behaviour the sub-class
    /// should override Encrypt, Decrypt, AddMessage, UnwrapMessage, EncryptAttachment, 
    /// DecryptAttachment as these are the functions that make use of encryption that
    /// deal with encryption.
    /// 
    /// To remove the digest you need to change AddMessage, UnwrapMessage, EncryptAttachment, 
    /// DecryptAttachment, as these four methods all will by default make use of the hash
    /// algorithm provided by GetHashAlgorithm.  Alternatively you could make this return
    /// a custom hash method that just passes through what is written to it, and then override
    /// the base method and replace the empty hash that was generated.
    /// 
    /// The default operation on encryption use whatever SymetricAlgorithm is provided.  They
    /// treat EncryptionInfo of EncryptedData as the IV of the operation.  If other behavior is needed
    /// 
    /// All sizes of keys are in bytes.
    /// </remarks>
    [ContractClass(typeof(PackagerBaseContract))]
    public abstract class PackagerBase
    {

        #region Fields

        /// <summary>
        /// Random number generator for used in this packager.
        /// </summary>
        private static RandomNumberGenerator generator = RandomNumberGenerator.Create();

        /// <summary>
        /// Default data contract serializer.
        /// </summary>
        private DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Message));

        #endregion

        #region Constants

        /// <summary>
        /// Key to use for the attributes table for the creator id.
        /// </summary>
        protected const string CreatorIdAttributeKey = "CreatorId";

        /// <summary>
        /// Key to use for the attributes table for the creator key's period.
        /// </summary>
        protected const string TimestampAttributeKey = "Timestamp";

        /// <summary>
        /// Key to use for the attributes table for the key server URL.
        /// </summary>
        protected const string KeyServerUriAttributeKey = "KeyServerUri";

        /// <summary>
        /// Key to use for the attributes table for the nonce.
        /// </summary>
        protected const string MessageNonceAttributeKey = "Nonce";

        #endregion

        #region Abstract Properties

        /// <summary>
        /// Gets the size of keys used for encryption.
        /// </summary>
        public abstract int KeySize { get; }

        /// <summary>
        /// Gets the size of the IV.  0 indicates to not use an IV.
        /// </summary>
        protected abstract int IVSize { get; }

        /// <summary>
        /// Gets the size of the Nonce.  0 indicates to not use a nonce.
        /// </summary>
        protected abstract int NonceSize { get; }

        /// <summary>
        /// Gets the encryption algorithm to use for encryption.
        /// </summary>
        /// <remarks>
        /// If you are making the algorithm based on what is stored in
        /// EncryptedData's EncryptionInfo field then have this property 
        /// return null, to make sure their is no code that is using this.
        /// </remarks>
        protected abstract SymmetricAlgorithm EncryptionAlgorithm { get; }

        /// <summary>
        /// Get the serializer to use for serializing the message.
        /// </summary>
        protected virtual XmlObjectSerializer MessageSerializer { get { return serializer; } }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the creator id.
        /// </summary>
        public string CreatorId { get; set; }

        /// <summary>
        /// Gets or sets the creator key.  Only needed if wrapping the package.
        /// Not set when a package is unwrapped.
        /// </summary>
        public byte[] CreatorKey { get; set; }

        /// <summary>
        /// Gets or sets the date time for the message, used in requesting keys.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the key server uri.
        /// </summary>
        public Uri KeyServerUri { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public Message Message { get; set; }

        /// <summary>
        /// Gets or sets the viewers of the message.  The creator
        /// is considered a viewer.
        /// </summary>
        public ISet<string> ViewerIds { get; private set; }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Gets or sets the key used to encrypt the message.
        /// </summary>
        protected byte[] MessageKey { get; set; }

        /// <summary>
        /// Gets or sets the IV used to encrypt the message.
        /// </summary>
        protected byte[] MessageIV { get; set; }

        /// <summary>
        /// Gets or sets the digest of the message.
        /// </summary>
        protected byte[] MessageDigest { get; set; }

        /// <summary>
        /// Gets or sets the nonce used by the message.
        /// </summary>
        protected byte[] MessageNonce { get; set; }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Creates the packager.  Initializes MessageKey, MessageIV, MessageNonce.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        protected PackagerBase()
        {
            Contract.Ensures(MessageKey != null);
            Contract.Ensures(ViewerIds != null);

            ViewerIds = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            MessageKey = GetRandomBytes(KeySize);
            MessageIV = GetRandomBytes(IVSize);
            MessageNonce = GetRandomBytes(NonceSize);
        }

        #endregion

        #region Wrap Package

        /// <summary>
        /// Wraps the package, and returns a package with the contents encrypted
        /// and ready to be sent.
        /// </summary>
        /// <returns>Fully constructed package.</returns>
        public virtual Package Wrap()
        {
            Contract.Requires(MessageKey != null);
            Contract.Ensures(Contract.Result<Package>() != null);

            Package package = new Package();
            AddAttributes(package);
            AddMessage(package);
            AddViewers(package);
            return package;
        }

        /// <summary>
        /// Adds the packages attributes to the attribute dictionary.
        /// </summary>
        /// <remarks>The attributes added by the base class are: Creator id,
        /// key period for the creator key, and the Uri of the key server.</remarks>
        /// <param name="package">Package to add the attributes to.</param>
        protected virtual void AddAttributes(Package package)
        {
            Contract.Requires(package != null);

            package.Attributes[CreatorIdAttributeKey] = CreatorId;
            package.Attributes[TimestampAttributeKey] = Timestamp;
            package.Attributes[KeyServerUriAttributeKey] = KeyServerUri.ToString();

            if (MessageNonce != null)
                package.Attributes[MessageNonceAttributeKey] = Convert.ToBase64String(MessageNonce);
        }

        /// <summary>
        /// Adds the encrypted message and set the digest for the message.
        /// </summary>
        /// <remarks>The message is first zipped, then encrypted.  The digest is taken
        /// on the zipped pre-encrypted message.</remarks>
        /// <param name="package">Package to add the message to.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification=(@"Output is a memory stream so if their is an exception it will still be cleaned up by the garbage collector regardless
            if Dispose() is called or not."))]
        protected virtual void AddMessage(Package package)
        {
            Contract.Requires(package != null);
            Contract.Requires(MessageKey != null);
            Contract.Requires(Message != null);
            Contract.Ensures(package.EncryptedMessage != null);

            using (HashAlgorithm hashAlgorithm = GetHashAlgorithm())
            {
                MemoryStream output = new MemoryStream();
                CryptoStream encryptionStream = new CryptoStream(output, EncryptionAlgorithm.CreateEncryptor(MessageKey, MessageIV), CryptoStreamMode.Write);
                CryptoStream digestStream = new CryptoStream(encryptionStream, hashAlgorithm, CryptoStreamMode.Write);
                //GZipStream gZipStream = new GZipStream(digestStream, CompressionMode.Compress);

                MessageSerializer.WriteObject(digestStream, Message);
                digestStream.Close();
                //MessageSerializer.WriteObject(gZipStream, Message);
                //gZipStream.Close();

                package.EncryptedMessage = new EncryptedData(MessageIV, output.ToArray());
                MessageDigest = hashAlgorithm.Hash;
            }
        }

        /// <summary>
        /// Adds the viewers to the package.
        /// </summary>
        /// <remarks>
        /// Base implementation expects that MessageDigest is not null.
        /// </remarks>
        /// <param name="package">Package to add viewers to.</param>
        protected virtual void AddViewers(Package package)
        {
            Contract.Requires(package != null);
            Contract.Requires(CreatorId != null);
            Contract.Requires(CreatorKey != null);
            Contract.Requires(MessageKey != null);

            // Ensure that the viewers contain the creator.
            ViewerIds.Add(CreatorId);

            // Add each viewer to the package.
            foreach (var viewerId in ViewerIds)
            {
                // Get the viewer key.
                byte[] viewerKey = Hkdf.DeriveViewerKey(CreatorKey, viewerId, KeySize);

                // Use the four keys as such:
                // viewerKeyBasedKeys[0] = KEK for the message key.
                // viewerKeyBasedKeys[1] = Encryption key for the message digest.
                byte[][] viewerKeyBasedKeys = Hkdf.DeriveViewerKeyBasedKeys(viewerKey, MessageNonce, 2, KeySize);

                // Add the viewer to the package.
                package.Viewers[viewerId] =
                    new ViewerInfo(
                        Encrypt(viewerKeyBasedKeys[0], GetRandomBytes(IVSize), MessageKey),
                        Encrypt(viewerKeyBasedKeys[1], GetRandomBytes(IVSize), MessageDigest));
            }
        }

        /// <summary>
        /// Encrypts the attachment and returns the AttachmentInformation for the attachment
        /// that can be added to the message.
        /// </summary>
        /// <param name="input">Input stream of the attachment.</param>
        /// <param name="output">Output stream to write the encrypted attachment to.</param>
        /// <param name="originalName">Original name of the attachment.</param>
        /// <returns>Attachment info with the original name and digest of the attachment.</returns>
        /// <remarks>Closes both input and output.</remarks>
        public virtual AttachmentInfo EncryptAttachment(Stream input, Stream output, string originalName)
        {
            Contract.Requires(MessageKey != null);
            Contract.Requires(input != null);
            Contract.Requires(output != null);
            Contract.Ensures(Contract.Result<AttachmentInfo>() != null);
            Contract.Ensures(Contract.Result<AttachmentInfo>().OriginalName == originalName);

            using (HashAlgorithm hashAlgorithm = GetHashAlgorithm())
            {
                CryptoStream encryptionStream = new CryptoStream(output, EncryptionAlgorithm.CreateEncryptor(MessageKey, MessageIV), CryptoStreamMode.Write);
                CryptoStream digestStream = new CryptoStream(encryptionStream, hashAlgorithm, CryptoStreamMode.Write);

                input.CopyTo(digestStream);
                digestStream.Close();
                input.Close();

                return new AttachmentInfo(originalName, hashAlgorithm.Hash);
            }
        }
        
        #endregion

        #region Unwrap Package

        /// <summary>
        /// Unwraps a package and sets all the information.
        /// </summary>
        /// <param name="package">Package to unwrap.</param>
        /// <param name="viewerId">ID of the viewer reading this package.</param>
        /// <param name="viewerKey">Viewer's key for this package.</param>
        public virtual void Unwrap(Package package, string viewerId, byte[] viewerKey)
        {
            Contract.Requires(package != null);
            Contract.Requires(viewerKey != null);
            Contract.Requires(viewerId != null);
            Contract.Ensures(CreatorId != null);
            Contract.Ensures(KeyServerUri != null);
            Contract.Ensures(MessageKey != null);
            Contract.Ensures(Message != null);

            UnwrapAttributes(package);
            UnwrapViewers(package, viewerId, viewerKey);
            UnwrapMessage(package);
        }

        /// <summary>
        /// Unwraps the attributes from the package.
        /// </summary>
        /// <param name="package">Package to unwrap.</param>
        protected virtual void UnwrapAttributes(Package package)
        {
            Contract.Requires(package != null);
            Contract.Ensures(CreatorId != null);
            Contract.Ensures(KeyServerUri != null);

            CreatorId = (string)package.Attributes[CreatorIdAttributeKey];
            Timestamp = ((string)package.Attributes[TimestampAttributeKey]).ParseJsonDate();
            KeyServerUri = new Uri((string)package.Attributes[KeyServerUriAttributeKey]);

            if (package.Attributes.ContainsKey(MessageNonceAttributeKey))
                MessageNonce = Convert.FromBase64String((string)package.Attributes[MessageNonceAttributeKey]);
            else
                MessageNonce = null;
        }

        /// <summary>
        /// Unwraps the viewers. Retreives the MessageKey.
        /// </summary>
        /// <remarks>
        /// Base implementation ensures that MessageDigest is not null.
        /// </remarks>
        /// <param name="package">Package to unwrap.</param>
        /// <param name="viewerId">ID of the viewer reading this package.</param>
        /// <param name="viewerKey">Viewer's key for this package.</param>
        /// <exception cref="SecurityException">Thrown when the viewer id and key given
        /// cannot be used to read this message.</exception>
        protected virtual void UnwrapViewers(Package package, string viewerId, byte[] viewerKey)
        {
            Contract.Requires(package != null);
            Contract.Requires(viewerKey != null);
            Contract.Requires(viewerId != null);
            Contract.Ensures(MessageKey != null);

            // Use the four keys as such:
            // viewerKeyBasedKeys[0] = KEK for the message key.
            // viewerKeyBasedKeys[1] = Encryption key for the message digest.
            byte[][] viewerKeyBasedKeys = Hkdf.DeriveViewerKeyBasedKeys(viewerKey, MessageNonce, 2, KeySize);

            if (!package.Viewers.ContainsKey(viewerId))
                throw new SecurityException("The given user does not have view rights on this message.");

            ViewerInfo viewerInfo = package.Viewers[viewerId];
            MessageKey = Decrypt(viewerKeyBasedKeys[0], viewerInfo.MessageKey);
            MessageDigest = Decrypt(viewerKeyBasedKeys[1], viewerInfo.MessageDigest);
        }

        /// <summary>
        /// Unwraps the message from package.
        /// </summary>
        /// <remarks>
        /// The MessageKey, and MessageDigest should already been unwrapped.
        /// </remarks>
        /// <param name="package">Package to unwrap message from.</param>
        /// <exception cref="System.Security.CryptographicException">
        /// Thrown when the digest stored in the viewer list is not equivalent to the calculated digest.
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification=(@"Input is a memory stream so if their is an exception it will still be cleaned up by the garbage collector regardless
            if Dispose() is called or not."))]
        protected virtual void UnwrapMessage(Package package)
        {
            Contract.Requires(package != null);
            Contract.Requires(MessageKey != null);
            Contract.Ensures(Message != null);

            MessageIV = package.EncryptedMessage.EncryptionInfo;

            using (HashAlgorithm hashAlgorithm = GetHashAlgorithm())
            {
                MemoryStream input = new MemoryStream(package.EncryptedMessage.Value);
                CryptoStream decryptionStream = new CryptoStream(input, EncryptionAlgorithm.CreateDecryptor(MessageKey, MessageIV), CryptoStreamMode.Read);
                CryptoStream digestStream = new CryptoStream(decryptionStream, hashAlgorithm, CryptoStreamMode.Read);
                //GZipStream gZipStream = new GZipStream(digestStream, CompressionMode.Decompress);

                Message = (Message)MessageSerializer.ReadObject(digestStream);
                //Message = (Message)MessageSerializer.ReadObject(gZipStream);
                digestStream.Close();

                // Check digest equality.
                if (!MessageDigest.SequenceEqual(hashAlgorithm.Hash))
                    throw new CryptographicException("Digest mis-match.  The message has been changed.");
            }
        }

        /// <summary>
        /// Dercypts the given attachment, and checks the digest.
        /// </summary>
        /// <param name="input">Input stream for the encrypted attachment.</param>
        /// <param name="output">Output stream to write the unencrypted attachment to.</param>
        /// <param name="info">Attachment info to check digest against.</param>
        /// <exception cref="System.Security.CryptographicException">
        /// Thrown when the digest stored in the attachment info is not equivalent to the calculated digest.
        /// </exception>
        /// <remarks>Closes both input and output.</remarks>
        public virtual void DecryptAttachment(Stream input, Stream output, AttachmentInfo info)
        {
            Contract.Requires(MessageKey != null);
            Contract.Requires(input != null);
            Contract.Requires(output != null);

            using (HashAlgorithm hashAlgorithm = GetHashAlgorithm())
            {
                CryptoStream decryptionStream = new CryptoStream(input, EncryptionAlgorithm.CreateDecryptor(MessageKey, MessageIV), CryptoStreamMode.Read);
                CryptoStream hashStream = new CryptoStream(decryptionStream, hashAlgorithm, CryptoStreamMode.Read);

                hashStream.CopyTo(output);
                output.Close();
                input.Close();

                // Check digest equality.
                if (!info.Digest.SequenceEqual(hashAlgorithm.Hash))
                    throw new CryptographicException("Digest mis-match.  The attachment has been changed.");
            }
        }

        #endregion

        #region Cryptopgraphy Methods

        /// <summary>
        /// Encrypts the given data with the given key, and returns a properly
        /// formatted EncryptedData.
        /// </summary>
        /// <param name="key">Key to use for encryption.</param>
        /// <param name="data">Data to encrypt.</param>
        /// <returns>EncryptedData object that can be decrypted by
        /// the corresponding Decrypt method.</returns>
        [Pure]
        protected virtual EncryptedData Encrypt(byte[] key, byte[] iv, byte[] data)
        {
            Contract.Requires(key != null);
            Contract.Requires(data != null);
            Contract.Ensures(Contract.Result<EncryptedData>() != null);

            return new EncryptedData(iv, EncryptionAlgorithm.CreateEncryptor(key, iv).TransformFinalBlock(data, 0, data.Length));
        }

        /// <summary>
        /// Decrypts the given data with the given key, and returns a properly
        /// formatted EncryptedData.
        /// </summary>
        /// <param name="key">Key to use for decryption.</param>
        /// <param name="data">Data to decrypt.</param>
        /// <returns>byte[] with the contents of data decrypted.</returns>
        [Pure]
        protected virtual byte[] Decrypt(byte[] key, EncryptedData data)
        {
            Contract.Requires(key != null);
            Contract.Requires(data != null);
            Contract.Ensures(Contract.Result<byte[]>() != null);

            byte[] iv = data.EncryptionInfo;
            return EncryptionAlgorithm.CreateDecryptor(key, iv).TransformFinalBlock(data.Value, 0, data.Value.Length);
        }

        /// <summary>
        /// Gets the algorithm used for digest generation in this package.
        /// </summary>
        /// <returns>The digest algorithm used to generate digests.</returns>
        [Pure]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected abstract HashAlgorithm GetHashAlgorithm();

        #endregion

        #region Static Utility Methods

        /// <summary>
        /// Gets random bytes.  Returns null if the size is 0.
        /// </summary>
        /// <param name="size">Number of bytes to return.</param>
        /// <returns>byte[] with the given number of bytes, or null if size is 0.</returns>
        [Pure]
        protected static byte[] GetRandomBytes(int size)
        {
            Contract.Requires(size >= 0);
            Contract.Ensures(size == 0 ? Contract.Result<byte[]>() == null : Contract.Result<byte[]>().Length == size);

            if (size == 0)
                return null;

            byte[] value = new byte[size];
            generator.GetBytes(value);
            return value;
        }

        #endregion

    }

    #region Contract Class

    /// <summary>
    /// Contract class to check abstract methods in PackagerBase.
    /// </summary>
    [ContractClassFor(typeof(PackagerBase))]
    abstract class PackagerBaseContract : PackagerBase
    {

        /// <summary>
        /// Gets the algorithm used for digest generation in this package.
        /// </summary>
        /// <returns>The digest algorithm used to generate digests.</returns>
        [Pure]
        protected override HashAlgorithm GetHashAlgorithm()
        {
            Contract.Ensures(Contract.Result<HashAlgorithm>() != null);

            return default(HashAlgorithm);
        }

        /// <summary>
        /// Gets the size of keys used for encryption.
        /// </summary>
        public override int KeySize
        {
            get 
            { 
                Contract.Ensures(Contract.Result<int>() > 0);
                return default(int);
            }
        }
    }

    #endregion

}
