﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// This is the package that is sent between parties for secure
    /// communication using Kiwi.
    /// </summary>
    [DataContract]
    public sealed class Package
    {

        #region Properties

        /// <summary>
        /// Gets the viewer dictionary.  Returns null if the package was created without one.
        /// </summary>
        public IDictionary<string, ViewerInfo> Viewers { get; private set; }

        /// <summary>
        /// DataMember in place of viewers.  Used to keep viewers
        /// from being serialized if it is empty.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DataMember(Name = "Viewers", EmitDefaultValue = false)]
        private IDictionary<string, ViewerInfo> ViewersSerialized
        {
            get { return Viewers.Count == 0 ? null : Viewers; }
            set { Viewers = value; }
        }

        /// <summary>
        /// Gets the attributes dictionary.  Returns null if the package was created without one.
        /// </summary>
        public IDictionary<string, object> Attributes { get; private set; }

        /// <summary>
        /// DataMember in place of attributes.  Used to keep attributes
        /// from being serialized if it is empty.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DataMember(Name = "Attributes", EmitDefaultValue = false)]
        private IDictionary<string, object> AttributesSerialized
        {
            get { return Attributes.Count == 0 ? null : Attributes; }
            set { Attributes = value; }
        }

        /// <summary>
        /// Gets or sets the encrypted message.
        /// </summary>
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        public EncryptedData EncryptedMessage { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a package.
        /// </summary>
        public Package()
        {
            Viewers = new Dictionary<string, ViewerInfo>();
            Attributes = new Dictionary<string, object>();
        }

        #endregion

    }

}
