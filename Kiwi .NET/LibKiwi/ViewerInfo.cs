﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics.Contracts;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Information for viewers to decrypt and verify a message.
    /// </summary>
    [DataContract]
    public sealed class ViewerInfo
    {

        #region Property

        /// <summary>
        /// Gets the encrypted message key.
        /// </summary>
        [DataMember(EmitDefaultValue = false, IsRequired=true)]
        public EncryptedData MessageKey { get; private set; }

        /// <summary>
        /// Gets the message digest.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public EncryptedData MessageDigest { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a viewer.
        /// </summary>
        /// <param name="messageKey">Encrypted message key for this viewer.</param>
        /// <param name="messageDigest">Encrypted message digest for this viewer.</param>
        public ViewerInfo(EncryptedData messageKey, EncryptedData messageDigest)
        {
            Contract.Requires(messageKey != null);
            Contract.Ensures(MessageKey == messageKey);
            Contract.Ensures(MessageDigest == messageDigest);

            MessageKey = messageKey;
            MessageDigest = messageDigest;
        }

        #endregion

        #region Invariants

        /// <summary>
        /// Ensures that the invariants are maintained.
        /// </summary>
        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(MessageKey != null);
        }

        #endregion

    }

}
