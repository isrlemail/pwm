﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.Contracts;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Base class for all messages.
    /// </summary>
    [DataContract]
    public sealed class Message
    {

        #region Properties

        /// <summary>
        /// Gets or sets the object that is the contents of this object.
        /// </summary>
        [DataMember(EmitDefaultValue = false, IsRequired = true)]
        public object Contents { get; private set; }

        /// <summary>
        /// Gets the dictionary of attributes authenticated by the message digest.
        /// </summary>
        public IDictionary<string, object> AuthenticatedAttributes { get; private set; }

        /// <summary>
        /// DataMember in place of attributes.  Used to keep attachments
        /// from being serialized if it is empty.
        /// </summary>
        [DataMember(Name = "Attributes", EmitDefaultValue = false)]
        private IDictionary<string, object> AuthenticatedAttributesSerialized
        {
            get { return AuthenticatedAttributes.Count == 0 ? null : AuthenticatedAttributes; }
            set { AuthenticatedAttributes = value; }
        }

        /// <summary>
        /// Gets the dictionary of attachments info.
        /// </summary>
        /// <remarks>
        /// The key is the name of the attachment that was transmited with the package.
        /// The attachment info will contain the original name of the file, along with
        /// an optional digest.
        /// 
        /// Use a packager to generate a digest and encrypt the actual attachment.
        /// </remarks>
        public IDictionary<string, AttachmentInfo> Attachments { get; private set; }

        /// <summary>
        /// DataMember in place of attachments.  Used to keep attachments
        /// from being serialized if it is empty.
        /// </summary>
        [DataMember(Name = "Attachments", EmitDefaultValue = false)]
        private IDictionary<string, AttachmentInfo> AttachmentsSerialized
        {
            get { return Attachments.Count == 0 ? null : Attachments; }
            set { Attachments = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a message.
        /// </summary>
        /// <param name="contents">Contents of the message.</param>
        /// <param name="hasAttachments">Whether or not this message will have attachments.</param>
        public Message(object contents)
        {
            Contract.Requires(contents != null);

            Contents = contents;
            Attachments = new Dictionary<string, AttachmentInfo>();
            AuthenticatedAttributes = new Dictionary<string, object>();
        }

        #endregion

    }
}
