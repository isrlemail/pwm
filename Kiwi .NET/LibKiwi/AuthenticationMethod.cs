﻿using System.Runtime.Serialization;
using System;

namespace Kiwi.LibKiwi
{
	/// <summary>
	/// Methods of getting an authentication token.
	/// </summary>
	[DataContract]
	public enum AuthenticationMethod
	{

		[EnumMember]
		None = 0,

		[EnumMember]
		Saw = 1,

		[EnumMember]
		Password = 2,

		[EnumMember]
		Other = 3

	}
}
