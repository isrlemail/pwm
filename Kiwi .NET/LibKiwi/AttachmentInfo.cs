﻿using System;
using System.Runtime.Serialization;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// This is an attachment to a message.  Attachments are stored externally,
    /// but information about them is stored in the message.
    /// </summary>
    [DataContract]
    public sealed class AttachmentInfo
    {

        #region Properties

        /// <summary>
        /// Gets the name of this attachment.
        /// </summary>
        [DataMember(Name = "Name", EmitDefaultValue = false)]
        public string OriginalName { get; private set; }

        /// <summary>
        /// Gets the digest for this attachment.
        /// </summary>
        public byte[] Digest { get; private set; }

        /// <summary>
        /// Serialized backing for Digest.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DataMember(Name = "Digest", EmitDefaultValue = false)]
        private string DigestSerialized
        {
            get { return Digest == null || Digest.Length == 0 ? null : Convert.ToBase64String(Digest); }
            set { Digest = string.IsNullOrEmpty(value) ? null : Convert.FromBase64String(value); }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a message attachment.
        /// </summary>
        /// <param name="originalName">Original name of the message before being encrypted.</param>
        /// <param name="digest">Digest of the attachment contents.</param>
        public AttachmentInfo(string originalName, byte[] digest)
        {
            OriginalName = originalName;
            Digest = digest;
        }

        #endregion

    }
}
