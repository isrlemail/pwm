﻿using System;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Various extensions to classes.
    /// </summary>
    [Pure]
    public static class Extensions
    {

        #region Byte Operations

        /// <summary>
        /// Takes the exlusive or of the two byte arrays.
        /// </summary>
        /// <param name="lhs">Left hand set of bytes.</param>
        /// <param name="rhs">Right hand set of bytes.</param>
        /// <returns></returns>
        public static byte[] Xor(this byte[] lhs, byte[] rhs)
        {
            Contract.Requires(lhs != null);
            Contract.Requires(rhs != null);
            Contract.Requires(lhs.Length == rhs.Length);
            Contract.Ensures(Contract.Result<byte[]>() != null);
            Contract.Ensures(Contract.Result<byte[]>().Length == lhs.Length);

            // Create the value array the bytewise xor of the two arrays.
            byte[] value = new byte[lhs.Length];
            for (int i = 0; i < lhs.Length; i++)
                value[i] = (byte)(lhs[i] ^ rhs[i]);
            return value;
        }

        #endregion

        #region Datetime parsing

        /// <summary>
        /// Regex for parsing JSON date time streams.
        /// </summary>
        private static Regex dateTimeRegex = new Regex(@"^\/Date\((?<offset>-?\d+)(?<timezone>-\d{4})?\)\/$", RegexOptions.IgnoreCase);

        /// <summary>
        /// DateTime for the unix epoch.
        /// </summary>
        private static DateTime Epoch = new DateTime(1970, 1, 1);

        /// <summary>
        /// Parses the given string as a JSON formatted date.
        /// </summary>
        /// <param name="value">Value to parse.</param>
        /// <returns>DateTime.</returns>
        public static DateTime ParseJsonDate(this string value)
        {
            var match = dateTimeRegex.Match(value);
            if (match.Success)
            {
                DateTime parsedDateTime = Epoch.AddMilliseconds(double.Parse(match.Groups["offset"].Value, CultureInfo.InvariantCulture));
                //if(match.Groups["timezone"].Success)
                //    parsedDateTime = parsedDateTime.AddHours(double.Parse(match.Groups["timezone"].Value, CultureInfo.InvariantCulture) / 100.0);
                return parsedDateTime;
            }
            else
                return DateTime.Parse(value);
        }

        #endregion

    }

}
