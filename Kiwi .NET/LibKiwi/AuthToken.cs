﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Represents either a token, either split or whole.
    /// </summary>
    [DataContract]
    public sealed class AuthToken
    {

        #region Constants

        /// <summary>
        /// The size of the password keys.
        /// </summary>
        private const int PasswordKeySize = 32;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the UserId this token is for.
        /// </summary>
        [DataMember(EmitDefaultValue = false, IsRequired = true)]
        public string UserId { get; private set; }

        /// <summary>
        /// Gets the period for which the token is valid.
        /// </summary>
        [DataMember(EmitDefaultValue = false, IsRequired = true)]
        public PeriodBase Period { get; private set; }

        /// <summary>
        /// Gets the actual bytes of the token.
        /// </summary>
        public byte[] Value { get; private set; }

        /// <summary>
        /// Base64 encoded string in place of value.
        /// </summary>
        [DataMember(Name = "Value", EmitDefaultValue = false, IsRequired = true)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private string ValueSerialized
        {
            get { return Convert.ToBase64String(Value); }
            set { Value = Convert.FromBase64String(value); }
        }

        /// <summary>
        /// Gets the method used for this token.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public AuthenticationMethod Method { get; private set; }

        /// <summary>
        /// Gets the transaction id.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string TransactionId { get; private set; }

        /// <summary>
        /// Gets whether or not the token is completed.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public bool IsTokenComplete { get; private set; }

        /// <summary>
        /// Gets the Uri of the auth server that gave this token.
        /// </summary>
        [DataMember(EmitDefaultValue = false, IsRequired = true)]
        public Uri AuthServerUri { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates an auth token.
        /// </summary>
        /// <param name="userId">Id of the user the auth token is for.</param>
        /// <param name="tokenPeriod">Period the token is valid for.</param>
        /// <param name="value">Value of the token.</param>
        /// <param name="authServerUri">Uri of the auth server that issued the token.</param>
        /// <param name="transactionId">Id of the transaction for this token.</param>
        /// <param name="isTokenComplete">Whether the token is a complete token.</param>
        public AuthToken(string userId, PeriodBase tokenPeriod, byte[] value, Uri authServerUri, AuthenticationMethod method,
            string transactionId = null, bool isTokenComplete = true)
        {
            Contract.Requires(!string.IsNullOrEmpty(userId));
            Contract.Requires(tokenPeriod != null);
            Contract.Requires(value != null);
            Contract.Requires(authServerUri != null);
            Contract.Ensures(UserId == userId);
            Contract.Ensures(Period == tokenPeriod);
            Contract.Ensures(Value == value);
            Contract.Ensures(Method == method);
            Contract.Ensures(AuthServerUri.Equals(authServerUri));
            Contract.Ensures(TransactionId == transactionId);
            Contract.Ensures(IsTokenComplete == isTokenComplete);

            UserId = userId;
            Period = tokenPeriod;
            Value = value;
            Method = method;
            AuthServerUri = new Uri(authServerUri.ToString());
            TransactionId = transactionId;
            IsTokenComplete = isTokenComplete;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the key bassed on the password for password protection of the token.
        /// </summary>
        /// <param name="password">The password to get a key for.</param>
        /// <returns>The key used in ApplyPassword().</returns>
        public byte[] GetPasswordKey(string password)
        {
            return Hkdf.DerivePasswordKey(UserId, password, PasswordKeySize);
        }

        /// <summary>
        /// Encrypts the token with a key derived from a password.
        /// </summary>
        /// <param name="key">The key to use to add the password.</param>
        public void EncryptWithPasswordKey(byte[] key)
        {
            Contract.Requires(key != null);
            Contract.Ensures(!IsTokenComplete);

            using (Aes aes = Aes.Create())
            {
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                using (ICryptoTransform transform = aes.CreateEncryptor(key, new byte[16]))
                {
                    Value = transform.TransformFinalBlock(Value, 0, Value.Length);
                }
            }

            IsTokenComplete = false;
        }

        /// <summary>
        /// Decrypts the token with a key derived from the password.
        /// </summary>
        /// <param name="key">The key to use to remove the password.</param>
        public void DecryptWithPasswordKey(byte[] key)
        {
            Contract.Requires(key != null);
            Contract.Ensures(IsTokenComplete);

            using (Aes aes = Aes.Create())
            {
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                using (ICryptoTransform transform = aes.CreateDecryptor(key, new byte[16]))
                {
                    Value = transform.TransformFinalBlock(Value, 0, Value.Length);
                }
            }

            IsTokenComplete = true;
        }

        /// <summary>
        /// Combines the two parts of the same token.
        /// </summary>
        /// <param name="token">Token to combine with this one.</param>
        /// <param name="isTokenComplete">Whether the newly combined token is complete.</param>
        /// <returns>Auth token with the two tokens combined.</returns>
        public void Combine(byte[] token, bool isTokenComplete)
        {
            Contract.Requires(token != null);
            Contract.Requires(token.Length == Value.Length);
            Contract.Requires(!IsTokenComplete);
            Contract.Ensures(IsTokenComplete == isTokenComplete);

            Value = Value.Xor(token);
            IsTokenComplete = isTokenComplete;
        }

        #endregion

    }

}
