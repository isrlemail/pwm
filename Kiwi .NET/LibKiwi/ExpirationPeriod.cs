﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics.Contracts;
using System.Globalization;

namespace Kiwi.LibKiwi
{
	/// <summary>
	/// Key period that is valid until a specified date
	/// </summary>
	[DataContract]
	public sealed class ExpirationPeriod : PeriodBase
	{
		/// <summary>
		/// Creates a key period with the given expiration date
		/// </summary>
		/// <param name="expirationDate"></param>
		public ExpirationPeriod(DateTime expirationDate)
		{
			End = expirationDate;
		}

		/// <summary>
		/// Gets whether the period is valid for the given time.
		/// </summary>
		/// <param name="time"></param>
		/// <returns></returns>
		[Pure]
		public override bool ValidForDateTime(DateTime time)
		{
			return time < End;
		}

		/// <summary>
		/// Returns a description of this key period base
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return End.Value.ToString("u", CultureInfo.InvariantCulture);
		}
	}
}
