﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO.Compression;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Diagnostics.Contracts;
using System.Security;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Packager for email messages.
    /// </summary>
    public sealed class EmailPackager : PackagerBase
    {

        #region Fields

        /// <summary>
        /// Fieled that backs EncryptionAlgorithm.
        /// </summary>
        private Aes aes = Aes.Create();

        #endregion

        #region Constants

        /// <summary>
        /// Key for storing whether the message is an HTML message.
        /// </summary>
        private const string IsHtmlMessageKey = "IsHTMLMessage";

        #endregion

        #region Overridden Properties

        /// <summary>
        /// Gets the size of keys used for encryption.
        /// </summary>
        public override int KeySize { get { return 32; } }

        /// <summary>
        /// Gets the size of the IV.  0 indicates to not use an IV.
        /// </summary>
        protected override int IVSize { get { return 16; } }

        /// <summary>
        /// Gets the size of the Nonce.  0 indicates to not use a nonce.
        /// </summary>
        protected override int NonceSize { get { return 32; } }

        /// <summary>
        /// Gets the encryption algorithm to use for encryption.
        /// </summary>
        protected override SymmetricAlgorithm EncryptionAlgorithm { get { return aes; } }

        #endregion

        #region Static properties

        /// <summary>
        /// Gets the size of keys used for encryption.
        /// </summary>
        public static int PackageKeySize { get { return 32; } }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates the e-mail packager.
        /// </summary>
        public EmailPackager() : base()
        {
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
        }

        /// <summary>
        /// Create an e-mail packager from the given package, unwrapping based on
        /// the viewer and their key.
        /// </summary>
        /// <param name="package">Package to unwrap.</param>
        /// <param name="viewerId">ID of the viewer reading this package.</param>
        /// <param name="viewerKey">Viewer's key for this package.</param>
        public EmailPackager(Package package)
        {
            Contract.Requires(package != null);
            Contract.Ensures(CreatorId != null);
            Contract.Ensures(KeyServerUri != null);

            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            UnwrapAttributes(package);
        }

        #endregion

        #region Overridden Wrap Methods

        /// <summary>
        /// Adds the viewers to the package.  Viewers are put into the list
        /// with an obifuscated user name.
        /// </summary>
        /// <param name="package">Package to add viewers to.</param>
        protected override void AddViewers(Package package)
        {
            // Ensure that the viewers contain the creator.
            ViewerIds.Add(CreatorId);

            // Add each viewer to the package.
            foreach (var viewerId in ViewerIds)
            {
                // Get the viewer key.
                byte[] viewerKey = Hkdf.DeriveViewerKey(CreatorKey, viewerId, KeySize);

                // Use the four keys as such:
                // viewerKeyBasedKeys[0] = Key on the viewer dictionary in the package.
                // viewerKeyBasedKeys[1] = KEK for the message key.
                // viewerKeyBasedKeys[2] = Encryption key for the message digest.
                byte[][] viewerKeyBasedKeys = Hkdf.DeriveViewerKeyBasedKeys(viewerKey, MessageNonce, 3, KeySize);

                // Add the viewer to the package.
                package.Viewers[Convert.ToBase64String(viewerKeyBasedKeys[0])] =
                    new ViewerInfo(
                        Encrypt(viewerKeyBasedKeys[1], GetRandomBytes(IVSize), MessageKey),
                        Encrypt(viewerKeyBasedKeys[2], GetRandomBytes(IVSize), MessageDigest));
            }
        }

        #endregion

        #region Overridden Unwrap Methods

        /// <summary>
        /// Unwraps the viewers.
        /// </summary>
        /// <remarks>
        /// Nonce must already be set.
        /// 
        /// Retreiving the MessageKey and digest.
        /// </remarks>
        /// <param name="package">Package to unwrap.</param>
        /// <param name="viewerId">ID of the viewer reading this package.</param>
        /// <param name="viewerKey">Viewer's key for this package.</param>
        /// <exception cref="SecurityException">Thrown when the viewer id and key given
        /// cannot be used to read this message.</exception>
        protected override void UnwrapViewers(Package package, string viewerId, byte[] viewerKey)
        {
            Contract.Ensures(MessageDigest != null);

            // Use the four keys as such:
            // viewerKeyBasedKeys[0] = Key on the viewer dictionary in the package.
            // viewerKeyBasedKeys[1] = KEK for the message key.
            // viewerKeyBasedKeys[2] = Encryption key for the message digest.
            byte[][] viewerKeyBasedKeys = Hkdf.DeriveViewerKeyBasedKeys(viewerKey, MessageNonce, 3, KeySize);
            string viewerIdKey = Convert.ToBase64String(viewerKeyBasedKeys[0]);

            if (!package.Viewers.ContainsKey(viewerIdKey))
                throw new SecurityException("The given user does not have view rights on this message.");

            ViewerInfo viewerInfo = package.Viewers[viewerIdKey];
            MessageKey = Decrypt(viewerKeyBasedKeys[1], viewerInfo.MessageKey);
            MessageDigest = Decrypt(viewerKeyBasedKeys[2], viewerInfo.MessageDigest);
        }

        #endregion

        #region Overridden Cryptopgraphy Methods

        /// <summary>
        /// Gets the algorithm used for digest generation in this package.
        /// </summary>
        /// <returns>The digest algorithm used to generate digests.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override HashAlgorithm GetHashAlgorithm()
        {
            return SHA512.Create();
        }

        #endregion

    }
}
