﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Diagnostics.Contracts;
using System.IO;
using System.IO.Compression;

namespace Kiwi.LibKiwi
{

    public sealed class P2PPackager : PackagerBase
    {

        #region Fields

        /// <summary>
        /// Fieled that backs EncryptionAlgorithm.
        /// </summary>
        private Aes aes = Aes.Create();

        #endregion

        #region Overridden Properties

        /// <summary>
        /// Gets the size of keys used for encryption.
        /// </summary>
        public override int KeySize { get { return 16; } }

        /// <summary>
        /// Gets the size of the IV.  0 indicates to not use an IV.
        /// </summary>
        protected override int IVSize { get { return 0; } }

        /// <summary>
        /// Gets the size of the Nonce.  0 indicates to not use a nonce.
        /// </summary>
        protected override int NonceSize { get { return 0; } }

        /// <summary>
        /// Gets the encryption algorithm to use for encryption.
        /// </summary>
        protected override SymmetricAlgorithm EncryptionAlgorithm { get { return aes; } }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates the e-mail packager.
        /// </summary>
        public P2PPackager() : base()
        {
            aes.Mode = CipherMode.ECB;
            aes.Padding = PaddingMode.PKCS7;
        }

        ///// <summary>
        ///// Create an e-mail packager from the given package, unwrapping based on
        ///// the viewer and their key.
        ///// </summary>
        ///// <param name="package">Package to unwrap.</param>
        ///// <param name="viewerId">ID of the viewer reading this package.</param>
        ///// <param name="viewerKey">Viewer's key for this package.</param>
        //public EmailPackager(Package package)
        //{
        //    Contract.Requires(package != null);
        //    Contract.Ensures(CreatorId != null);
        //    Contract.Ensures(CreatorKeyPeriod != null);
        //    Contract.Ensures(KeyServerUri != null);

        //    UnwrapAttributes(package);
        //}

        #endregion

        #region Overridden Wrap Methods

        public override Package Wrap()
        {
            Package package = new Package();

            package.Attributes[KeyServerUriAttributeKey] = KeyServerUri.ToString();
            AddMessage(package);

            byte[] viewerKey = Hkdf.DeriveViewerKey(CreatorKey, ViewerIds.First(item => item != CreatorId), KeySize);

            // Use the four keys as such:
            // viewerKeyBasedKeys[0] = KEK for the message key.
            // viewerKeyBasedKeys[1] = Encryption key for the message digest.
            byte[][] viewerKeyBasedKeys = Hkdf.DeriveViewerKeyBasedKeys(viewerKey, MessageNonce, 2, KeySize);

            // Add the viewer to the package.
            package.Attributes["Key"] = Encrypt(viewerKeyBasedKeys[0], GetRandomBytes(IVSize), MessageKey);
            //package.Attributes["Digest"] = Encrypt(viewerKeyBasedKeys[1], GetRandomBytes(IVSize), MessageDigest);

            return package;
        }

        /// <summary>
        /// Adds the encrypted message and set the digest for the message.
        /// </summary>
        /// <remarks>The message is first zipped, then encrypted.  The digest is taken
        /// on the zipped pre-encrypted message.</remarks>
        /// <param name="package">Package to add the message to.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification = (@"Output is a memory stream so if their is an exception it will still be cleaned up by the garbage collector regardless
            if Dispose() is called or not."))]
        protected override void AddMessage(Package package)
        {
            
            using (HashAlgorithm hashAlgorithm = GetHashAlgorithm())
            {
                MemoryStream output = new MemoryStream();
                CryptoStream encryptionStream = new CryptoStream(output, EncryptionAlgorithm.CreateEncryptor(MessageKey, MessageIV), CryptoStreamMode.Write);
                CryptoStream digestStream = new CryptoStream(encryptionStream, hashAlgorithm, CryptoStreamMode.Write);
                //GZipStream gZipStream = new GZipStream(digestStream, CompressionMode.Compress);
                Stream gZipStream = digestStream;

                MessageSerializer.WriteObject(gZipStream, Message);
                gZipStream.Close();

                package.EncryptedMessage = new EncryptedData(MessageIV, output.ToArray());
                MessageDigest = hashAlgorithm.Hash;
            }
        }

        #endregion

        #region Overridden Cryptopgraphy Methods

        /// <summary>
        /// Gets the algorithm used for digest generation in this package.
        /// </summary>
        /// <returns>The digest algorithm used to generate digests.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override HashAlgorithm GetHashAlgorithm()
        {
            return SHA1.Create();
        }

        #endregion

    }

}
