﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Class that handles saw operations.
    /// </summary>
    public static class Saw
    {

        /// <summary>
        /// Get a saw subject for the given subject.
        /// </summary>
        /// <param name="token">Token to create a subject for.</param>
        /// <returns>Subject line for saw token.</returns>
        public static string GetSawSubject(string transactionId, byte[] value)
        {
            Contract.Requires(transactionId != null);
            Contract.Requires(value != null);
            Contract.Ensures(!string.IsNullOrEmpty(Contract.Result<string>()));

            return string.Format(CultureInfo.InvariantCulture, "[AuthToken]{0}&{1}", transactionId, Convert.ToBase64String(value));
        }

        /// <summary>
        /// Parses the transaction ID and token from a saw email subject.
        /// </summary>
        /// <param name="subject">Subject to parse.</param>
        /// <returns>Null if not found, else the transaction ID and token from the message.</returns>
        public static Tuple<string, byte[]> GetSawPartsFromSubject(string subject)
        {
            Contract.Requires(subject != null);

            Regex regex = new Regex(@"\[AuthToken\](?<TransactionId>[^&]+)&(?<Token>[^\s]+)");
            Match match = regex.Match(subject);

            if (match.Success)
                return new Tuple<string, byte[]>(match.Groups["TransactionId"].Value,
                    Convert.FromBase64String(match.Groups["Token"].Value));
            else
                return null;
        }


    }
}
