﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;
using System.Diagnostics.Contracts;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Represents the time for which any given key is valid.
    /// </summary>
    [DataContract, KnownType(typeof(MonthlyPeriod)), KnownType(typeof(ExpirationPeriod))]
    public abstract class PeriodBase : IEquatable<PeriodBase>
    {

        #region Properties

        /// <summary>
        /// Gets or sets the start time for the period.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        protected DateTime? Start { get; set; }

        /// <summary>
        /// Gets or sets the end time for the period.
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        protected DateTime? End { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Gets whether the period is valid at the current moment.
        /// </summary>
        /// <returns>Whether the period is currently valid.</returns>
        [Pure]
        public bool IsCurrent()
        {
            return ValidForDateTime(DateTime.Now);
        }

        /// <summary>
        /// Gets whether the period is valid for the given time.
        /// </summary>
        /// <returns>Whether the period is valid for the given time.</returns>
        [Pure]
        public virtual bool ValidForDateTime(DateTime time)
        {
            return time >= Start && time <= End;
        }

        #endregion

        #region ToString

        /// <summary>
        /// Returns a description of this key period base.
        /// </summary>
        /// <returns></returns>
        [Pure]
        public override string ToString()
        {
            Contract.Ensures(Contract.Result<string>() != null);
            Contract.Ensures(Contract.Result<string>().Length > 0);

            return string.Format(
                CultureInfo.InvariantCulture,
                "{0} | {1} - {2}",
                GetType().ToString(),
                Start.HasValue ? Start.Value.ToString() : "",
                End.HasValue ? End.Value.ToString() : "");
        }

        #endregion

        #region Equality Operators

        /// <summary>
        /// Checks equality by calling IEquatable{KeyPeriodBase}.Equals().
        /// </summary>
        /// <param name="obj">Object to test for equality.</param>
        /// <returns>True if the key periods are for the same period.</returns>
        [Pure]
        public override bool Equals(object obj)
        {
            return Equals(obj as PeriodBase);
        }

        /// <summary>
        /// Checks for equality of two key periods.
        /// </summary>
        /// <param name="obj">Key period to test for equality.</param>
        /// <returns>True if the key periods are for the same period.</returns>
        [Pure]
        public virtual bool Equals(PeriodBase other)
        {
            return 
                other != null &&
                this.GetType() == other.GetType() &&
                this.Start.HasValue == other.Start.HasValue &&
                this.Start == other.Start &&
                this.End.HasValue == other.End.HasValue &&
                this.End == other.End;
        }

        /// <summary>
        /// Gets the hash code for the key period.
        /// </summary>
        /// <returns>Hashcode for the key period.</returns>
        [Pure]
        public override int GetHashCode()
        {
            return
                this.GetType().GetHashCode() ^
                (this.Start.HasValue ? this.Start.Value.GetHashCode() : 0) ^
                (this.End.HasValue ? this.End.Value.GetHashCode() : 0);
        }

        /// <summary>
        /// Compares equality of the two key periods.
        /// </summary>
        /// <param name="lhs">First key period.</param>
        /// <param name="rhs">Second key period.</param>
        /// <returns>Equality.</returns>
        [Pure]
        public static bool operator ==(PeriodBase lhs, PeriodBase rhs)
        {
            if (object.Equals(lhs, null))
                return object.Equals(rhs, null);
            else
                return lhs.Equals(rhs);
        }

        /// <summary>
        /// Compares equality of the two key periods.
        /// </summary>
        /// <param name="lhs">First key period.</param>
        /// <param name="rhs">Second key period.</param>
        /// <returns>Equality.</returns>
        [Pure]
        public static bool operator !=(PeriodBase lhs, PeriodBase rhs)
        {
            if (object.Equals(lhs, null))
                return !object.Equals(rhs, null);
            else
                return !lhs.Equals(rhs);
        }

        #endregion

        #region Invariants

        /// <summary>
        /// Ensures that the invariants are maintained.
        /// </summary>
        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(End != null && Start != null ? End >= Start : true);
        }

        #endregion

    }
}
