﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics.Contracts;

namespace Kiwi.LibKiwi
{

    /// <summary>
    /// Contains encrypted data.
    /// </summary>
    [DataContract]
    public sealed class EncryptedData
    {

        #region Properties

        /// <summary>
        /// Gets the information needed for decryption.  Can contain information
        /// such as which algorithm was used, and what the IV or other parameters were.
        /// </summary>
        public byte[] EncryptionInfo { get; private set; }

        /// <summary>
        /// Serialized backing for EncryptionInfo.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DataMember(Name = "EncryptionInfo", EmitDefaultValue = false)]
        private string EncryptionInfoSerialized
        {
            get { return EncryptionInfo == null ? null : Convert.ToBase64String(EncryptionInfo); }
            set { EncryptionInfo = value == null ? null : Convert.FromBase64String(value); }
        }

        /// <summary>
        /// Gets the encrypted data.
        /// </summary>
        public byte[] Value { get; private set; }

        /// <summary>
        /// Serialized backing for Value.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [DataMember(Name = "Value", IsRequired=true, EmitDefaultValue = false)]
        private string ValueSerialized
        {
            get { return Convert.ToBase64String(Value); }
            set { Value = Convert.FromBase64String(value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a encrypted data value.
        /// </summary>
        /// <param name="encryptionInfo">Information needed to decrypt the value.</param>
        /// <param name="value">Encrypted value.</param>
        public EncryptedData(byte[] encryptionInfo, byte[] value)
        {
            Contract.Requires(value != null);
            Contract.Requires(value.Length != 0);
            Contract.Ensures(EncryptionInfo == encryptionInfo);
            Contract.Ensures(Value == value);

            EncryptionInfo = encryptionInfo;
            Value = value;
        }

        #endregion

        #region Invariants

        /// <summary>
        /// Ensures that the invariants are maintained.
        /// </summary>
        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(Value != null);
        }

        #endregion

    }

}
