﻿using System.Diagnostics.Contracts;
using System.Runtime.Serialization;
using System;

namespace Kiwi.LibKiwi
{

    #region Key Request Type

    /// <summary>
    /// Enumeration for the different keys that can be requested.
    /// </summary>
    [DataContract]
    public enum KeyType
    {

        [EnumMember]
        CreatorKey = 1,

        [EnumMember]
        ViewerKey = 2
    }

    #endregion

    /// <summary>
    /// Represents a request for a key from the Kiwi key server.
    /// </summary>
    [DataContract]
    public sealed class KeyRequest
    {

        #region Properties

        /// <summary>
        /// Gets the auth token that authorizes this request to the key server.
        /// </summary>
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        public LibKiwi.AuthToken AuthToken { get; private set; }

        /// <summary>
        /// Gets the Id of the creator.
        /// </summary>
        /// <remarks>
        /// For creator keys this must match the user in auth token.
        /// </remarks>
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        public string CreatorId { get; private set; }

        /// <summary>
        /// Gets the Id of the viewer.  Not used for creator key requests.
        /// </summary>
        /// <remarks>
        /// For viewer keys this must match the user in auth token.
        /// </remarks>
        [DataMember(EmitDefaultValue = false)]
        public string ViewerId { get; private set; }

        /// <summary>
        /// The date time for which the request is being made.
        /// </summary>
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        public DateTime Timestamp { get; private set; }

        /// <summary>
        /// Gets the size of the key desired.
        /// </summary>
        [DataMember(IsRequired = true, EmitDefaultValue = false)]
        public int KeySize { get; private set; }

        /// <summary>
        /// Gets the type of the request, whether for a creator or viewer key.
        /// </summary>
        [DataMember(IsRequired = true)]
        public KeyType KeyType { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a key request.
        /// </summary>
        /// <param name="authToken">Auth token that will allow access to the key server.</param>
        /// <param name="creatorId">ID of the creator part of the requested key.</param>
        /// <param name="viewerId">ID of the viewer part of the requested key.</param>
        /// <param name="timestamp">The date time the key will be valid for.</param>
        /// <param name="keySize">Desired size of the key.</param>
        /// <param name="keyType">Type of key requested.</param>
        public KeyRequest(LibKiwi.AuthToken authToken, string creatorId, string viewerId, DateTime timestamp, int keySize, KeyType keyType)
        {
            Contract.Requires(authToken != null);
            Contract.Requires(!string.IsNullOrEmpty(creatorId));
            Contract.Requires(keyType == KeyType.ViewerKey ? !string.IsNullOrEmpty(viewerId) : true);
            Contract.Requires(timestamp != null);
            Contract.Requires(keySize > 0);
            Contract.Ensures(AuthToken == authToken);
            Contract.Ensures(CreatorId == creatorId);
            Contract.Ensures(ViewerId == viewerId);
            Contract.Ensures(Timestamp == timestamp);
            Contract.Ensures(KeySize == keySize);
            Contract.Ensures(KeyType == keyType);

            AuthToken = authToken;
            CreatorId = creatorId;
            ViewerId = viewerId;
            Timestamp = timestamp;
            KeySize = keySize;
            KeyType = keyType;
        }

        #endregion

    }

}
