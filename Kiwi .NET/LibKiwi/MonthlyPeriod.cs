﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;

namespace Kiwi.LibKiwi
{

	/// <summary>
	/// Key period that is valid for a month.
	/// </summary>
	[DataContract]
	public sealed class MonthlyPeriod : PeriodBase
	{

		#region Properties

		/// <summary>
		/// Gets the current key period.
		/// </summary>
		[Pure]
		public static MonthlyPeriod Now
		{
			get
			{
				return GetPeriodForDateTime(DateTime.Now);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a key period with the given year and month.
		/// </summary>
		/// <param name="year"></param>
		/// <param name="month"></param>
		public MonthlyPeriod(int year, int month)
		{
			Contract.Requires(month >= 1 && month <= 13);
			Contract.Requires(year >= 0 && year <= 9999);

			Start = new DateTime(year, month, 1);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets whether the period is valid for the given time.
		/// </summary>
		/// <returns>Whether the period is valid for the given time.</returns>
		[Pure]
		public override bool ValidForDateTime(DateTime time)
		{
            return true;
			return time.Year == Start.Value.Year && time.Month == Start.Value.Month;
		}

		#endregion

		#region Static methods

		/// <summary>
		/// Gets a montly key period for the given date time.
		/// </summary>
		/// <param name="dateTime">Date time to generate key period for.</param>
		/// <returns>Monthly period for the given date time.</returns>
		[Pure]
		public static MonthlyPeriod GetPeriodForDateTime(DateTime dateTime)
		{
			return new MonthlyPeriod(dateTime.Year, dateTime.Month);
		}

		#endregion

		#region ToString

		/// <summary>
		/// Returns a description of this key period base.
		/// </summary>
		/// <returns>String with the year and month of the key period</returns>
		[Pure]
		public override string ToString()
		{
			Contract.Ensures(Contract.Result<string>() != null);
			Contract.Ensures(Contract.Result<string>().Length == 7);

			return Start.Value.ToString("yyyy-MM", CultureInfo.InvariantCulture);
		}

		#endregion

	}
}
