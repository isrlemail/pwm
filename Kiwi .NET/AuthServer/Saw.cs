﻿using System;
using System.Net.Mail;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Kiwi.AuthServer.Properties;
using Kiwi.LibKiwi;
using System.Web;

namespace Kiwi.AuthServer
{
	/// <summary>
	/// Web service that handles SAW based authentication.
	/// </summary>
    [ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
	public sealed class Saw
	{

		#region Constants / Fields / Properties

		/// <summary>
		/// Random number generator to generate random bytes.
		/// </summary>
		private RandomNumberGenerator random = RandomNumberGenerator.Create();

		/// <summary>
		/// The size of tokens to generate.
		/// </summary>
		private int authTokenSize = Settings.Default.AuthTokenSize;

		/// <summary>
		/// The uri of the auth server.
		/// </summary>
		private Uri AuthServerUri
		{
			get
			{
				return new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
						VirtualPathUtility.ToAbsolute("~/"));
			}

		}

		/// <summary>
		/// The e-mail message sent out to users.
		/// </summary>
		private string emailMessage = Settings.Default.SawEmailMessage;

		#endregion

		#region Web Methods

		/// <summary>
		/// Gets a token using SAW.
		/// </summary>
		/// <param name="userId">Id of the user to get the token for.</param>
		/// <returns>Partial token, with the rest sent through SAW.</returns>
		[WebInvoke(UriTemplate = "", Method = "POST",
				RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
				BodyStyle = WebMessageBodyStyle.Bare)]
		public AuthToken GetSawBasedTokens(string userId)
		{
			Tuple<byte[], PeriodBase, string> tokenInfo = null;
			try
			{
				tokenInfo = TokenGenerator.GetToken(userId, true);
			}
			catch (ArgumentException e)
			{
				throw new WebFaultException<ArgumentException>(e, System.Net.HttpStatusCode.BadRequest);
			}


			// Split the token, one part stored in the auth token, and another
			// sent through email.
			byte[] tokenPart1 = new byte[authTokenSize];
			random.GetBytes(tokenPart1);
			AuthToken token = new AuthToken(userId, tokenInfo.Item2, tokenInfo.Item1.Xor(tokenPart1), AuthServerUri, AuthenticationMethod.Saw, tokenInfo.Item3, false);

			using (SmtpClient smtpClient = new SmtpClient())
			{
				using (MailMessage message = new MailMessage())
				{
					message.Subject = Kiwi.LibKiwi.Saw.GetSawSubject(token.TransactionId, tokenPart1);
					message.Body = string.Format(emailMessage, Convert.ToBase64String(tokenPart1), userId, tokenInfo.Item3);
					message.IsBodyHtml = true;

					message.To.Add(userId);
					smtpClient.Send(message);
					//smtpClient.SendAsync(message, null);
				}
			}

			// Return the token XORed with the token sent to e-mail.
			// When they are re XORed they will form the original token.
			return token;
		}

		#endregion

	}
}
