﻿using System;
using System.Text.RegularExpressions;
using Kiwi.AuthServer.Properties;
using Kiwi.LibKiwi;

namespace Kiwi.AuthServer
{

	/// <summary>
	/// Used to generate tokens
	/// </summary>
	internal static class TokenGenerator
	{

		#region Properties

		/// <summary>
		/// The shared key used to generate tokens.
		/// </summary>
		private static byte[] sharedKey = Convert.FromBase64String(Settings.Default.AuthServerSharedKey);

		/// <summary>
		/// The size of tokens to generate.
		/// </summary>
		private static int authTokenSize = Settings.Default.AuthTokenSize;

		/// <summary>
		/// The regex used to check e-mails as valid ids for use in saw.
		/// </summary>
		private static string emailRegex = Settings.Default.EmailRegex;

		#endregion

		#region Generator

		/// <summary>
		/// Gets the token.
		/// </summary>
		/// <param name="userId">Id of the user to get a token for.</param>
		/// <param name="checkUserIdIsEmail">Whether to check if the user id is an e-mail address or not.</param>
		/// <returns>The bytes of the token as well as its period and transaction id.</returns>
		public static Tuple<byte[], PeriodBase, string> GetToken(string userId, bool checkUserIdIsEmail = false)
		{
			if (checkUserIdIsEmail && !Regex.IsMatch(userId, emailRegex))
				throw new ArgumentException("userId", string.Format("{0} was not a valid e-mail.", userId));

			/// Generate the token.
			PeriodBase period = MonthlyPeriod.Now;
			byte[] token = Hkdf.DeriveAuthenticationToken(sharedKey, userId, period, authTokenSize);
			string transactionId = Guid.NewGuid().ToString();

			return new Tuple<byte[], PeriodBase, string>(token, period, transactionId);
		}

		/// <summary>
		/// Gets the token.
		/// </summary>
		/// <param name="userId">Id of the user to get a token for.</param>
		/// <param name="expirationDate">The expiration date of the token</param>
		/// <param name="checkUserIdIsEmail">Whether to check if the user id is an e-mail address or not.</param>
		/// <returns>The bytes of the token as well as its period and transaction id.</returns>
		public static Tuple<byte[], PeriodBase, string> GetToken(string userId, DateTime expirationDate, bool checkUserIdIsEmail = false)
		{
			if (checkUserIdIsEmail && !Regex.IsMatch(userId, emailRegex))
				throw new ArgumentException("userId", string.Format("{0} was not a valid e-mail.", userId));

			/// Generate the token.
			PeriodBase period = new ExpirationPeriod(expirationDate);
			byte[] token = Hkdf.DeriveAuthenticationToken(sharedKey, userId, period, authTokenSize);
			string transactionId = Guid.NewGuid().ToString();

			return new Tuple<byte[], PeriodBase, string>(token, period, transactionId);
		}

		#endregion

	}


}