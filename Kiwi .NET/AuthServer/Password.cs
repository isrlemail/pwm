﻿using System;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Kiwi.AuthServer.Properties;
using Kiwi.LibKiwi;
using System.Web;

namespace Kiwi.AuthServer
{
	/// <summary>
	/// Web service that handles SAW based authentication.
	/// </summary>
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
	public sealed class Password
	{

		#region Constants / Fields / Properties

		/// <summary>
		/// The shared key used to generate tokens.
		/// </summary>
		private byte[] sharedKey = Convert.FromBase64String(Settings.Default.AuthServerSharedKey);

		/// <summary>
		/// The size of tokens to generate.
		/// </summary>
		private int authTokenSize = Settings.Default.AuthTokenSize;

		/// <summary>
		/// The uri of the auth server.
		/// </summary>
		private Uri AuthServerUri
		{
			get
			{
				return new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
						VirtualPathUtility.ToAbsolute("~/"));
			}
		}

		/// <summary>
		/// The regex used to check e-mails as valid ids for use in saw.
		/// </summary>
		private string emailRegex = Settings.Default.EmailRegex;

		#endregion

		#region Web Methods

		/// <summary>
		/// Gets a token encrypted with a password.  Uses AES-256.
		/// </summary>
		/// <param name="userId">Id of the user to get the token for.</param>
		/// <returns>Token encrypted with the password.</returns>
		[WebInvoke(UriTemplate = "", Method = "POST",
				RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
				BodyStyle = WebMessageBodyStyle.Bare)]
		public AuthToken GetPasswordBasedTokens(string userId)
		{
			Tuple<byte[], PeriodBase, string> tokenInfo = null;
			try
			{
				tokenInfo = TokenGenerator.GetToken(userId);
			}
			catch (ArgumentException e)
			{
				throw new WebFaultException<ArgumentException>(e, System.Net.HttpStatusCode.BadRequest);
			}

			AuthToken token = new AuthToken(userId, tokenInfo.Item2, tokenInfo.Item1, AuthServerUri, AuthenticationMethod.Password, tokenInfo.Item3, false);
			token.EncryptWithPasswordKey(token.GetPasswordKey("password"));
			return token;
		}

		#endregion

	}
}
