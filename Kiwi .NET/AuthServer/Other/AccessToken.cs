﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Kiwi.AuthServer.Other
{
	/// <summary>
	/// Enumeration of access token types
	/// </summary>
	public enum AccessTokenType : int
	{
		Facebook = 0
	}

	/// <summary>
	/// Token received from client side authentication
	/// </summary>
	[DataContract]
	public class AccessToken
	{
		[DataMember(Name = "userId")]
		public string UserId { get; set; }

		[DataMember(Name = "value")]
		public string Value { get; set; }

		[DataMember(Name = "expires")]
		public DateTime Expires { get; set; }

		[DataMember(Name = "type")]
		public AccessTokenType TokenType { get; set; }

		[DataMember(Name = "baseObject")]
		public string BaseObject { get; set; }
	}
}