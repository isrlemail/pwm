﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Kiwi.AuthServer.Other.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<link href="../CSS/OtherAuthentication.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.js"></script>
	<script src="../Scripts/Common.js" type="text/javascript"></script>
	<script src="../Scripts/AccessToken.js" type="text/javascript"></script>
</head>
<body>
	<form id="aspnetForm" runat="server">
	<asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true">
	</asp:ScriptManager>
	<div id="fb-root">
	</div>
	<script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>
	<script src="../Scripts/FacebookConnect.js" type="text/javascript"></script>
	<div id="messageDialog">
		<h3>
			Please wait while we try to authenticate you to the Kiwi Messaging system...
			<br />
			<br />
			If this dialog doesn't automatically disappear shortly, refresh the page
			and begin secure chat again.</h3>
	</div>
	<div id="loginSelection" style="display: none">
		Please login with one of the follow:<br />
		<br />
		<fb:login-button id="fbLogin">Login with Facebook</fb:login-button>
	</div>
	</form>
</body>
</html>
