﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kiwi.LibKiwi;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;

namespace Kiwi.AuthServer.Other
{
	public static class AuthTokenExtensions
	{
		public static string ToJson(this AuthToken authToken)
		{
			DataContractJsonSerializer serializer = new DataContractJsonSerializer(authToken.GetType());
			using (MemoryStream memStream = new MemoryStream())
			{
				serializer.WriteObject(memStream, authToken);
				return Encoding.UTF8.GetString(memStream.ToArray());
			}
		}
	}
}