﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Facebook;
using Kiwi.LibKiwi;
using System.Text;

namespace Kiwi.AuthServer.Other
{
	public partial class Default : System.Web.UI.Page
	{
		/// <summary>
		/// The uri of the auth server.
		/// </summary>
		private static Uri AuthServerUri
		{
			get
			{
				return new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
						VirtualPathUtility.ToAbsolute("~/"));
			}
		}

		/// <summary>
		/// Gets a facebook token and returns a kiwi auth token
		/// </summary>
		/// <param name="pFacebookAuthToken"></param>
		/// <returns></returns>
		[WebMethod]
		public static string GetAuthToken(AccessToken pToken)
		{
			if (!IsValidToken(pToken))
				throw new HttpException(400, "Invalid token");

			Tuple<byte[], PeriodBase, string> tokenInfo = null;
			try
			{
				tokenInfo = TokenGenerator.GetToken(pToken.UserId, pToken.Expires);
				AuthToken authToken = new AuthToken(pToken.UserId, tokenInfo.Item2, Encoding.UTF8.GetBytes(pToken.Value), AuthServerUri, AuthenticationMethod.Other, tokenInfo.Item3);
				return authToken.ToJson();
			}
			catch { throw new HttpException(400, "Bad Request"); }
		}

		/// <summary>
		/// Entry point to validate tokens
		/// </summary>
		/// <param name="pToken"></param>
		/// <returns></returns>
		private static bool IsValidToken(AccessToken pToken)
		{
			switch (pToken.TokenType)
			{
				case AccessTokenType.Facebook:
					try
					{
						FacebookClient client = new FacebookClient(pToken.Value);
						dynamic me = client.Get("me");
						return me.id == pToken.UserId;
					}
					catch (Exception) { }
					break;
			}
			return false;
		}
	}
}