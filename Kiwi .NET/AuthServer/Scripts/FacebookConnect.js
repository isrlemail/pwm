﻿/**
* A class for handling facebook auth events
* @class FacebookConnect
*/
var FacebookConnect = {

	/**
	* Constructor
	* @method initialize
	*/
	initialize: function ()
	{
		var self = this;

		// Handles successful calls to GetAuthToken
		this.onAuthTokenSuccess = function (authToken)
		{
			var authTokenResp = JSON.parse(authToken);
			window.sessionStorage[self.getAuthTokenResponseStorageKey(authTokenResp.UserId, authTokenResp.AuthServerUri)] = authToken;
		};

		// Handles failed calls to GetAuthToken
		this.onAuthTokenFailure = function (error)
		{
			console.log(error);
		};

		this.getAuthToken = function (session)
		{
			debugger;
			var accessToken = new AccessToken(session.userID, session.accessToken, self.getExpirationDate(session.expiresIn), AccessTokenType.Facebook, session);
			PageMethods.GetAuthToken(accessToken, self.onAuthTokenSuccess, self.onAuthTokenFailure);
		};

		// Initializes Facebook
		FB.init({
			appId: '200599993322414',
			status: true, // check login status
			cookie: true, // enable cookies to allow the server to access the session
			xfbml: true  // parse XFBML
		});

		// Gets the currect login status
		FB.getLoginStatus(function (response)
		{
			if (response.status === 'connected')
			{
				self.getAuthToken(response.authResponse);
			}
			else
			{
				FB.Event.subscribe("auth.login", function (response)
				{
					jQuery("#messageDialog").css("display", "block");
					jQuery("#loginSelection").css("display", "none");
					self.getAuthToken(response.authResponse);
				});
				jQuery("#messageDialog").css("display", "none");
				jQuery("#loginSelection").css("display", "block");
			}
		});
	},

	/**
	* Gets a Date object from the expires value
	* @method getExpirationDate
	* @param {Number} expires The number of seconds from now
	*/
	getExpirationDate: function (expires)
	{
		var now = new Date();
		now.setSeconds(now.getSeconds() + expires);
		return now;
	},

	/**
	* Gets the local storage key for an auth token response
	* @method getAuthTokenResponseStorageKey
	*/
	getAuthTokenResponseStorageKey: function (userId, authServerId)
	{
		return String.format('AUTH_RESPONSE|{0}|{1}', userId.toLowerCase(), authServerId);
	}

};
FacebookConnect.initialize();