﻿/**
* Enum of access token types
* @class AccessTokenType
*/
var AccessTokenType = {

	Facebook: 0

};


/**
* Class that represents an access token
* @class AccessToken
*/
var AccessToken = function (userId, value, expires, type, baseObject)
{
	this.userId = userId;
	this.value = value;
	this.expires = expires;
	this.type = type;
	this.baseObject = JSON.stringify(baseObject);
};