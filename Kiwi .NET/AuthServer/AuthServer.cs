﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Kiwi.AuthServer.Properties;
using Kiwi.LibKiwi;
using System.Web;

namespace Kiwi.AuthServer
{
	/// <summary>
	/// Web service that handles SAW based authentication.
	/// </summary>
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
	public sealed class AuthServer
	{

		#region Constants / Fields / Properties

		/// <summary>
		/// The uri of the auth server.
		/// </summary>
		private Uri AuthServerUri
		{
			get
			{
				return new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)),
						VirtualPathUtility.ToAbsolute("~/"));
			}

		}

		#endregion

		#region Web Methods

		/// <summary>
		/// Gets the list of authentication methods and their urls.
		/// </summary>
		/// <returns>List of authentication methods and their urls.</returns>
		[WebInvoke(UriTemplate = "MethodsAndUris", Method = "GET",
						RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare)]
		public IEnumerable<Tuple<AuthenticationMethod, Uri>> GetMethodsAndUris()
		{
			return
					from method in GetAuthenticationMethods()
					select new Tuple<AuthenticationMethod, Uri>(method, GetAuthenticationMethodUri(method));
		}

		/// <summary>
		/// Gets the list of authentication methods supported by this server.
		/// </summary>
		/// <returns>List of authentication methods supported by this server.</returns>
		[WebInvoke(UriTemplate = "Methods", Method = "GET",
						RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare)]
		public IEnumerable<AuthenticationMethod> GetAuthenticationMethods()
		{
			return new[] { AuthenticationMethod.Password, AuthenticationMethod.Saw, AuthenticationMethod.Other };
		}

		/// <summary>
		/// Gets the Uri for a given authentication method.
		/// </summary>
		/// <param name="method">Method to get Uri for.</param>
		/// <returns>Uri for the given method.</returns>
		[WebInvoke(UriTemplate = "MethodUri?method={method}", Method = "GET",
						RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
						BodyStyle = WebMessageBodyStyle.Bare)]
		public Uri GetAuthenticationMethodUri(AuthenticationMethod method)
		{
			switch (method)
			{
				case AuthenticationMethod.Password:
					return new Uri(AuthServerUri, "Password/");

				case AuthenticationMethod.Saw:
					return new Uri(AuthServerUri, "SAW/");

				case AuthenticationMethod.Other:
					return new Uri(AuthServerUri, "Other/");

				default:
					throw new WebFaultException<ArgumentException>(
							new ArgumentException("method", "This method is not supported on this server."),
							System.Net.HttpStatusCode.BadRequest);
			}
		}

		#endregion

	}
}
