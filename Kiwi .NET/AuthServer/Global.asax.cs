﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;

namespace Kiwi.AuthServer
{
	public class Global : HttpApplication
	{
		void Application_Start(object sender, EventArgs e)
		{
			RegisterRoutes();
		}

		private void RegisterRoutes()
		{
			RouteTable.Routes.Add(new ServiceRoute("SAW", new WebServiceHostFactory(), typeof(Saw)));
			RouteTable.Routes.Add(new ServiceRoute("Password", new WebServiceHostFactory(), typeof(Password)));
			RouteTable.Routes.Add(new ServiceRoute("Info", new WebServiceHostFactory(), typeof(AuthServer)));
		}
	}
}
