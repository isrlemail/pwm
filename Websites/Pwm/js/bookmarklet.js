(function($) {	
	// Get a param from the query string.
	var getQueryStringParam = function(key) {
		key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
		var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
		return match && decodeURIComponent(match[1].replace(/\+/g, " "));
	}

	var bookmark = $("#bookmark");
	var bookmarkType = getQueryStringParam('version') || window.localStorage['bookmarkType'] || 'v1';
	window.localStorage['bookmarkType'] = bookmarkType;

	// Load the bookmarklet code.
	$.get("/" + bookmarkType + "/Bookmarklet.js?type=email&debug=true", function(data) {
		$("#bookmark").prop("href", data);		
	});
	
	// If clicked directly show an error.
	bookmark.popover({
		content: "Drag to bookmarks bar!",
		trigger: "manual"
	});
	bookmark.click(function(event) {
		event.preventDefault();
		bookmark.popover("show");
	});
})(jQuery);