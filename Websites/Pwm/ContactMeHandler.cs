﻿using System;
using System.Net.Mail;
using System.Web;

namespace Pwm
{

    // Handler for the contact form.
    public class ContactMeHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string name = context.Request.Params["name"],
                email = context.Request.Params["email"],
                message = context.Request.Params["message"];

            // If the values are not set then exit.
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(message))
            {
                context.Response.StatusCode = 400;
                return;
            }

            // Send the email.
            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(email),
                Subject = string.Format("[Pwm Website] {0} <{1}> contacted you via your website", name, email),
                Body = string.Format("{0}<br />{1}<br /><br />{2}", name, email, message),
                IsBodyHtml = true
            };
            mailMessage.To.Add(new MailAddress("ruoti@isrl.byu.edu"));

            try
            {
                new SmtpClient { EnableSsl = true }.Send(mailMessage);
                context.Response.StatusCode = 200;
            }
            catch (SmtpException)
            {
                context.Response.StatusCode = 400;
            }
            finally
            {
                context.Response.End();
            }
        }

        #endregion
    }
}
